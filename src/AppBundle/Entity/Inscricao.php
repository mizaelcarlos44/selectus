<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Inscricao
 *
 * @ORM\Table(name="inscricao")
 * @ORM\Entity
 */
class Inscricao
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="colegio_atual", type="string")
     */
    private $colegioAtual;

    /**
     * @var integer
     *
     * @ORM\Column(name="serie", type="integer")
     */
    private $serie;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Inscricao
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Inscricao
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set colegioAtual
     *
     * @param string $colegioAtual
     *
     * @return Inscricao
     */
    public function setColegioAtual($colegioAtual)
    {
        $this->colegioAtual = $colegioAtual;

        return $this;
    }

    /**
     * Get colegioAtual
     *
     * @return string
     */
    public function getColegioAtual()
    {
        return $this->colegioAtual;
    }

    /**
     * Set serie
     *
     * @param integer $serie
     *
     * @return Inscricao
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * Get serie
     *
     * @return integer
     */
    public function getSerie()
    {
        return $this->serie;
    }
}
