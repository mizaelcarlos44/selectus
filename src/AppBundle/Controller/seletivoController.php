<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Inscricao;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;


 /**
 * @Route("/seletivo")
 */
class seletivoController extends Controller
{
	/**
     * @Route("/", name="seletivo_inscricao", methods={"GET","POST"})
     */
	public function inscricaoAction(Request $request)
    {
        
		$inscricao = new Inscricao();

		$form = $this->createFormBuilder($inscricao)
			->add('nome', TextType::class)
			->add('email', TextType::class)
			->add('colegioAtual', TextType::class)
			->add('serie', TextType::class)
			->add('save', SubmitType::class, ['label' => 'Salvar'])
			->getForm();

		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$inscricao = $form->getData();

			$entityManager = $this->getDoctrine()->getManager();
			$entityManager->persist($inscricao);
			$entityManager->flush();

			return $this->redirectToRoute('seletivo_inscricao');     
		}
		 return $this->render('seletivo/inscricao.html.twig', [
            'form' => $form->createView(),
			]);
    }
	
}
