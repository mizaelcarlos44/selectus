-- --------------------------------------------------------
-- Servidor:                     selectus.mysql.uhserver.com
-- Versão do servidor:           5.6.26-log - MySQL Community Server (GPL)
-- OS do Servidor:               Linux
-- HeidiSQL Versão:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para selectus
CREATE DATABASE IF NOT EXISTS `selectus` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `selectus`;


-- Copiando estrutura para tabela selectus.inscricao
CREATE TABLE IF NOT EXISTS `inscricao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `colegio_atual` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serie` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela selectus.inscricao: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `inscricao` DISABLE KEYS */;
INSERT INTO `inscricao` (`id`, `nome`, `email`, `colegio_atual`, `serie`) VALUES
	(5, 'Mizael', 'mizaelcaros44@gmail.com', 'UNICIV', 3),
	(6, 'Carlos', 'carlos56@hotmail.com', 'UEESP', 3),
	(7, 'Carlos', 'carlos56@hotmail.com', 'UEESP', 3),
	(8, 'Taise', 'cristina44@gmail.com', 'FILISMINO FREITAS', 3);
/*!40000 ALTER TABLE `inscricao` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
