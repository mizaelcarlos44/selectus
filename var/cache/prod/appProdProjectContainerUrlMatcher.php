<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if (substr($pathinfo, -1) !== '/') {
                return array_replace($ret, $this->redirect($pathinfo.'/', 'homepage'));
            }

            return $ret;
        }

        if (0 === strpos($pathinfo, '/inscricao')) {
            // inscricao_index
            if ('/inscricao' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_inscricao_index;
                }

                $ret = array (  '_controller' => 'AppBundle\\Controller\\InscricaoController::indexAction',  '_route' => 'inscricao_index',);
                if (substr($pathinfo, -1) !== '/') {
                    return array_replace($ret, $this->redirect($pathinfo.'/', 'inscricao_index'));
                }

                return $ret;
            }
            not_inscricao_index:

            // inscricao_new
            if ('/inscricao/new' === $pathinfo) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_inscricao_new;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\InscricaoController::newAction',  '_route' => 'inscricao_new',);
            }
            not_inscricao_new:

            // inscricao_show
            if (preg_match('#^/inscricao/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_inscricao_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'inscricao_show')), array (  '_controller' => 'AppBundle\\Controller\\InscricaoController::showAction',));
            }
            not_inscricao_show:

            // inscricao_edit
            if (preg_match('#^/inscricao/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_inscricao_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'inscricao_edit')), array (  '_controller' => 'AppBundle\\Controller\\InscricaoController::editAction',));
            }
            not_inscricao_edit:

            // inscricao_delete
            if (preg_match('#^/inscricao/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_inscricao_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'inscricao_delete')), array (  '_controller' => 'AppBundle\\Controller\\InscricaoController::deleteAction',));
            }
            not_inscricao_delete:

        }

        // seletivo_inscricao
        if ('/seletivo' === $trimmedPathinfo) {
            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                $allow = array_merge($allow, array('GET', 'POST'));
                goto not_seletivo_inscricao;
            }

            $ret = array (  '_controller' => 'AppBundle\\Controller\\seletivoController::inscricaoAction',  '_route' => 'seletivo_inscricao',);
            if (substr($pathinfo, -1) !== '/') {
                return array_replace($ret, $this->redirect($pathinfo.'/', 'seletivo_inscricao'));
            }

            return $ret;
        }
        not_seletivo_inscricao:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
