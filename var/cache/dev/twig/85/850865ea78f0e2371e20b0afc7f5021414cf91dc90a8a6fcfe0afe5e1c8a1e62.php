<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_5bd7f8a780af153f7b032cdcf8ff8bed527ab6abfb102758ecf7625f9382938d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf983f9d9883b9e68fcfd1840953f6ca3b51c394a717d99525ca6edf4db625a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf983f9d9883b9e68fcfd1840953f6ca3b51c394a717d99525ca6edf4db625a1->enter($__internal_bf983f9d9883b9e68fcfd1840953f6ca3b51c394a717d99525ca6edf4db625a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_c1874e0b17559530bbb56329f7477bb434ac774204319f828a5ac67c035ba3e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1874e0b17559530bbb56329f7477bb434ac774204319f828a5ac67c035ba3e2->enter($__internal_c1874e0b17559530bbb56329f7477bb434ac774204319f828a5ac67c035ba3e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bf983f9d9883b9e68fcfd1840953f6ca3b51c394a717d99525ca6edf4db625a1->leave($__internal_bf983f9d9883b9e68fcfd1840953f6ca3b51c394a717d99525ca6edf4db625a1_prof);

        
        $__internal_c1874e0b17559530bbb56329f7477bb434ac774204319f828a5ac67c035ba3e2->leave($__internal_c1874e0b17559530bbb56329f7477bb434ac774204319f828a5ac67c035ba3e2_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2b0344bf2d1a21f94e9c769469ec37c74006a65efd51d019ee56540b24cfafd7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b0344bf2d1a21f94e9c769469ec37c74006a65efd51d019ee56540b24cfafd7->enter($__internal_2b0344bf2d1a21f94e9c769469ec37c74006a65efd51d019ee56540b24cfafd7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_8da9c1cd87ded4ad438e23a057c10781c8de3b362be595aaea42fe5ce3ed08a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8da9c1cd87ded4ad438e23a057c10781c8de3b362be595aaea42fe5ce3ed08a5->enter($__internal_8da9c1cd87ded4ad438e23a057c10781c8de3b362be595aaea42fe5ce3ed08a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_8da9c1cd87ded4ad438e23a057c10781c8de3b362be595aaea42fe5ce3ed08a5->leave($__internal_8da9c1cd87ded4ad438e23a057c10781c8de3b362be595aaea42fe5ce3ed08a5_prof);

        
        $__internal_2b0344bf2d1a21f94e9c769469ec37c74006a65efd51d019ee56540b24cfafd7->leave($__internal_2b0344bf2d1a21f94e9c769469ec37c74006a65efd51d019ee56540b24cfafd7_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_9ac6c6a93be2db2fa886241761827d27ccfcc5c71a22fb379be248410e82ffc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ac6c6a93be2db2fa886241761827d27ccfcc5c71a22fb379be248410e82ffc3->enter($__internal_9ac6c6a93be2db2fa886241761827d27ccfcc5c71a22fb379be248410e82ffc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_8b38ed8164307ce0bc39f16f722eb1334b8a223e351b93ed5a968a8ec13a0627 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b38ed8164307ce0bc39f16f722eb1334b8a223e351b93ed5a968a8ec13a0627->enter($__internal_8b38ed8164307ce0bc39f16f722eb1334b8a223e351b93ed5a968a8ec13a0627_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_8b38ed8164307ce0bc39f16f722eb1334b8a223e351b93ed5a968a8ec13a0627->leave($__internal_8b38ed8164307ce0bc39f16f722eb1334b8a223e351b93ed5a968a8ec13a0627_prof);

        
        $__internal_9ac6c6a93be2db2fa886241761827d27ccfcc5c71a22fb379be248410e82ffc3->leave($__internal_9ac6c6a93be2db2fa886241761827d27ccfcc5c71a22fb379be248410e82ffc3_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_358a8b111fa5629271276808ce90d11f765f29993e0092e739733906a606d324 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_358a8b111fa5629271276808ce90d11f765f29993e0092e739733906a606d324->enter($__internal_358a8b111fa5629271276808ce90d11f765f29993e0092e739733906a606d324_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_14e027c5fb3b644772c8e8e005aac6cd7f71d782c61cc9ec6142cdf03a366b36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14e027c5fb3b644772c8e8e005aac6cd7f71d782c61cc9ec6142cdf03a366b36->enter($__internal_14e027c5fb3b644772c8e8e005aac6cd7f71d782c61cc9ec6142cdf03a366b36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_14e027c5fb3b644772c8e8e005aac6cd7f71d782c61cc9ec6142cdf03a366b36->leave($__internal_14e027c5fb3b644772c8e8e005aac6cd7f71d782c61cc9ec6142cdf03a366b36_prof);

        
        $__internal_358a8b111fa5629271276808ce90d11f765f29993e0092e739733906a606d324->leave($__internal_358a8b111fa5629271276808ce90d11f765f29993e0092e739733906a606d324_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
