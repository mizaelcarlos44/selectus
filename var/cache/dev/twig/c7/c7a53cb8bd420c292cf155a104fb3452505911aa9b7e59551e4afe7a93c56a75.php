<?php

/* form_table_layout.html.twig */
class __TwigTemplate_b4ea34ee317421f01863344ec8868c5a8e666a0a86386209dc3ddfc3a19886c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "form_table_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'hidden_row' => array($this, 'block_hidden_row'),
                'form_widget_compound' => array($this, 'block_form_widget_compound'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c060421483abe0d2ad22ec49ab7ffd3203a1560ee8819674f9bb1dfd522c9df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c060421483abe0d2ad22ec49ab7ffd3203a1560ee8819674f9bb1dfd522c9df->enter($__internal_4c060421483abe0d2ad22ec49ab7ffd3203a1560ee8819674f9bb1dfd522c9df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        $__internal_8f566775570fa4dedd337d98161d8246ece6d99f6d91ba84bd80c19ff6939bf4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f566775570fa4dedd337d98161d8246ece6d99f6d91ba84bd80c19ff6939bf4->enter($__internal_8f566775570fa4dedd337d98161d8246ece6d99f6d91ba84bd80c19ff6939bf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form_table_layout.html.twig"));

        // line 3
        $this->displayBlock('form_row', $context, $blocks);
        // line 15
        $this->displayBlock('button_row', $context, $blocks);
        // line 24
        $this->displayBlock('hidden_row', $context, $blocks);
        // line 32
        $this->displayBlock('form_widget_compound', $context, $blocks);
        
        $__internal_4c060421483abe0d2ad22ec49ab7ffd3203a1560ee8819674f9bb1dfd522c9df->leave($__internal_4c060421483abe0d2ad22ec49ab7ffd3203a1560ee8819674f9bb1dfd522c9df_prof);

        
        $__internal_8f566775570fa4dedd337d98161d8246ece6d99f6d91ba84bd80c19ff6939bf4->leave($__internal_8f566775570fa4dedd337d98161d8246ece6d99f6d91ba84bd80c19ff6939bf4_prof);

    }

    // line 3
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_45cffcb47129bbb50fc054eba8c91e41f41e9985a84170b0cd449461aea7f662 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45cffcb47129bbb50fc054eba8c91e41f41e9985a84170b0cd449461aea7f662->enter($__internal_45cffcb47129bbb50fc054eba8c91e41f41e9985a84170b0cd449461aea7f662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_f1715fe26caba61ee847735fc3ce2764603abe39d794e6918d23f47475f75c26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1715fe26caba61ee847735fc3ce2764603abe39d794e6918d23f47475f75c26->enter($__internal_f1715fe26caba61ee847735fc3ce2764603abe39d794e6918d23f47475f75c26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 4
        echo "<tr>
        <td>";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 7
        echo "</td>
        <td>";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 11
        echo "</td>
    </tr>";
        
        $__internal_f1715fe26caba61ee847735fc3ce2764603abe39d794e6918d23f47475f75c26->leave($__internal_f1715fe26caba61ee847735fc3ce2764603abe39d794e6918d23f47475f75c26_prof);

        
        $__internal_45cffcb47129bbb50fc054eba8c91e41f41e9985a84170b0cd449461aea7f662->leave($__internal_45cffcb47129bbb50fc054eba8c91e41f41e9985a84170b0cd449461aea7f662_prof);

    }

    // line 15
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_4ec046a1405f3501041067a1feb8b7b43dc299478ad41c00bdd8e32e8389efb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ec046a1405f3501041067a1feb8b7b43dc299478ad41c00bdd8e32e8389efb1->enter($__internal_4ec046a1405f3501041067a1feb8b7b43dc299478ad41c00bdd8e32e8389efb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_cb0b62019ea26102f0975b9e0a29d5101b10e1cc0aee5b925defac246bb08d8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb0b62019ea26102f0975b9e0a29d5101b10e1cc0aee5b925defac246bb08d8e->enter($__internal_cb0b62019ea26102f0975b9e0a29d5101b10e1cc0aee5b925defac246bb08d8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 16
        echo "<tr>
        <td></td>
        <td>";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 20
        echo "</td>
    </tr>";
        
        $__internal_cb0b62019ea26102f0975b9e0a29d5101b10e1cc0aee5b925defac246bb08d8e->leave($__internal_cb0b62019ea26102f0975b9e0a29d5101b10e1cc0aee5b925defac246bb08d8e_prof);

        
        $__internal_4ec046a1405f3501041067a1feb8b7b43dc299478ad41c00bdd8e32e8389efb1->leave($__internal_4ec046a1405f3501041067a1feb8b7b43dc299478ad41c00bdd8e32e8389efb1_prof);

    }

    // line 24
    public function block_hidden_row($context, array $blocks = array())
    {
        $__internal_945dfaa8915ebe3796fcbedc2ac7d935bcc48790a2332536898f3e5f9e7693a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_945dfaa8915ebe3796fcbedc2ac7d935bcc48790a2332536898f3e5f9e7693a8->enter($__internal_945dfaa8915ebe3796fcbedc2ac7d935bcc48790a2332536898f3e5f9e7693a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        $__internal_552b54050f340ef61493c9968c3e9320d46ad05e20d9e435177be7f94bec940b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_552b54050f340ef61493c9968c3e9320d46ad05e20d9e435177be7f94bec940b->enter($__internal_552b54050f340ef61493c9968c3e9320d46ad05e20d9e435177be7f94bec940b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "hidden_row"));

        // line 25
        echo "<tr style=\"display: none\">
        <td colspan=\"2\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 28
        echo "</td>
    </tr>";
        
        $__internal_552b54050f340ef61493c9968c3e9320d46ad05e20d9e435177be7f94bec940b->leave($__internal_552b54050f340ef61493c9968c3e9320d46ad05e20d9e435177be7f94bec940b_prof);

        
        $__internal_945dfaa8915ebe3796fcbedc2ac7d935bcc48790a2332536898f3e5f9e7693a8->leave($__internal_945dfaa8915ebe3796fcbedc2ac7d935bcc48790a2332536898f3e5f9e7693a8_prof);

    }

    // line 32
    public function block_form_widget_compound($context, array $blocks = array())
    {
        $__internal_64ff7dcd60fc4239533bdf4b42623509047c882f2e61513e193c7cb0df6de423 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64ff7dcd60fc4239533bdf4b42623509047c882f2e61513e193c7cb0df6de423->enter($__internal_64ff7dcd60fc4239533bdf4b42623509047c882f2e61513e193c7cb0df6de423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        $__internal_0cf5babb7a029eada72c8267de5a121f28d4f81f1ceb2a0bd1572faffdfdd4ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0cf5babb7a029eada72c8267de5a121f28d4f81f1ceb2a0bd1572faffdfdd4ae->enter($__internal_0cf5babb7a029eada72c8267de5a121f28d4f81f1ceb2a0bd1572faffdfdd4ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_compound"));

        // line 33
        echo "<table ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 34
        if ((twig_test_empty($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) && (twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0))) {
            // line 35
            echo "<tr>
            <td colspan=\"2\">";
            // line 37
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 38
            echo "</td>
        </tr>";
        }
        // line 41
        $this->displayBlock("form_rows", $context, $blocks);
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'rest');
        // line 43
        echo "</table>";
        
        $__internal_0cf5babb7a029eada72c8267de5a121f28d4f81f1ceb2a0bd1572faffdfdd4ae->leave($__internal_0cf5babb7a029eada72c8267de5a121f28d4f81f1ceb2a0bd1572faffdfdd4ae_prof);

        
        $__internal_64ff7dcd60fc4239533bdf4b42623509047c882f2e61513e193c7cb0df6de423->leave($__internal_64ff7dcd60fc4239533bdf4b42623509047c882f2e61513e193c7cb0df6de423_prof);

    }

    public function getTemplateName()
    {
        return "form_table_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  168 => 43,  166 => 42,  164 => 41,  160 => 38,  158 => 37,  155 => 35,  153 => 34,  149 => 33,  140 => 32,  129 => 28,  127 => 27,  124 => 25,  115 => 24,  104 => 20,  102 => 19,  98 => 16,  89 => 15,  78 => 11,  76 => 10,  74 => 9,  71 => 7,  69 => 6,  66 => 4,  57 => 3,  47 => 32,  45 => 24,  43 => 15,  41 => 3,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{%- block form_row -%}
    <tr>
        <td>
            {{- form_label(form) -}}
        </td>
        <td>
            {{- form_errors(form) -}}
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock form_row -%}

{%- block button_row -%}
    <tr>
        <td></td>
        <td>
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock button_row -%}

{%- block hidden_row -%}
    <tr style=\"display: none\">
        <td colspan=\"2\">
            {{- form_widget(form) -}}
        </td>
    </tr>
{%- endblock hidden_row -%}

{%- block form_widget_compound -%}
    <table {{ block('widget_container_attributes') }}>
        {%- if form.parent is empty and errors|length > 0 -%}
        <tr>
            <td colspan=\"2\">
                {{- form_errors(form) -}}
            </td>
        </tr>
        {%- endif -%}
        {{- block('form_rows') -}}
        {{- form_rest(form) -}}
    </table>
{%- endblock form_widget_compound -%}
", "form_table_layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\form_table_layout.html.twig");
    }
}
