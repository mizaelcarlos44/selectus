<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_cf0e391ddfb9b3f718960afeba9c380f039968cbc9627c1999b7bd77f30fca45 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f70ef8dcd5c634a853395c7b74350a5f8d5c27dcc31d3abad7f8e994e4e8258a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f70ef8dcd5c634a853395c7b74350a5f8d5c27dcc31d3abad7f8e994e4e8258a->enter($__internal_f70ef8dcd5c634a853395c7b74350a5f8d5c27dcc31d3abad7f8e994e4e8258a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_9d7f9996969b4fef69b712209cfab5ceea3210206fc219ce8265a035c0f567c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d7f9996969b4fef69b712209cfab5ceea3210206fc219ce8265a035c0f567c5->enter($__internal_9d7f9996969b4fef69b712209cfab5ceea3210206fc219ce8265a035c0f567c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_f70ef8dcd5c634a853395c7b74350a5f8d5c27dcc31d3abad7f8e994e4e8258a->leave($__internal_f70ef8dcd5c634a853395c7b74350a5f8d5c27dcc31d3abad7f8e994e4e8258a_prof);

        
        $__internal_9d7f9996969b4fef69b712209cfab5ceea3210206fc219ce8265a035c0f567c5->leave($__internal_9d7f9996969b4fef69b712209cfab5ceea3210206fc219ce8265a035c0f567c5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\url_widget.html.php");
    }
}
