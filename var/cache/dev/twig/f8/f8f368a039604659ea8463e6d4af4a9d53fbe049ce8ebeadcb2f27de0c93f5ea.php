<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_10f6101ecd7ac1a0be1907c372495ed7e194300a9674f771a52cbbea2c5441a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_69e7474417ec39b4e8364f5ec2fc8b4c68e705a725c8814e6c54bb6d4edd20b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69e7474417ec39b4e8364f5ec2fc8b4c68e705a725c8814e6c54bb6d4edd20b2->enter($__internal_69e7474417ec39b4e8364f5ec2fc8b4c68e705a725c8814e6c54bb6d4edd20b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_cad6a0e8d11f5d00db219e78c3869ad1410c656cf193b86bc69fad4cfbc9dc4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cad6a0e8d11f5d00db219e78c3869ad1410c656cf193b86bc69fad4cfbc9dc4d->enter($__internal_cad6a0e8d11f5d00db219e78c3869ad1410c656cf193b86bc69fad4cfbc9dc4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_69e7474417ec39b4e8364f5ec2fc8b4c68e705a725c8814e6c54bb6d4edd20b2->leave($__internal_69e7474417ec39b4e8364f5ec2fc8b4c68e705a725c8814e6c54bb6d4edd20b2_prof);

        
        $__internal_cad6a0e8d11f5d00db219e78c3869ad1410c656cf193b86bc69fad4cfbc9dc4d->leave($__internal_cad6a0e8d11f5d00db219e78c3869ad1410c656cf193b86bc69fad4cfbc9dc4d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rest.html.php");
    }
}
