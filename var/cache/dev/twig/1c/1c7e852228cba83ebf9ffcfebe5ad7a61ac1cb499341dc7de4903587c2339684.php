<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_c774741d0954d72c7fd4d72d9a7e031a802163f1676f79fe8b6364e19c8446fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9c4aeff4a6e257768b776995ff15b170afb7833a94cb7cf591d26eca6568ee0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9c4aeff4a6e257768b776995ff15b170afb7833a94cb7cf591d26eca6568ee0->enter($__internal_a9c4aeff4a6e257768b776995ff15b170afb7833a94cb7cf591d26eca6568ee0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_ee7e81dfa5c2ea4273df67f718a73506842bc7317262b08b0b7cb9fb0a9569a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee7e81dfa5c2ea4273df67f718a73506842bc7317262b08b0b7cb9fb0a9569a1->enter($__internal_ee7e81dfa5c2ea4273df67f718a73506842bc7317262b08b0b7cb9fb0a9569a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_a9c4aeff4a6e257768b776995ff15b170afb7833a94cb7cf591d26eca6568ee0->leave($__internal_a9c4aeff4a6e257768b776995ff15b170afb7833a94cb7cf591d26eca6568ee0_prof);

        
        $__internal_ee7e81dfa5c2ea4273df67f718a73506842bc7317262b08b0b7cb9fb0a9569a1->leave($__internal_ee7e81dfa5c2ea4273df67f718a73506842bc7317262b08b0b7cb9fb0a9569a1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
