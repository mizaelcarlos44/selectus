<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_0c43d32cf737ba1f3c931c45f96023f7f224f015d0da55a2ed73c92414d07599 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7bdcfaf806598044ebff5d395de0e1cc96cc4d1a81fd75ba1415651cde82bb91 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7bdcfaf806598044ebff5d395de0e1cc96cc4d1a81fd75ba1415651cde82bb91->enter($__internal_7bdcfaf806598044ebff5d395de0e1cc96cc4d1a81fd75ba1415651cde82bb91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_ce34ab6290358c1813902bd92f17e5fa8716c2225163ca41d68ec1dc33e07c5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce34ab6290358c1813902bd92f17e5fa8716c2225163ca41d68ec1dc33e07c5c->enter($__internal_ce34ab6290358c1813902bd92f17e5fa8716c2225163ca41d68ec1dc33e07c5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_7bdcfaf806598044ebff5d395de0e1cc96cc4d1a81fd75ba1415651cde82bb91->leave($__internal_7bdcfaf806598044ebff5d395de0e1cc96cc4d1a81fd75ba1415651cde82bb91_prof);

        
        $__internal_ce34ab6290358c1813902bd92f17e5fa8716c2225163ca41d68ec1dc33e07c5c->leave($__internal_ce34ab6290358c1813902bd92f17e5fa8716c2225163ca41d68ec1dc33e07c5c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}
