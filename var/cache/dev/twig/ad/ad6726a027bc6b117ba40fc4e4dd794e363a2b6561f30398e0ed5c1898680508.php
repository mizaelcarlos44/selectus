<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_3f16bd6e92c62ae4ff11e44fb2a4a617eaf443af56cc18f9fe10a9f9c5f76ee2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ab58becab70b3e0553f7bb9d774065001cfea7a5912f497de819ea8d1a09aa6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ab58becab70b3e0553f7bb9d774065001cfea7a5912f497de819ea8d1a09aa6->enter($__internal_4ab58becab70b3e0553f7bb9d774065001cfea7a5912f497de819ea8d1a09aa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        $__internal_fe1d4564ef22e6069df2595da5ed83d0e3ef5a283732b08b90638eec7b690506 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fe1d4564ef22e6069df2595da5ed83d0e3ef5a283732b08b90638eec7b690506->enter($__internal_fe1d4564ef22e6069df2595da5ed83d0e3ef5a283732b08b90638eec7b690506_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_4ab58becab70b3e0553f7bb9d774065001cfea7a5912f497de819ea8d1a09aa6->leave($__internal_4ab58becab70b3e0553f7bb9d774065001cfea7a5912f497de819ea8d1a09aa6_prof);

        
        $__internal_fe1d4564ef22e6069df2595da5ed83d0e3ef5a283732b08b90638eec7b690506->leave($__internal_fe1d4564ef22e6069df2595da5ed83d0e3ef5a283732b08b90638eec7b690506_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "@Twig/Exception/exception.js.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.js.twig");
    }
}
