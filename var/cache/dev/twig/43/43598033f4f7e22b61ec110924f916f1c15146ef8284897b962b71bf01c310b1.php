<?php

/* @Twig/Exception/error.rdf.twig */
class __TwigTemplate_04113541e54b72efbe0bde2db0ad51ce980c2fd3b06071b618da70533e663e40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71260a27b2ea823264805ce0f5ad930a50ff9aad5f73e705e58ef85f1c256d18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71260a27b2ea823264805ce0f5ad930a50ff9aad5f73e705e58ef85f1c256d18->enter($__internal_71260a27b2ea823264805ce0f5ad930a50ff9aad5f73e705e58ef85f1c256d18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.rdf.twig"));

        $__internal_7a3485872dbf9a9340abf918141548849c12a6ea234d61b05699fe80c07cf3f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a3485872dbf9a9340abf918141548849c12a6ea234d61b05699fe80c07cf3f8->enter($__internal_7a3485872dbf9a9340abf918141548849c12a6ea234d61b05699fe80c07cf3f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_71260a27b2ea823264805ce0f5ad930a50ff9aad5f73e705e58ef85f1c256d18->leave($__internal_71260a27b2ea823264805ce0f5ad930a50ff9aad5f73e705e58ef85f1c256d18_prof);

        
        $__internal_7a3485872dbf9a9340abf918141548849c12a6ea234d61b05699fe80c07cf3f8->leave($__internal_7a3485872dbf9a9340abf918141548849c12a6ea234d61b05699fe80c07cf3f8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "@Twig/Exception/error.rdf.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.rdf.twig");
    }
}
