<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_8ece518a7d47cbec3cd793902ac2d1780625c2e915ea685331c4e93ae6c98d3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8bb166a1daf2e5a3e2bb6cf7a6dd004de4875d34587d56b3b6eae0d4e10cdd16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8bb166a1daf2e5a3e2bb6cf7a6dd004de4875d34587d56b3b6eae0d4e10cdd16->enter($__internal_8bb166a1daf2e5a3e2bb6cf7a6dd004de4875d34587d56b3b6eae0d4e10cdd16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_1b63f5e0109faa73203781c24266c101091bdfdfae879c4eb29108eee60730ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b63f5e0109faa73203781c24266c101091bdfdfae879c4eb29108eee60730ad->enter($__internal_1b63f5e0109faa73203781c24266c101091bdfdfae879c4eb29108eee60730ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8bb166a1daf2e5a3e2bb6cf7a6dd004de4875d34587d56b3b6eae0d4e10cdd16->leave($__internal_8bb166a1daf2e5a3e2bb6cf7a6dd004de4875d34587d56b3b6eae0d4e10cdd16_prof);

        
        $__internal_1b63f5e0109faa73203781c24266c101091bdfdfae879c4eb29108eee60730ad->leave($__internal_1b63f5e0109faa73203781c24266c101091bdfdfae879c4eb29108eee60730ad_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5f7aae6c2fcdd6f4efc4c5bf73421d3d577bd41a19629d0e381c6711e2dfcaf8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f7aae6c2fcdd6f4efc4c5bf73421d3d577bd41a19629d0e381c6711e2dfcaf8->enter($__internal_5f7aae6c2fcdd6f4efc4c5bf73421d3d577bd41a19629d0e381c6711e2dfcaf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_e302a8e1fea5c189524544c0e04aca54f514c7fd7fd3c68d462f23aab85a5c9f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e302a8e1fea5c189524544c0e04aca54f514c7fd7fd3c68d462f23aab85a5c9f->enter($__internal_e302a8e1fea5c189524544c0e04aca54f514c7fd7fd3c68d462f23aab85a5c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_e302a8e1fea5c189524544c0e04aca54f514c7fd7fd3c68d462f23aab85a5c9f->leave($__internal_e302a8e1fea5c189524544c0e04aca54f514c7fd7fd3c68d462f23aab85a5c9f_prof);

        
        $__internal_5f7aae6c2fcdd6f4efc4c5bf73421d3d577bd41a19629d0e381c6711e2dfcaf8->leave($__internal_5f7aae6c2fcdd6f4efc4c5bf73421d3d577bd41a19629d0e381c6711e2dfcaf8_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_30e57ec95d0cb1663e682f3de4b69a16edd24a61d366c92e8a2ce01f206190aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30e57ec95d0cb1663e682f3de4b69a16edd24a61d366c92e8a2ce01f206190aa->enter($__internal_30e57ec95d0cb1663e682f3de4b69a16edd24a61d366c92e8a2ce01f206190aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_646c49a26100543dbaf69e4e90fd2c608c63a6d26a02964a3ef9cd216bddb380 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_646c49a26100543dbaf69e4e90fd2c608c63a6d26a02964a3ef9cd216bddb380->enter($__internal_646c49a26100543dbaf69e4e90fd2c608c63a6d26a02964a3ef9cd216bddb380_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_646c49a26100543dbaf69e4e90fd2c608c63a6d26a02964a3ef9cd216bddb380->leave($__internal_646c49a26100543dbaf69e4e90fd2c608c63a6d26a02964a3ef9cd216bddb380_prof);

        
        $__internal_30e57ec95d0cb1663e682f3de4b69a16edd24a61d366c92e8a2ce01f206190aa->leave($__internal_30e57ec95d0cb1663e682f3de4b69a16edd24a61d366c92e8a2ce01f206190aa_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_1ba7851ce0a6a19213a9e400e00e8a0eb70b915d56a47440fabb9e4301d903ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ba7851ce0a6a19213a9e400e00e8a0eb70b915d56a47440fabb9e4301d903ca->enter($__internal_1ba7851ce0a6a19213a9e400e00e8a0eb70b915d56a47440fabb9e4301d903ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_968653ce212b22a6de1096d66bdd1480fb49251bcce407b4b5080f4aaca2deb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_968653ce212b22a6de1096d66bdd1480fb49251bcce407b4b5080f4aaca2deb9->enter($__internal_968653ce212b22a6de1096d66bdd1480fb49251bcce407b4b5080f4aaca2deb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_968653ce212b22a6de1096d66bdd1480fb49251bcce407b4b5080f4aaca2deb9->leave($__internal_968653ce212b22a6de1096d66bdd1480fb49251bcce407b4b5080f4aaca2deb9_prof);

        
        $__internal_1ba7851ce0a6a19213a9e400e00e8a0eb70b915d56a47440fabb9e4301d903ca->leave($__internal_1ba7851ce0a6a19213a9e400e00e8a0eb70b915d56a47440fabb9e4301d903ca_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
