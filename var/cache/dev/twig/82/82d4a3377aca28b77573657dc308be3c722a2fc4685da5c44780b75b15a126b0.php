<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_c78883b0a794df95bdc741371f79e94d9f33e13589dce19f76379b9c0a0f1d06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3b89d0f3ec84bedeeda74125fbd501052346239644a02fb9ff4a30ef93ab667 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3b89d0f3ec84bedeeda74125fbd501052346239644a02fb9ff4a30ef93ab667->enter($__internal_e3b89d0f3ec84bedeeda74125fbd501052346239644a02fb9ff4a30ef93ab667_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_709e8b5bd7648baf6d6c512d2899c9869a1f073dfc838bbfd240c8d7107f22a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_709e8b5bd7648baf6d6c512d2899c9869a1f073dfc838bbfd240c8d7107f22a1->enter($__internal_709e8b5bd7648baf6d6c512d2899c9869a1f073dfc838bbfd240c8d7107f22a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e3b89d0f3ec84bedeeda74125fbd501052346239644a02fb9ff4a30ef93ab667->leave($__internal_e3b89d0f3ec84bedeeda74125fbd501052346239644a02fb9ff4a30ef93ab667_prof);

        
        $__internal_709e8b5bd7648baf6d6c512d2899c9869a1f073dfc838bbfd240c8d7107f22a1->leave($__internal_709e8b5bd7648baf6d6c512d2899c9869a1f073dfc838bbfd240c8d7107f22a1_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_ce0bb3778921d5eb9e88d9e8bab8f234ffb7253c4827070dc6364aa7dc1288c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce0bb3778921d5eb9e88d9e8bab8f234ffb7253c4827070dc6364aa7dc1288c7->enter($__internal_ce0bb3778921d5eb9e88d9e8bab8f234ffb7253c4827070dc6364aa7dc1288c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_c89e21386ee00c064148ee38377047e39635fa9f7b14e261704e147ffcdd46aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c89e21386ee00c064148ee38377047e39635fa9f7b14e261704e147ffcdd46aa->enter($__internal_c89e21386ee00c064148ee38377047e39635fa9f7b14e261704e147ffcdd46aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_c89e21386ee00c064148ee38377047e39635fa9f7b14e261704e147ffcdd46aa->leave($__internal_c89e21386ee00c064148ee38377047e39635fa9f7b14e261704e147ffcdd46aa_prof);

        
        $__internal_ce0bb3778921d5eb9e88d9e8bab8f234ffb7253c4827070dc6364aa7dc1288c7->leave($__internal_ce0bb3778921d5eb9e88d9e8bab8f234ffb7253c4827070dc6364aa7dc1288c7_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_b5f470309f9ed20953fb10a67f15b12d0069a250f616bda5e98e07b8a5e9e54b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b5f470309f9ed20953fb10a67f15b12d0069a250f616bda5e98e07b8a5e9e54b->enter($__internal_b5f470309f9ed20953fb10a67f15b12d0069a250f616bda5e98e07b8a5e9e54b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_0339920322d9267cf3bf80bebac78ccb7c6f4b5f96ddfd3ba5090b0589d58d24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0339920322d9267cf3bf80bebac78ccb7c6f4b5f96ddfd3ba5090b0589d58d24->enter($__internal_0339920322d9267cf3bf80bebac78ccb7c6f4b5f96ddfd3ba5090b0589d58d24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_0339920322d9267cf3bf80bebac78ccb7c6f4b5f96ddfd3ba5090b0589d58d24->leave($__internal_0339920322d9267cf3bf80bebac78ccb7c6f4b5f96ddfd3ba5090b0589d58d24_prof);

        
        $__internal_b5f470309f9ed20953fb10a67f15b12d0069a250f616bda5e98e07b8a5e9e54b->leave($__internal_b5f470309f9ed20953fb10a67f15b12d0069a250f616bda5e98e07b8a5e9e54b_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_957d652242db78de5c9808d06826233c388acffbeb949cc0770de478b06b5c20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_957d652242db78de5c9808d06826233c388acffbeb949cc0770de478b06b5c20->enter($__internal_957d652242db78de5c9808d06826233c388acffbeb949cc0770de478b06b5c20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_656ff39bcfdb17ac02f3bf68c30b9df9f10603b92fb28487dafbc14d8a14a94e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_656ff39bcfdb17ac02f3bf68c30b9df9f10603b92fb28487dafbc14d8a14a94e->enter($__internal_656ff39bcfdb17ac02f3bf68c30b9df9f10603b92fb28487dafbc14d8a14a94e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_656ff39bcfdb17ac02f3bf68c30b9df9f10603b92fb28487dafbc14d8a14a94e->leave($__internal_656ff39bcfdb17ac02f3bf68c30b9df9f10603b92fb28487dafbc14d8a14a94e_prof);

        
        $__internal_957d652242db78de5c9808d06826233c388acffbeb949cc0770de478b06b5c20->leave($__internal_957d652242db78de5c9808d06826233c388acffbeb949cc0770de478b06b5c20_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
