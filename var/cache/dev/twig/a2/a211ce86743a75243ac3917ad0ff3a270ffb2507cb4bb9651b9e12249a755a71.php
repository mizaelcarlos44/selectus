<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_402f1c9b018ff08e401763608822776d824b6adecc7ad7da3973b5441ab378a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_963734fc417d97b32747b22cd0c8a3f9ce7e25a2a83c07f119565f5a3eb9dce8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_963734fc417d97b32747b22cd0c8a3f9ce7e25a2a83c07f119565f5a3eb9dce8->enter($__internal_963734fc417d97b32747b22cd0c8a3f9ce7e25a2a83c07f119565f5a3eb9dce8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_cb730375b3b9de117b9d479ae32e247e47d48b55ed91af751e1270b697e0fb5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb730375b3b9de117b9d479ae32e247e47d48b55ed91af751e1270b697e0fb5f->enter($__internal_cb730375b3b9de117b9d479ae32e247e47d48b55ed91af751e1270b697e0fb5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_963734fc417d97b32747b22cd0c8a3f9ce7e25a2a83c07f119565f5a3eb9dce8->leave($__internal_963734fc417d97b32747b22cd0c8a3f9ce7e25a2a83c07f119565f5a3eb9dce8_prof);

        
        $__internal_cb730375b3b9de117b9d479ae32e247e47d48b55ed91af751e1270b697e0fb5f->leave($__internal_cb730375b3b9de117b9d479ae32e247e47d48b55ed91af751e1270b697e0fb5f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_options.html.php");
    }
}
