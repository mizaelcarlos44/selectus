<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_38200d3abb114a03932fc98ec8defbe19cd8ef067be01b80fd4acdc4a319f006 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f997a03335e1fa681004da70dd336e39208b945d80a39111a2d77ac3157bbab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f997a03335e1fa681004da70dd336e39208b945d80a39111a2d77ac3157bbab->enter($__internal_5f997a03335e1fa681004da70dd336e39208b945d80a39111a2d77ac3157bbab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_533c23d4167d0d694ff420f52c92341ac340a45cb5ff30a35b9f580201927038 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_533c23d4167d0d694ff420f52c92341ac340a45cb5ff30a35b9f580201927038->enter($__internal_533c23d4167d0d694ff420f52c92341ac340a45cb5ff30a35b9f580201927038_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, ($context["widget"] ?? $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_5f997a03335e1fa681004da70dd336e39208b945d80a39111a2d77ac3157bbab->leave($__internal_5f997a03335e1fa681004da70dd336e39208b945d80a39111a2d77ac3157bbab_prof);

        
        $__internal_533c23d4167d0d694ff420f52c92341ac340a45cb5ff30a35b9f580201927038->leave($__internal_533c23d4167d0d694ff420f52c92341ac340a45cb5ff30a35b9f580201927038_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\money_widget.html.php");
    }
}
