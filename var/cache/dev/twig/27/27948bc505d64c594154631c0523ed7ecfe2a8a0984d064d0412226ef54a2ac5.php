<?php

/* seletivo/inscricao.html.twig */
class __TwigTemplate_cf75cea273eb73f4e40cf7e3b1e452bd069451b0b0226c2ca1618229b6aed76d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "seletivo/inscricao.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f7e32d02664913a7aae0254ead7c85dde656d2c3b41445c244dcde63928fe82e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f7e32d02664913a7aae0254ead7c85dde656d2c3b41445c244dcde63928fe82e->enter($__internal_f7e32d02664913a7aae0254ead7c85dde656d2c3b41445c244dcde63928fe82e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "seletivo/inscricao.html.twig"));

        $__internal_62dcb2bd9152d8edb50f3aeea8ba54a7cecb5fefda231e9754e05213d24bb414 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62dcb2bd9152d8edb50f3aeea8ba54a7cecb5fefda231e9754e05213d24bb414->enter($__internal_62dcb2bd9152d8edb50f3aeea8ba54a7cecb5fefda231e9754e05213d24bb414_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "seletivo/inscricao.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f7e32d02664913a7aae0254ead7c85dde656d2c3b41445c244dcde63928fe82e->leave($__internal_f7e32d02664913a7aae0254ead7c85dde656d2c3b41445c244dcde63928fe82e_prof);

        
        $__internal_62dcb2bd9152d8edb50f3aeea8ba54a7cecb5fefda231e9754e05213d24bb414->leave($__internal_62dcb2bd9152d8edb50f3aeea8ba54a7cecb5fefda231e9754e05213d24bb414_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e0b77d23f1719c26fa80b1ee26e7030374856a4149ab4f47833f0bbcbfee6e22 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0b77d23f1719c26fa80b1ee26e7030374856a4149ab4f47833f0bbcbfee6e22->enter($__internal_e0b77d23f1719c26fa80b1ee26e7030374856a4149ab4f47833f0bbcbfee6e22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_23c448ba5b1220cc752c0b9889fcb910122de875a874598d37d66c5d045bc63b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23c448ba5b1220cc752c0b9889fcb910122de875a874598d37d66c5d045bc63b->enter($__internal_23c448ba5b1220cc752c0b9889fcb910122de875a874598d37d66c5d045bc63b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Fazer inscrição</h1>

";
        // line 6
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
";
        // line 8
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_23c448ba5b1220cc752c0b9889fcb910122de875a874598d37d66c5d045bc63b->leave($__internal_23c448ba5b1220cc752c0b9889fcb910122de875a874598d37d66c5d045bc63b_prof);

        
        $__internal_e0b77d23f1719c26fa80b1ee26e7030374856a4149ab4f47833f0bbcbfee6e22->leave($__internal_e0b77d23f1719c26fa80b1ee26e7030374856a4149ab4f47833f0bbcbfee6e22_prof);

    }

    public function getTemplateName()
    {
        return "seletivo/inscricao.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 8,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Fazer inscrição</h1>

{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}
{% endblock %}
", "seletivo/inscricao.html.twig", "C:\\wamp64\\www\\selectus\\app\\Resources\\views\\seletivo\\inscricao.html.twig");
    }
}
