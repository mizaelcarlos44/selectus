<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_20b3e78f9ba7ee2537c9bcb6e8979af69dccc81268833ffa6d12d4fde6758868 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ee34b4a85e6ba9e79406177982537cd694745faa2a5152abf8ae62da6266cf9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3ee34b4a85e6ba9e79406177982537cd694745faa2a5152abf8ae62da6266cf9->enter($__internal_3ee34b4a85e6ba9e79406177982537cd694745faa2a5152abf8ae62da6266cf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_3093ffb36c0dd637f29f21935409a446e6831a3895d7199fd73a8446039e6bcc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3093ffb36c0dd637f29f21935409a446e6831a3895d7199fd73a8446039e6bcc->enter($__internal_3093ffb36c0dd637f29f21935409a446e6831a3895d7199fd73a8446039e6bcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_3ee34b4a85e6ba9e79406177982537cd694745faa2a5152abf8ae62da6266cf9->leave($__internal_3ee34b4a85e6ba9e79406177982537cd694745faa2a5152abf8ae62da6266cf9_prof);

        
        $__internal_3093ffb36c0dd637f29f21935409a446e6831a3895d7199fd73a8446039e6bcc->leave($__internal_3093ffb36c0dd637f29f21935409a446e6831a3895d7199fd73a8446039e6bcc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\widget_attributes.html.php");
    }
}
