<?php

/* inscricao/show.html.twig */
class __TwigTemplate_47e9060c3d144ac47775d1098074b89012f604ae36bed205d555f7b9dd9e5a13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "inscricao/show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2564075ebedaf592c7feeb07e6800df2c63485ac4734f46c16a051c6cc9185da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2564075ebedaf592c7feeb07e6800df2c63485ac4734f46c16a051c6cc9185da->enter($__internal_2564075ebedaf592c7feeb07e6800df2c63485ac4734f46c16a051c6cc9185da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/show.html.twig"));

        $__internal_82c92f239dd539b8d7962791212ff4bd26314c2b78c2d8dc894050a9583c58bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82c92f239dd539b8d7962791212ff4bd26314c2b78c2d8dc894050a9583c58bb->enter($__internal_82c92f239dd539b8d7962791212ff4bd26314c2b78c2d8dc894050a9583c58bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2564075ebedaf592c7feeb07e6800df2c63485ac4734f46c16a051c6cc9185da->leave($__internal_2564075ebedaf592c7feeb07e6800df2c63485ac4734f46c16a051c6cc9185da_prof);

        
        $__internal_82c92f239dd539b8d7962791212ff4bd26314c2b78c2d8dc894050a9583c58bb->leave($__internal_82c92f239dd539b8d7962791212ff4bd26314c2b78c2d8dc894050a9583c58bb_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3bbf0e325ac90d1405927bda06407ab10b29c48b5977cae99f6ccfdc7f67421c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3bbf0e325ac90d1405927bda06407ab10b29c48b5977cae99f6ccfdc7f67421c->enter($__internal_3bbf0e325ac90d1405927bda06407ab10b29c48b5977cae99f6ccfdc7f67421c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a2c51ebd060617e168f103f7508d0c101d524099dbab4602901b8bcca467acd3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2c51ebd060617e168f103f7508d0c101d524099dbab4602901b8bcca467acd3->enter($__internal_a2c51ebd060617e168f103f7508d0c101d524099dbab4602901b8bcca467acd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Visualizar Inscrição</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute(($context["inscricao"] ?? $this->getContext($context, "inscricao")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Nome</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute(($context["inscricao"] ?? $this->getContext($context, "inscricao")), "nome", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute(($context["inscricao"] ?? $this->getContext($context, "inscricao")), "email", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Colegioatual</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute(($context["inscricao"] ?? $this->getContext($context, "inscricao")), "colegioAtual", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Serie</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute(($context["inscricao"] ?? $this->getContext($context, "inscricao")), "serie", array()), "html", null, true);
        echo "</td>
            </tr>
        </tbody>
    </table>

    
        
            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input class=\"btn btn-danger\" type=\"submit\" value=\"Delete\">
            ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
        
   
";
        
        $__internal_a2c51ebd060617e168f103f7508d0c101d524099dbab4602901b8bcca467acd3->leave($__internal_a2c51ebd060617e168f103f7508d0c101d524099dbab4602901b8bcca467acd3_prof);

        
        $__internal_3bbf0e325ac90d1405927bda06407ab10b29c48b5977cae99f6ccfdc7f67421c->leave($__internal_3bbf0e325ac90d1405927bda06407ab10b29c48b5977cae99f6ccfdc7f67421c_prof);

    }

    public function getTemplateName()
    {
        return "inscricao/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 35,  95 => 33,  85 => 26,  78 => 22,  71 => 18,  64 => 14,  57 => 10,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Visualizar Inscrição</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ inscricao.id }}</td>
            </tr>
            <tr>
                <th>Nome</th>
                <td>{{ inscricao.nome }}</td>
            </tr>
            <tr>
                <th>Email</th>
                <td>{{ inscricao.email }}</td>
            </tr>
            <tr>
                <th>Colegioatual</th>
                <td>{{ inscricao.colegioAtual }}</td>
            </tr>
            <tr>
                <th>Serie</th>
                <td>{{ inscricao.serie }}</td>
            </tr>
        </tbody>
    </table>

    
        
            {{ form_start(delete_form) }}
                <input class=\"btn btn-danger\" type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        
   
{% endblock %}
", "inscricao/show.html.twig", "C:\\wamp64\\www\\selectus\\app\\Resources\\views\\inscricao\\show.html.twig");
    }
}
