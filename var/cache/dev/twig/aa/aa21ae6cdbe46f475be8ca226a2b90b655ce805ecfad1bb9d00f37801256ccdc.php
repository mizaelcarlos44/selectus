<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_77a1fdcb3d733a62eca66f75da0ab332c7a0043cd01c9cce54e958e43fc839a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a1e5257fc2750a60c4d5338377981706fe923bd80de996d0a06def3d00d2150 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a1e5257fc2750a60c4d5338377981706fe923bd80de996d0a06def3d00d2150->enter($__internal_8a1e5257fc2750a60c4d5338377981706fe923bd80de996d0a06def3d00d2150_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_19b74b9d15a9a7428474a413638703ba59f92d230d3b347dc602631b2e4f095d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19b74b9d15a9a7428474a413638703ba59f92d230d3b347dc602631b2e4f095d->enter($__internal_19b74b9d15a9a7428474a413638703ba59f92d230d3b347dc602631b2e4f095d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_8a1e5257fc2750a60c4d5338377981706fe923bd80de996d0a06def3d00d2150->leave($__internal_8a1e5257fc2750a60c4d5338377981706fe923bd80de996d0a06def3d00d2150_prof);

        
        $__internal_19b74b9d15a9a7428474a413638703ba59f92d230d3b347dc602631b2e4f095d->leave($__internal_19b74b9d15a9a7428474a413638703ba59f92d230d3b347dc602631b2e4f095d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget.html.php");
    }
}
