<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_b30c823462b383e5de0b753849d8c65af2aa25ed045ea5966984cd8a8bdb5673 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87df621804f86c4b0b2c6007bf4a42616c7464a2251c7dba51fed4be66326a35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87df621804f86c4b0b2c6007bf4a42616c7464a2251c7dba51fed4be66326a35->enter($__internal_87df621804f86c4b0b2c6007bf4a42616c7464a2251c7dba51fed4be66326a35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_24b1c7951170dad1d291902f049e35f6f8882697197ac813db202ef2c157364d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24b1c7951170dad1d291902f049e35f6f8882697197ac813db202ef2c157364d->enter($__internal_24b1c7951170dad1d291902f049e35f6f8882697197ac813db202ef2c157364d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_87df621804f86c4b0b2c6007bf4a42616c7464a2251c7dba51fed4be66326a35->leave($__internal_87df621804f86c4b0b2c6007bf4a42616c7464a2251c7dba51fed4be66326a35_prof);

        
        $__internal_24b1c7951170dad1d291902f049e35f6f8882697197ac813db202ef2c157364d->leave($__internal_24b1c7951170dad1d291902f049e35f6f8882697197ac813db202ef2c157364d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget_expanded.html.php");
    }
}
