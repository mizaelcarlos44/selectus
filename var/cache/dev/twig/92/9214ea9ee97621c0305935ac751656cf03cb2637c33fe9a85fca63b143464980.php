<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_33c3695c3499b65e7e6e7620fc5fe4800b6f710eed7c122dd53b52dac3e5ac05 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e668b5cbc98cb12bb40ac241ef2184fa1e8415dc64f6611488b00855b3c93ed3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e668b5cbc98cb12bb40ac241ef2184fa1e8415dc64f6611488b00855b3c93ed3->enter($__internal_e668b5cbc98cb12bb40ac241ef2184fa1e8415dc64f6611488b00855b3c93ed3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_1eb8df3441ca599c4714fce477670c639f0726d45753ebf5de7335702129898a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1eb8df3441ca599c4714fce477670c639f0726d45753ebf5de7335702129898a->enter($__internal_1eb8df3441ca599c4714fce477670c639f0726d45753ebf5de7335702129898a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_e668b5cbc98cb12bb40ac241ef2184fa1e8415dc64f6611488b00855b3c93ed3->leave($__internal_e668b5cbc98cb12bb40ac241ef2184fa1e8415dc64f6611488b00855b3c93ed3_prof);

        
        $__internal_1eb8df3441ca599c4714fce477670c639f0726d45753ebf5de7335702129898a->leave($__internal_1eb8df3441ca599c4714fce477670c639f0726d45753ebf5de7335702129898a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\widget_container_attributes.html.php");
    }
}
