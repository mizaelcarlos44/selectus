<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_4c71843004658eea480d1426f79a80653b7afafba1c2049cb195499103285b50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81717981833b4417cc9d3685c59e57bb8bc5d7551396bade381849f409ab6e6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81717981833b4417cc9d3685c59e57bb8bc5d7551396bade381849f409ab6e6c->enter($__internal_81717981833b4417cc9d3685c59e57bb8bc5d7551396bade381849f409ab6e6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_d5e1037de5142d530ba33bf88a8d62f0634c5a61bf27265f82d745eb830a9ace = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5e1037de5142d530ba33bf88a8d62f0634c5a61bf27265f82d745eb830a9ace->enter($__internal_d5e1037de5142d530ba33bf88a8d62f0634c5a61bf27265f82d745eb830a9ace_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_81717981833b4417cc9d3685c59e57bb8bc5d7551396bade381849f409ab6e6c->leave($__internal_81717981833b4417cc9d3685c59e57bb8bc5d7551396bade381849f409ab6e6c_prof);

        
        $__internal_d5e1037de5142d530ba33bf88a8d62f0634c5a61bf27265f82d745eb830a9ace->leave($__internal_d5e1037de5142d530ba33bf88a8d62f0634c5a61bf27265f82d745eb830a9ace_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rows.html.php");
    }
}
