<?php

/* inscricao/index.html.twig */
class __TwigTemplate_9c6c6557791a18949e2c7e6a2bd524f858082ac54f4fe7ece93e31fc52324016 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "inscricao/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c247f10a4eb04afcb7f85b3b1b7ad282e8e77fa03ecea86a0eefb50e12268436 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c247f10a4eb04afcb7f85b3b1b7ad282e8e77fa03ecea86a0eefb50e12268436->enter($__internal_c247f10a4eb04afcb7f85b3b1b7ad282e8e77fa03ecea86a0eefb50e12268436_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/index.html.twig"));

        $__internal_16be485b56402938aeceb4a37de1535853f49f04a05098f6655ad3150f7a1757 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16be485b56402938aeceb4a37de1535853f49f04a05098f6655ad3150f7a1757->enter($__internal_16be485b56402938aeceb4a37de1535853f49f04a05098f6655ad3150f7a1757_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c247f10a4eb04afcb7f85b3b1b7ad282e8e77fa03ecea86a0eefb50e12268436->leave($__internal_c247f10a4eb04afcb7f85b3b1b7ad282e8e77fa03ecea86a0eefb50e12268436_prof);

        
        $__internal_16be485b56402938aeceb4a37de1535853f49f04a05098f6655ad3150f7a1757->leave($__internal_16be485b56402938aeceb4a37de1535853f49f04a05098f6655ad3150f7a1757_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b847df6a50d71a83d0d830194a045f4cad9768fe05a4d32c152b3f95584dfb9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b847df6a50d71a83d0d830194a045f4cad9768fe05a4d32c152b3f95584dfb9e->enter($__internal_b847df6a50d71a83d0d830194a045f4cad9768fe05a4d32c152b3f95584dfb9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e0b7fac92fd04aff050689a5621de878da45d26c9dc03f207fd5090f4e3741b2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0b7fac92fd04aff050689a5621de878da45d26c9dc03f207fd5090f4e3741b2->enter($__internal_e0b7fac92fd04aff050689a5621de878da45d26c9dc03f207fd5090f4e3741b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Lista de inscrições</h1>

    <table class=\"table .table-bordered\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Colégio atual</th>
                <th>Série</th>
                <th>Opções</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["inscricaos"] ?? $this->getContext($context, "inscricaos")));
        foreach ($context['_seq'] as $context["_key"] => $context["inscricao"]) {
            // line 19
            echo "            <tr>
                <td><a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("inscricao_show", array("id" => $this->getAttribute($context["inscricao"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["inscricao"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["inscricao"], "nome", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["inscricao"], "email", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["inscricao"], "colegioAtual", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["inscricao"], "serie", array()), "html", null, true);
            echo "</td>
                <td>
                        
                          <a class=\"btn btn-info\" href=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("inscricao_show", array("id" => $this->getAttribute($context["inscricao"], "id", array()))), "html", null, true);
            echo "\">Visualizar</a>
                          <a class=\"btn btn-primary\" href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("inscricao_edit", array("id" => $this->getAttribute($context["inscricao"], "id", array()))), "html", null, true);
            echo "\">Alterar</a>
                        
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['inscricao'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "        </tbody>
    </table>

    <ul>
            <a class=\"btn btn-success\" href=\"";
        // line 37
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("inscricao_new");
        echo "\">Cadastrar inscrição</a>
       
    </ul>
";
        
        $__internal_e0b7fac92fd04aff050689a5621de878da45d26c9dc03f207fd5090f4e3741b2->leave($__internal_e0b7fac92fd04aff050689a5621de878da45d26c9dc03f207fd5090f4e3741b2_prof);

        
        $__internal_b847df6a50d71a83d0d830194a045f4cad9768fe05a4d32c152b3f95584dfb9e->leave($__internal_b847df6a50d71a83d0d830194a045f4cad9768fe05a4d32c152b3f95584dfb9e_prof);

    }

    public function getTemplateName()
    {
        return "inscricao/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 37,  111 => 33,  100 => 28,  96 => 27,  90 => 24,  86 => 23,  82 => 22,  78 => 21,  72 => 20,  69 => 19,  65 => 18,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Lista de inscrições</h1>

    <table class=\"table .table-bordered\">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Email</th>
                <th>Colégio atual</th>
                <th>Série</th>
                <th>Opções</th>
            </tr>
        </thead>
        <tbody>
        {% for inscricao in inscricaos %}
            <tr>
                <td><a href=\"{{ path('inscricao_show', { 'id': inscricao.id }) }}\">{{ inscricao.id }}</a></td>
                <td>{{ inscricao.nome }}</td>
                <td>{{ inscricao.email }}</td>
                <td>{{ inscricao.colegioAtual }}</td>
                <td>{{ inscricao.serie }}</td>
                <td>
                        
                          <a class=\"btn btn-info\" href=\"{{ path('inscricao_show', { 'id': inscricao.id }) }}\">Visualizar</a>
                          <a class=\"btn btn-primary\" href=\"{{ path('inscricao_edit', { 'id': inscricao.id }) }}\">Alterar</a>
                        
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
            <a class=\"btn btn-success\" href=\"{{ path('inscricao_new') }}\">Cadastrar inscrição</a>
       
    </ul>
{% endblock %}
", "inscricao/index.html.twig", "C:\\wamp64\\www\\selectus\\app\\Resources\\views\\inscricao\\index.html.twig");
    }
}
