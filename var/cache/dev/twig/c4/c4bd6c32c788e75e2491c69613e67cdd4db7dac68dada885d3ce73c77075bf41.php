<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_45bea2313c538a2004e5fe6b6234a045a6da19e7e482b2aaa3ab21d8affe724a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ac985ab2bda8b63191707cd00b44360c857138188368966e524938a7ad4c8d14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac985ab2bda8b63191707cd00b44360c857138188368966e524938a7ad4c8d14->enter($__internal_ac985ab2bda8b63191707cd00b44360c857138188368966e524938a7ad4c8d14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_5430b571e1af775ff1363de2bf448c86535451a574a79975ed107dc32af380b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5430b571e1af775ff1363de2bf448c86535451a574a79975ed107dc32af380b8->enter($__internal_5430b571e1af775ff1363de2bf448c86535451a574a79975ed107dc32af380b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ac985ab2bda8b63191707cd00b44360c857138188368966e524938a7ad4c8d14->leave($__internal_ac985ab2bda8b63191707cd00b44360c857138188368966e524938a7ad4c8d14_prof);

        
        $__internal_5430b571e1af775ff1363de2bf448c86535451a574a79975ed107dc32af380b8->leave($__internal_5430b571e1af775ff1363de2bf448c86535451a574a79975ed107dc32af380b8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_4b8f36f3f0ad2c2a362be26014419f890fb53168377a8208100714ed23be96be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b8f36f3f0ad2c2a362be26014419f890fb53168377a8208100714ed23be96be->enter($__internal_4b8f36f3f0ad2c2a362be26014419f890fb53168377a8208100714ed23be96be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ca85750f0e4e21a042ec8f5fe504df38871960372f5e29d842492cfc3d4d708b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca85750f0e4e21a042ec8f5fe504df38871960372f5e29d842492cfc3d4d708b->enter($__internal_ca85750f0e4e21a042ec8f5fe504df38871960372f5e29d842492cfc3d4d708b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_ca85750f0e4e21a042ec8f5fe504df38871960372f5e29d842492cfc3d4d708b->leave($__internal_ca85750f0e4e21a042ec8f5fe504df38871960372f5e29d842492cfc3d4d708b_prof);

        
        $__internal_4b8f36f3f0ad2c2a362be26014419f890fb53168377a8208100714ed23be96be->leave($__internal_4b8f36f3f0ad2c2a362be26014419f890fb53168377a8208100714ed23be96be_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_138cc4467d1fa103053ed8067099ad9f8477082751e00fb62bbc1a020c6d9e93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_138cc4467d1fa103053ed8067099ad9f8477082751e00fb62bbc1a020c6d9e93->enter($__internal_138cc4467d1fa103053ed8067099ad9f8477082751e00fb62bbc1a020c6d9e93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d4d3e87af090c910bd59ee2fa092665d451ab734bef70cc216f05179ed6a9a79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4d3e87af090c910bd59ee2fa092665d451ab734bef70cc216f05179ed6a9a79->enter($__internal_d4d3e87af090c910bd59ee2fa092665d451ab734bef70cc216f05179ed6a9a79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_d4d3e87af090c910bd59ee2fa092665d451ab734bef70cc216f05179ed6a9a79->leave($__internal_d4d3e87af090c910bd59ee2fa092665d451ab734bef70cc216f05179ed6a9a79_prof);

        
        $__internal_138cc4467d1fa103053ed8067099ad9f8477082751e00fb62bbc1a020c6d9e93->leave($__internal_138cc4467d1fa103053ed8067099ad9f8477082751e00fb62bbc1a020c6d9e93_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}
