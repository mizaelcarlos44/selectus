<?php

/* foundation_5_layout.html.twig */
class __TwigTemplate_acb45a52ee70b90b49d96d2b9e5659196e1fe27be6dc98ffd3017547487a65be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "foundation_5_layout.html.twig", 1);
        $this->blocks = array(
            'form_widget_simple' => array($this, 'block_form_widget_simple'),
            'textarea_widget' => array($this, 'block_textarea_widget'),
            'button_widget' => array($this, 'block_button_widget'),
            'money_widget' => array($this, 'block_money_widget'),
            'percent_widget' => array($this, 'block_percent_widget'),
            'datetime_widget' => array($this, 'block_datetime_widget'),
            'date_widget' => array($this, 'block_date_widget'),
            'time_widget' => array($this, 'block_time_widget'),
            'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
            'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
            'checkbox_widget' => array($this, 'block_checkbox_widget'),
            'radio_widget' => array($this, 'block_radio_widget'),
            'form_label' => array($this, 'block_form_label'),
            'choice_label' => array($this, 'block_choice_label'),
            'checkbox_label' => array($this, 'block_checkbox_label'),
            'radio_label' => array($this, 'block_radio_label'),
            'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
            'form_row' => array($this, 'block_form_row'),
            'choice_row' => array($this, 'block_choice_row'),
            'date_row' => array($this, 'block_date_row'),
            'time_row' => array($this, 'block_time_row'),
            'datetime_row' => array($this, 'block_datetime_row'),
            'checkbox_row' => array($this, 'block_checkbox_row'),
            'radio_row' => array($this, 'block_radio_row'),
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23bf1fdd68b7aacf361ad118b6a8288b051809167af71e76c9930b9e1dcb8ca2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23bf1fdd68b7aacf361ad118b6a8288b051809167af71e76c9930b9e1dcb8ca2->enter($__internal_23bf1fdd68b7aacf361ad118b6a8288b051809167af71e76c9930b9e1dcb8ca2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $__internal_f183cf21c36fa2b5c2f6b6012c7648471f749fe2cd64dfed813442d7752ac901 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f183cf21c36fa2b5c2f6b6012c7648471f749fe2cd64dfed813442d7752ac901->enter($__internal_f183cf21c36fa2b5c2f6b6012c7648471f749fe2cd64dfed813442d7752ac901_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "foundation_5_layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_23bf1fdd68b7aacf361ad118b6a8288b051809167af71e76c9930b9e1dcb8ca2->leave($__internal_23bf1fdd68b7aacf361ad118b6a8288b051809167af71e76c9930b9e1dcb8ca2_prof);

        
        $__internal_f183cf21c36fa2b5c2f6b6012c7648471f749fe2cd64dfed813442d7752ac901->leave($__internal_f183cf21c36fa2b5c2f6b6012c7648471f749fe2cd64dfed813442d7752ac901_prof);

    }

    // line 6
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_3affb42429af457b9c896523c6ac7f07f70094aa41e2a09f037401c5cda8bfde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3affb42429af457b9c896523c6ac7f07f70094aa41e2a09f037401c5cda8bfde->enter($__internal_3affb42429af457b9c896523c6ac7f07f70094aa41e2a09f037401c5cda8bfde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_c4020965362b2558e13acf229dd6326e0c28c4082591930ae996d530c41740df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4020965362b2558e13acf229dd6326e0c28c4082591930ae996d530c41740df->enter($__internal_c4020965362b2558e13acf229dd6326e0c28c4082591930ae996d530c41740df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 7
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 8
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 9
            echo "    ";
        }
        // line 10
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_c4020965362b2558e13acf229dd6326e0c28c4082591930ae996d530c41740df->leave($__internal_c4020965362b2558e13acf229dd6326e0c28c4082591930ae996d530c41740df_prof);

        
        $__internal_3affb42429af457b9c896523c6ac7f07f70094aa41e2a09f037401c5cda8bfde->leave($__internal_3affb42429af457b9c896523c6ac7f07f70094aa41e2a09f037401c5cda8bfde_prof);

    }

    // line 13
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_d0303067157457c686ef88cfb6eaa4257f9720fe628db8e415b99ef7ead0b77a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0303067157457c686ef88cfb6eaa4257f9720fe628db8e415b99ef7ead0b77a->enter($__internal_d0303067157457c686ef88cfb6eaa4257f9720fe628db8e415b99ef7ead0b77a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_bd266f37f43c78b506a5e5760a1a41aebf481a74675d7fbe6ed987f041a6e2ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd266f37f43c78b506a5e5760a1a41aebf481a74675d7fbe6ed987f041a6e2ae->enter($__internal_bd266f37f43c78b506a5e5760a1a41aebf481a74675d7fbe6ed987f041a6e2ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 14
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 15
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 16
            echo "    ";
        }
        // line 17
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_bd266f37f43c78b506a5e5760a1a41aebf481a74675d7fbe6ed987f041a6e2ae->leave($__internal_bd266f37f43c78b506a5e5760a1a41aebf481a74675d7fbe6ed987f041a6e2ae_prof);

        
        $__internal_d0303067157457c686ef88cfb6eaa4257f9720fe628db8e415b99ef7ead0b77a->leave($__internal_d0303067157457c686ef88cfb6eaa4257f9720fe628db8e415b99ef7ead0b77a_prof);

    }

    // line 20
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_6aa0bdae508b12aa0a59e82a95a0a34d4c9f63462983b77d1d8b6279d9334fda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6aa0bdae508b12aa0a59e82a95a0a34d4c9f63462983b77d1d8b6279d9334fda->enter($__internal_6aa0bdae508b12aa0a59e82a95a0a34d4c9f63462983b77d1d8b6279d9334fda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_018d797e6fd463160d651f4e347eb567435f66275ddd3bcec807cf28b2ba29b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_018d797e6fd463160d651f4e347eb567435f66275ddd3bcec807cf28b2ba29b4->enter($__internal_018d797e6fd463160d651f4e347eb567435f66275ddd3bcec807cf28b2ba29b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 21
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " button"))));
        // line 22
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_018d797e6fd463160d651f4e347eb567435f66275ddd3bcec807cf28b2ba29b4->leave($__internal_018d797e6fd463160d651f4e347eb567435f66275ddd3bcec807cf28b2ba29b4_prof);

        
        $__internal_6aa0bdae508b12aa0a59e82a95a0a34d4c9f63462983b77d1d8b6279d9334fda->leave($__internal_6aa0bdae508b12aa0a59e82a95a0a34d4c9f63462983b77d1d8b6279d9334fda_prof);

    }

    // line 25
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_99decf3509629c0af4b1305c2f993f3b491b6d95db5e24c05621b8a339dd72de = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99decf3509629c0af4b1305c2f993f3b491b6d95db5e24c05621b8a339dd72de->enter($__internal_99decf3509629c0af4b1305c2f993f3b491b6d95db5e24c05621b8a339dd72de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_fd2b98eadf27a9bedea09780c6242505d59f7bc667b1f5888e3099c3bf5f13e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd2b98eadf27a9bedea09780c6242505d59f7bc667b1f5888e3099c3bf5f13e6->enter($__internal_fd2b98eadf27a9bedea09780c6242505d59f7bc667b1f5888e3099c3bf5f13e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 26
        echo "<div class=\"row collapse\">
        ";
        // line 27
        $context["prepend"] = ("{{" == twig_slice($this->env, ($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), 0, 2));
        // line 28
        echo "        ";
        if ( !($context["prepend"] ?? $this->getContext($context, "prepend"))) {
            // line 29
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">";
            // line 30
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
            </div>
        ";
        }
        // line 33
        echo "        <div class=\"small-9 large-10 columns\">";
        // line 34
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 35
        echo "</div>
        ";
        // line 36
        if (($context["prepend"] ?? $this->getContext($context, "prepend"))) {
            // line 37
            echo "            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">";
            // line 38
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
            </div>
        ";
        }
        // line 41
        echo "    </div>";
        
        $__internal_fd2b98eadf27a9bedea09780c6242505d59f7bc667b1f5888e3099c3bf5f13e6->leave($__internal_fd2b98eadf27a9bedea09780c6242505d59f7bc667b1f5888e3099c3bf5f13e6_prof);

        
        $__internal_99decf3509629c0af4b1305c2f993f3b491b6d95db5e24c05621b8a339dd72de->leave($__internal_99decf3509629c0af4b1305c2f993f3b491b6d95db5e24c05621b8a339dd72de_prof);

    }

    // line 44
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_d99c40bc7a6a6ea9c61b017e90ceacfbe0b0ae6738e4b8600e4ae48fa93c0c15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d99c40bc7a6a6ea9c61b017e90ceacfbe0b0ae6738e4b8600e4ae48fa93c0c15->enter($__internal_d99c40bc7a6a6ea9c61b017e90ceacfbe0b0ae6738e4b8600e4ae48fa93c0c15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_b9050619cae78d2f2aae2fca1dd1a957a5b1eb9438fba2246e3c2640dae38b5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9050619cae78d2f2aae2fca1dd1a957a5b1eb9438fba2246e3c2640dae38b5f->enter($__internal_b9050619cae78d2f2aae2fca1dd1a957a5b1eb9438fba2246e3c2640dae38b5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 45
        echo "<div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">";
        // line 47
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 48
        echo "</div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>";
        
        $__internal_b9050619cae78d2f2aae2fca1dd1a957a5b1eb9438fba2246e3c2640dae38b5f->leave($__internal_b9050619cae78d2f2aae2fca1dd1a957a5b1eb9438fba2246e3c2640dae38b5f_prof);

        
        $__internal_d99c40bc7a6a6ea9c61b017e90ceacfbe0b0ae6738e4b8600e4ae48fa93c0c15->leave($__internal_d99c40bc7a6a6ea9c61b017e90ceacfbe0b0ae6738e4b8600e4ae48fa93c0c15_prof);

    }

    // line 55
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_f546668404a2aa674e4ffa11f1b0265efd225c21f8e02b325d447ed3d9e0dd9b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f546668404a2aa674e4ffa11f1b0265efd225c21f8e02b325d447ed3d9e0dd9b->enter($__internal_f546668404a2aa674e4ffa11f1b0265efd225c21f8e02b325d447ed3d9e0dd9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_3c3f6ce7f1889ef2e561587c7af0b2c1107a3bac013ba7c7d4a968f25830350a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c3f6ce7f1889ef2e561587c7af0b2c1107a3bac013ba7c7d4a968f25830350a->enter($__internal_3c3f6ce7f1889ef2e561587c7af0b2c1107a3bac013ba7c7d4a968f25830350a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 56
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 57
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 59
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 60
            echo "        <div class=\"row\">
            <div class=\"large-7 columns\">";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'errors');
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'errors');
            echo "</div>
        </div>
        <div ";
            // line 64
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            <div class=\"large-7 columns\">";
            // line 65
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            echo "</div>
            <div class=\"large-5 columns\">";
            // line 66
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            echo "</div>
        </div>
    ";
        }
        
        $__internal_3c3f6ce7f1889ef2e561587c7af0b2c1107a3bac013ba7c7d4a968f25830350a->leave($__internal_3c3f6ce7f1889ef2e561587c7af0b2c1107a3bac013ba7c7d4a968f25830350a_prof);

        
        $__internal_f546668404a2aa674e4ffa11f1b0265efd225c21f8e02b325d447ed3d9e0dd9b->leave($__internal_f546668404a2aa674e4ffa11f1b0265efd225c21f8e02b325d447ed3d9e0dd9b_prof);

    }

    // line 71
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_da3f3e09bdfe760c6e767547570f1ac6bfcf7da76d4d77424b907e2fb7671047 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_da3f3e09bdfe760c6e767547570f1ac6bfcf7da76d4d77424b907e2fb7671047->enter($__internal_da3f3e09bdfe760c6e767547570f1ac6bfcf7da76d4d77424b907e2fb7671047_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_882e3e35d1167a780f641e67b9f66ef9d7c22b7171f3460181f0587080254523 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_882e3e35d1167a780f641e67b9f66ef9d7c22b7171f3460181f0587080254523->enter($__internal_882e3e35d1167a780f641e67b9f66ef9d7c22b7171f3460181f0587080254523_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 72
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 73
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 75
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 76
            echo "        ";
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 77
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 79
            echo twig_replace_filter(($context["date_pattern"] ?? $this->getContext($context, "date_pattern")), array("{{ year }}" => (("<div class=\"large-4 columns\">" .             // line 80
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "year", array()), 'widget')) . "</div>"), "{{ month }}" => (("<div class=\"large-4 columns\">" .             // line 81
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "month", array()), 'widget')) . "</div>"), "{{ day }}" => (("<div class=\"large-4 columns\">" .             // line 82
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "day", array()), 'widget')) . "</div>")));
            // line 84
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 85
                echo "            </div>
        ";
            }
            // line 87
            echo "    ";
        }
        
        $__internal_882e3e35d1167a780f641e67b9f66ef9d7c22b7171f3460181f0587080254523->leave($__internal_882e3e35d1167a780f641e67b9f66ef9d7c22b7171f3460181f0587080254523_prof);

        
        $__internal_da3f3e09bdfe760c6e767547570f1ac6bfcf7da76d4d77424b907e2fb7671047->leave($__internal_da3f3e09bdfe760c6e767547570f1ac6bfcf7da76d4d77424b907e2fb7671047_prof);

    }

    // line 90
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_fb5c9fae9f28beec6e6908da0ff65d34283f8f2d9e4abce75e2c1891210c398a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fb5c9fae9f28beec6e6908da0ff65d34283f8f2d9e4abce75e2c1891210c398a->enter($__internal_fb5c9fae9f28beec6e6908da0ff65d34283f8f2d9e4abce75e2c1891210c398a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_c8459a825b4205782445f82ed54855f6e90a5e8053988b5ff1f42cba3aa31da8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8459a825b4205782445f82ed54855f6e90a5e8053988b5ff1f42cba3aa31da8->enter($__internal_c8459a825b4205782445f82ed54855f6e90a5e8053988b5ff1f42cba3aa31da8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 91
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " row"))));
            // line 95
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 96
                echo "            <div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">
        ";
            }
            // line 98
            echo "        ";
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                // line 99
                echo "            <div class=\"large-4 columns\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 106
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 116
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "second", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            } else {
                // line 121
                echo "            <div class=\"large-6 columns\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
                echo "</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        ";
                // line 128
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
                echo "
                    </div>
                </div>
            </div>
        ";
            }
            // line 133
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 134
                echo "            </div>
        ";
            }
            // line 136
            echo "    ";
        }
        
        $__internal_c8459a825b4205782445f82ed54855f6e90a5e8053988b5ff1f42cba3aa31da8->leave($__internal_c8459a825b4205782445f82ed54855f6e90a5e8053988b5ff1f42cba3aa31da8_prof);

        
        $__internal_fb5c9fae9f28beec6e6908da0ff65d34283f8f2d9e4abce75e2c1891210c398a->leave($__internal_fb5c9fae9f28beec6e6908da0ff65d34283f8f2d9e4abce75e2c1891210c398a_prof);

    }

    // line 139
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_3572055dd28593695d0d68df5ee7bf99ecb95994a36892f8bd19afa3e58d1f59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3572055dd28593695d0d68df5ee7bf99ecb95994a36892f8bd19afa3e58d1f59->enter($__internal_3572055dd28593695d0d68df5ee7bf99ecb95994a36892f8bd19afa3e58d1f59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_e4097231c01673b101852cb38abe4ac54573d33ed1d821e740ff92fce70f80cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4097231c01673b101852cb38abe4ac54573d33ed1d821e740ff92fce70f80cb->enter($__internal_e4097231c01673b101852cb38abe4ac54573d33ed1d821e740ff92fce70f80cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 140
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 141
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 142
            echo "    ";
        }
        // line 143
        echo "
    ";
        // line 144
        if (($context["multiple"] ?? $this->getContext($context, "multiple"))) {
            // line 145
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("style" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "style", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "style", array()), "")) : ("")) . " height: auto; background-image: none;"))));
            // line 146
            echo "    ";
        }
        // line 147
        echo "
    ";
        // line 148
        if ((((($context["required"] ?? $this->getContext($context, "required")) && (null === ($context["placeholder"] ?? $this->getContext($context, "placeholder")))) &&  !($context["placeholder_in_choices"] ?? $this->getContext($context, "placeholder_in_choices"))) &&  !($context["multiple"] ?? $this->getContext($context, "multiple")))) {
            // line 149
            $context["required"] = false;
        }
        // line 151
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (($context["multiple"] ?? $this->getContext($context, "multiple"))) {
            echo " multiple=\"multiple\" data-customforms=\"disabled\"";
        }
        echo ">
        ";
        // line 152
        if ( !(null === ($context["placeholder"] ?? $this->getContext($context, "placeholder")))) {
            // line 153
            echo "<option value=\"\"";
            if ((($context["required"] ?? $this->getContext($context, "required")) && twig_test_empty(($context["value"] ?? $this->getContext($context, "value"))))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["placeholder"] ?? $this->getContext($context, "placeholder"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["placeholder"] ?? $this->getContext($context, "placeholder")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain"))))), "html", null, true);
            echo "</option>";
        }
        // line 155
        if ((twig_length_filter($this->env, ($context["preferred_choices"] ?? $this->getContext($context, "preferred_choices"))) > 0)) {
            // line 156
            $context["options"] = ($context["preferred_choices"] ?? $this->getContext($context, "preferred_choices"));
            // line 157
            $this->displayBlock("choice_widget_options", $context, $blocks);
            // line 158
            if (((twig_length_filter($this->env, ($context["choices"] ?? $this->getContext($context, "choices"))) > 0) &&  !(null === ($context["separator"] ?? $this->getContext($context, "separator"))))) {
                // line 159
                echo "<option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, ($context["separator"] ?? $this->getContext($context, "separator")), "html", null, true);
                echo "</option>";
            }
        }
        // line 162
        $context["options"] = ($context["choices"] ?? $this->getContext($context, "choices"));
        // line 163
        $this->displayBlock("choice_widget_options", $context, $blocks);
        // line 164
        echo "</select>";
        
        $__internal_e4097231c01673b101852cb38abe4ac54573d33ed1d821e740ff92fce70f80cb->leave($__internal_e4097231c01673b101852cb38abe4ac54573d33ed1d821e740ff92fce70f80cb_prof);

        
        $__internal_3572055dd28593695d0d68df5ee7bf99ecb95994a36892f8bd19afa3e58d1f59->leave($__internal_3572055dd28593695d0d68df5ee7bf99ecb95994a36892f8bd19afa3e58d1f59_prof);

    }

    // line 167
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_0e79af113208c73a0d2b370e250a2da985ae22977b5ecd2d70b3c531e3ae3024 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e79af113208c73a0d2b370e250a2da985ae22977b5ecd2d70b3c531e3ae3024->enter($__internal_0e79af113208c73a0d2b370e250a2da985ae22977b5ecd2d70b3c531e3ae3024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_7e67206c3a90c41f6f2abe02025e1bf48b1d303de3d3f1c269e879b6720bcdc9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e67206c3a90c41f6f2abe02025e1bf48b1d303de3d3f1c269e879b6720bcdc9->enter($__internal_7e67206c3a90c41f6f2abe02025e1bf48b1d303de3d3f1c269e879b6720bcdc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 168
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 169
            echo "        <ul class=\"inline-list\">
            ";
            // line 170
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 171
                echo "                <li>";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 172
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
                // line 173
                echo "</li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "        </ul>
    ";
        } else {
            // line 177
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 178
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 179
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 180
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
                // line 181
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 183
            echo "        </div>
    ";
        }
        
        $__internal_7e67206c3a90c41f6f2abe02025e1bf48b1d303de3d3f1c269e879b6720bcdc9->leave($__internal_7e67206c3a90c41f6f2abe02025e1bf48b1d303de3d3f1c269e879b6720bcdc9_prof);

        
        $__internal_0e79af113208c73a0d2b370e250a2da985ae22977b5ecd2d70b3c531e3ae3024->leave($__internal_0e79af113208c73a0d2b370e250a2da985ae22977b5ecd2d70b3c531e3ae3024_prof);

    }

    // line 187
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_a5c904025dfc12ba91f0df94bce9e86c2f1d5211f5480a8b491c43073c6f22f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5c904025dfc12ba91f0df94bce9e86c2f1d5211f5480a8b491c43073c6f22f2->enter($__internal_a5c904025dfc12ba91f0df94bce9e86c2f1d5211f5480a8b491c43073c6f22f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_3cf30242eb1661d900518b7258e7b45d98ae1e7908f7885666c58b8e702f07e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cf30242eb1661d900518b7258e7b45d98ae1e7908f7885666c58b8e702f07e5->enter($__internal_3cf30242eb1661d900518b7258e7b45d98ae1e7908f7885666c58b8e702f07e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 188
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 189
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 190
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 191
            echo "    ";
        }
        // line 192
        echo "    ";
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 193
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 195
            echo "        <div class=\"checkbox\">
            ";
            // line 196
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_3cf30242eb1661d900518b7258e7b45d98ae1e7908f7885666c58b8e702f07e5->leave($__internal_3cf30242eb1661d900518b7258e7b45d98ae1e7908f7885666c58b8e702f07e5_prof);

        
        $__internal_a5c904025dfc12ba91f0df94bce9e86c2f1d5211f5480a8b491c43073c6f22f2->leave($__internal_a5c904025dfc12ba91f0df94bce9e86c2f1d5211f5480a8b491c43073c6f22f2_prof);

    }

    // line 201
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_1931fd1b8797264cc29a1ff1e331284cff701eeac09dc68e3c143a105d6983f1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1931fd1b8797264cc29a1ff1e331284cff701eeac09dc68e3c143a105d6983f1->enter($__internal_1931fd1b8797264cc29a1ff1e331284cff701eeac09dc68e3c143a105d6983f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_9eb7531a047817d39ca89a689e99e59a5312d348db7ede49011683aa422cddc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9eb7531a047817d39ca89a689e99e59a5312d348db7ede49011683aa422cddc8->enter($__internal_9eb7531a047817d39ca89a689e99e59a5312d348db7ede49011683aa422cddc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 202
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), "")) : (""));
        // line 203
        echo "    ";
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 204
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
    ";
        } else {
            // line 206
            echo "        ";
            if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
                // line 207
                $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
                // line 208
                echo "        ";
            }
            // line 209
            echo "        <div class=\"radio\">
            ";
            // line 210
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            echo "
        </div>
    ";
        }
        
        $__internal_9eb7531a047817d39ca89a689e99e59a5312d348db7ede49011683aa422cddc8->leave($__internal_9eb7531a047817d39ca89a689e99e59a5312d348db7ede49011683aa422cddc8_prof);

        
        $__internal_1931fd1b8797264cc29a1ff1e331284cff701eeac09dc68e3c143a105d6983f1->leave($__internal_1931fd1b8797264cc29a1ff1e331284cff701eeac09dc68e3c143a105d6983f1_prof);

    }

    // line 217
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_56837e6cfde3298a8efdc6ee57a4637186f87bbf2f3dd9e09e8656b217b6b6df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_56837e6cfde3298a8efdc6ee57a4637186f87bbf2f3dd9e09e8656b217b6b6df->enter($__internal_56837e6cfde3298a8efdc6ee57a4637186f87bbf2f3dd9e09e8656b217b6b6df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_1771c9602d2da66b79147912ae6ce964d1fcca2e95d3ddc94d784ab34cd19d95 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1771c9602d2da66b79147912ae6ce964d1fcca2e95d3ddc94d784ab34cd19d95->enter($__internal_1771c9602d2da66b79147912ae6ce964d1fcca2e95d3ddc94d784ab34cd19d95_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 218
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 219
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 220
            echo "    ";
        }
        // line 221
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_1771c9602d2da66b79147912ae6ce964d1fcca2e95d3ddc94d784ab34cd19d95->leave($__internal_1771c9602d2da66b79147912ae6ce964d1fcca2e95d3ddc94d784ab34cd19d95_prof);

        
        $__internal_56837e6cfde3298a8efdc6ee57a4637186f87bbf2f3dd9e09e8656b217b6b6df->leave($__internal_56837e6cfde3298a8efdc6ee57a4637186f87bbf2f3dd9e09e8656b217b6b6df_prof);

    }

    // line 224
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_fa9049c8b487fff30f77f57d6d78632d8ab8d77685ef5a71c6e3d7d426b383f2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa9049c8b487fff30f77f57d6d78632d8ab8d77685ef5a71c6e3d7d426b383f2->enter($__internal_fa9049c8b487fff30f77f57d6d78632d8ab8d77685ef5a71c6e3d7d426b383f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_c9d31cc0884b11fe9533046b79caa92b0f3e09a5a7b700ddf31e6ff3985bc615 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9d31cc0884b11fe9533046b79caa92b0f3e09a5a7b700ddf31e6ff3985bc615->enter($__internal_c9d31cc0884b11fe9533046b79caa92b0f3e09a5a7b700ddf31e6ff3985bc615_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 225
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 226
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 227
            echo "    ";
        }
        // line 228
        echo "    ";
        // line 229
        echo "    ";
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 230
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_c9d31cc0884b11fe9533046b79caa92b0f3e09a5a7b700ddf31e6ff3985bc615->leave($__internal_c9d31cc0884b11fe9533046b79caa92b0f3e09a5a7b700ddf31e6ff3985bc615_prof);

        
        $__internal_fa9049c8b487fff30f77f57d6d78632d8ab8d77685ef5a71c6e3d7d426b383f2->leave($__internal_fa9049c8b487fff30f77f57d6d78632d8ab8d77685ef5a71c6e3d7d426b383f2_prof);

    }

    // line 233
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_e5443b5fc7748ec8a7029e4e2a4e49536d7faad0ba60466cabe355d1242f42df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5443b5fc7748ec8a7029e4e2a4e49536d7faad0ba60466cabe355d1242f42df->enter($__internal_e5443b5fc7748ec8a7029e4e2a4e49536d7faad0ba60466cabe355d1242f42df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_2a88d3ddbe1a8f4cec742d220f252f2f3715ac6aea4bd33f2aac42a142a3501e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a88d3ddbe1a8f4cec742d220f252f2f3715ac6aea4bd33f2aac42a142a3501e->enter($__internal_2a88d3ddbe1a8f4cec742d220f252f2f3715ac6aea4bd33f2aac42a142a3501e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 234
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_2a88d3ddbe1a8f4cec742d220f252f2f3715ac6aea4bd33f2aac42a142a3501e->leave($__internal_2a88d3ddbe1a8f4cec742d220f252f2f3715ac6aea4bd33f2aac42a142a3501e_prof);

        
        $__internal_e5443b5fc7748ec8a7029e4e2a4e49536d7faad0ba60466cabe355d1242f42df->leave($__internal_e5443b5fc7748ec8a7029e4e2a4e49536d7faad0ba60466cabe355d1242f42df_prof);

    }

    // line 237
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_e9d6b7393a7427da9ff8e083b23a2ad3e753f38b658f60c03445ef1489b04ef3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9d6b7393a7427da9ff8e083b23a2ad3e753f38b658f60c03445ef1489b04ef3->enter($__internal_e9d6b7393a7427da9ff8e083b23a2ad3e753f38b658f60c03445ef1489b04ef3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_37828e7ed8be0c883d74a9301a57cef81b009de032aa15528f858bec67aabfc3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37828e7ed8be0c883d74a9301a57cef81b009de032aa15528f858bec67aabfc3->enter($__internal_37828e7ed8be0c883d74a9301a57cef81b009de032aa15528f858bec67aabfc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 238
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_37828e7ed8be0c883d74a9301a57cef81b009de032aa15528f858bec67aabfc3->leave($__internal_37828e7ed8be0c883d74a9301a57cef81b009de032aa15528f858bec67aabfc3_prof);

        
        $__internal_e9d6b7393a7427da9ff8e083b23a2ad3e753f38b658f60c03445ef1489b04ef3->leave($__internal_e9d6b7393a7427da9ff8e083b23a2ad3e753f38b658f60c03445ef1489b04ef3_prof);

    }

    // line 241
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_3a85252a7f6fb9605c0ecb53b59051d1ecdf777e3d4894f31b40227cac20fa47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a85252a7f6fb9605c0ecb53b59051d1ecdf777e3d4894f31b40227cac20fa47->enter($__internal_3a85252a7f6fb9605c0ecb53b59051d1ecdf777e3d4894f31b40227cac20fa47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_79894f31ee0fee99fac1640344cb0b81e7e1ad2557b761fe8f3ea972b5ec0643 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_79894f31ee0fee99fac1640344cb0b81e7e1ad2557b761fe8f3ea972b5ec0643->enter($__internal_79894f31ee0fee99fac1640344cb0b81e7e1ad2557b761fe8f3ea972b5ec0643_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 242
        if (($context["required"] ?? $this->getContext($context, "required"))) {
            // line 243
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
            // line 244
            echo "    ";
        }
        // line 245
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 246
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " error"))));
            // line 247
            echo "    ";
        }
        // line 248
        echo "    ";
        if (array_key_exists("parent_label_class", $context)) {
            // line 249
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
            // line 250
            echo "    ";
        }
        // line 251
        echo "    ";
        if (twig_test_empty(($context["label"] ?? $this->getContext($context, "label")))) {
            // line 252
            if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                // line 253
                $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                 // line 254
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                 // line 255
($context["id"] ?? $this->getContext($context, "id"))));
            } else {
                // line 258
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
            }
        }
        // line 261
        echo "    <label";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
        ";
        // line 262
        echo ($context["widget"] ?? $this->getContext($context, "widget"));
        echo "
        ";
        // line 263
        echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain"))))), "html", null, true);
        echo "
    </label>";
        
        $__internal_79894f31ee0fee99fac1640344cb0b81e7e1ad2557b761fe8f3ea972b5ec0643->leave($__internal_79894f31ee0fee99fac1640344cb0b81e7e1ad2557b761fe8f3ea972b5ec0643_prof);

        
        $__internal_3a85252a7f6fb9605c0ecb53b59051d1ecdf777e3d4894f31b40227cac20fa47->leave($__internal_3a85252a7f6fb9605c0ecb53b59051d1ecdf777e3d4894f31b40227cac20fa47_prof);

    }

    // line 269
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_b989c65a30e4c22372926b929ca085a089fff38a1eba8e62fb7bbb81590f109f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b989c65a30e4c22372926b929ca085a089fff38a1eba8e62fb7bbb81590f109f->enter($__internal_b989c65a30e4c22372926b929ca085a089fff38a1eba8e62fb7bbb81590f109f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_406b0e2a0c129dc31c146476438b967aaf7923e291f4599b886e369ab182be75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_406b0e2a0c129dc31c146476438b967aaf7923e291f4599b886e369ab182be75->enter($__internal_406b0e2a0c129dc31c146476438b967aaf7923e291f4599b886e369ab182be75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 270
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 271
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " error";
        }
        echo "\">
            ";
        // line 272
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        echo "
            ";
        // line 273
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 274
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_406b0e2a0c129dc31c146476438b967aaf7923e291f4599b886e369ab182be75->leave($__internal_406b0e2a0c129dc31c146476438b967aaf7923e291f4599b886e369ab182be75_prof);

        
        $__internal_b989c65a30e4c22372926b929ca085a089fff38a1eba8e62fb7bbb81590f109f->leave($__internal_b989c65a30e4c22372926b929ca085a089fff38a1eba8e62fb7bbb81590f109f_prof);

    }

    // line 279
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_74c87889974d561cde0a546bd59f79e6adc3f161a430c9898f49d9e6c1374a8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74c87889974d561cde0a546bd59f79e6adc3f161a430c9898f49d9e6c1374a8a->enter($__internal_74c87889974d561cde0a546bd59f79e6adc3f161a430c9898f49d9e6c1374a8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_6981979ab24657427f7c9524b2d6979d34ce8d1bd11b1cf43f81e1cb07363f5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6981979ab24657427f7c9524b2d6979d34ce8d1bd11b1cf43f81e1cb07363f5d->enter($__internal_6981979ab24657427f7c9524b2d6979d34ce8d1bd11b1cf43f81e1cb07363f5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 280
        $context["force_error"] = true;
        // line 281
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_6981979ab24657427f7c9524b2d6979d34ce8d1bd11b1cf43f81e1cb07363f5d->leave($__internal_6981979ab24657427f7c9524b2d6979d34ce8d1bd11b1cf43f81e1cb07363f5d_prof);

        
        $__internal_74c87889974d561cde0a546bd59f79e6adc3f161a430c9898f49d9e6c1374a8a->leave($__internal_74c87889974d561cde0a546bd59f79e6adc3f161a430c9898f49d9e6c1374a8a_prof);

    }

    // line 284
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_1cbcd78a3eb23160ede0a0ce8fe9d3db8455635bf39fab9e463e5aa3d25603fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1cbcd78a3eb23160ede0a0ce8fe9d3db8455635bf39fab9e463e5aa3d25603fb->enter($__internal_1cbcd78a3eb23160ede0a0ce8fe9d3db8455635bf39fab9e463e5aa3d25603fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_2152db694446fc5cb6e715bde4c912fb0b637148de170935c24e190f94135d3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2152db694446fc5cb6e715bde4c912fb0b637148de170935c24e190f94135d3f->enter($__internal_2152db694446fc5cb6e715bde4c912fb0b637148de170935c24e190f94135d3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 285
        $context["force_error"] = true;
        // line 286
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_2152db694446fc5cb6e715bde4c912fb0b637148de170935c24e190f94135d3f->leave($__internal_2152db694446fc5cb6e715bde4c912fb0b637148de170935c24e190f94135d3f_prof);

        
        $__internal_1cbcd78a3eb23160ede0a0ce8fe9d3db8455635bf39fab9e463e5aa3d25603fb->leave($__internal_1cbcd78a3eb23160ede0a0ce8fe9d3db8455635bf39fab9e463e5aa3d25603fb_prof);

    }

    // line 289
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_bf5a41aa432760da013840aa859d38e5ed61f8f9f763cccf7919a876d204138f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf5a41aa432760da013840aa859d38e5ed61f8f9f763cccf7919a876d204138f->enter($__internal_bf5a41aa432760da013840aa859d38e5ed61f8f9f763cccf7919a876d204138f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_05f56056212c0ef6f3bdffb0305dd323900d57c2f2a3d2a8306d77d444457d54 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05f56056212c0ef6f3bdffb0305dd323900d57c2f2a3d2a8306d77d444457d54->enter($__internal_05f56056212c0ef6f3bdffb0305dd323900d57c2f2a3d2a8306d77d444457d54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 290
        $context["force_error"] = true;
        // line 291
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_05f56056212c0ef6f3bdffb0305dd323900d57c2f2a3d2a8306d77d444457d54->leave($__internal_05f56056212c0ef6f3bdffb0305dd323900d57c2f2a3d2a8306d77d444457d54_prof);

        
        $__internal_bf5a41aa432760da013840aa859d38e5ed61f8f9f763cccf7919a876d204138f->leave($__internal_bf5a41aa432760da013840aa859d38e5ed61f8f9f763cccf7919a876d204138f_prof);

    }

    // line 294
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_72f0402d49544657db32e819578cb6af8adda728512e6a0d4bcde41e79bd08f8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72f0402d49544657db32e819578cb6af8adda728512e6a0d4bcde41e79bd08f8->enter($__internal_72f0402d49544657db32e819578cb6af8adda728512e6a0d4bcde41e79bd08f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_bd6e8a583142444050e2deb452573f8bee5ac4d737686c85719e02d161598930 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd6e8a583142444050e2deb452573f8bee5ac4d737686c85719e02d161598930->enter($__internal_bd6e8a583142444050e2deb452573f8bee5ac4d737686c85719e02d161598930_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 295
        $context["force_error"] = true;
        // line 296
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_bd6e8a583142444050e2deb452573f8bee5ac4d737686c85719e02d161598930->leave($__internal_bd6e8a583142444050e2deb452573f8bee5ac4d737686c85719e02d161598930_prof);

        
        $__internal_72f0402d49544657db32e819578cb6af8adda728512e6a0d4bcde41e79bd08f8->leave($__internal_72f0402d49544657db32e819578cb6af8adda728512e6a0d4bcde41e79bd08f8_prof);

    }

    // line 299
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_0b8f78444b20b02cde8d3b74d99880f20377dc77b01d3288a8068506bef64043 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b8f78444b20b02cde8d3b74d99880f20377dc77b01d3288a8068506bef64043->enter($__internal_0b8f78444b20b02cde8d3b74d99880f20377dc77b01d3288a8068506bef64043_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_f266a64f3566e6ff819a1ac61654f8888ca89f442c420906bc4353feb3d2d052 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f266a64f3566e6ff819a1ac61654f8888ca89f442c420906bc4353feb3d2d052->enter($__internal_f266a64f3566e6ff819a1ac61654f8888ca89f442c420906bc4353feb3d2d052_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 300
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 301
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 302
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 303
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_f266a64f3566e6ff819a1ac61654f8888ca89f442c420906bc4353feb3d2d052->leave($__internal_f266a64f3566e6ff819a1ac61654f8888ca89f442c420906bc4353feb3d2d052_prof);

        
        $__internal_0b8f78444b20b02cde8d3b74d99880f20377dc77b01d3288a8068506bef64043->leave($__internal_0b8f78444b20b02cde8d3b74d99880f20377dc77b01d3288a8068506bef64043_prof);

    }

    // line 308
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_4ced59f37737657afad3ceaaff14d4830421c1cb8e63534a21ecc49188d04b04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ced59f37737657afad3ceaaff14d4830421c1cb8e63534a21ecc49188d04b04->enter($__internal_4ced59f37737657afad3ceaaff14d4830421c1cb8e63534a21ecc49188d04b04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_23c687a1fcf35f85048324a0d694b7aff98268f1eeb0a704314258686255e714 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23c687a1fcf35f85048324a0d694b7aff98268f1eeb0a704314258686255e714->enter($__internal_23c687a1fcf35f85048324a0d694b7aff98268f1eeb0a704314258686255e714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 309
        echo "<div class=\"row\">
        <div class=\"large-12 columns";
        // line 310
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " error";
        }
        echo "\">
            ";
        // line 311
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
            ";
        // line 312
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        echo "
        </div>
    </div>";
        
        $__internal_23c687a1fcf35f85048324a0d694b7aff98268f1eeb0a704314258686255e714->leave($__internal_23c687a1fcf35f85048324a0d694b7aff98268f1eeb0a704314258686255e714_prof);

        
        $__internal_4ced59f37737657afad3ceaaff14d4830421c1cb8e63534a21ecc49188d04b04->leave($__internal_4ced59f37737657afad3ceaaff14d4830421c1cb8e63534a21ecc49188d04b04_prof);

    }

    // line 319
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_ab1ff8a4db3220ff04a84e68d4ba5b7c9419007524531266fc78dc7c394c149d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab1ff8a4db3220ff04a84e68d4ba5b7c9419007524531266fc78dc7c394c149d->enter($__internal_ab1ff8a4db3220ff04a84e68d4ba5b7c9419007524531266fc78dc7c394c149d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_7dd4de633d15df1a714eaf95b233b2f4b12eb8e0d8fc439d13e9627624015254 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7dd4de633d15df1a714eaf95b233b2f4b12eb8e0d8fc439d13e9627624015254->enter($__internal_7dd4de633d15df1a714eaf95b233b2f4b12eb8e0d8fc439d13e9627624015254_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 320
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 321
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<small class=\"error\">";
            } else {
                echo "<div data-alert class=\"alert-box alert\">";
            }
            // line 322
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 323
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "
            ";
                // line 324
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 326
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</small>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_7dd4de633d15df1a714eaf95b233b2f4b12eb8e0d8fc439d13e9627624015254->leave($__internal_7dd4de633d15df1a714eaf95b233b2f4b12eb8e0d8fc439d13e9627624015254_prof);

        
        $__internal_ab1ff8a4db3220ff04a84e68d4ba5b7c9419007524531266fc78dc7c394c149d->leave($__internal_ab1ff8a4db3220ff04a84e68d4ba5b7c9419007524531266fc78dc7c394c149d_prof);

    }

    public function getTemplateName()
    {
        return "foundation_5_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1078 => 326,  1062 => 324,  1058 => 323,  1041 => 322,  1035 => 321,  1033 => 320,  1024 => 319,  1011 => 312,  1007 => 311,  1001 => 310,  998 => 309,  989 => 308,  976 => 303,  972 => 302,  966 => 301,  963 => 300,  954 => 299,  943 => 296,  941 => 295,  932 => 294,  921 => 291,  919 => 290,  910 => 289,  899 => 286,  897 => 285,  888 => 284,  877 => 281,  875 => 280,  866 => 279,  853 => 274,  849 => 273,  845 => 272,  839 => 271,  836 => 270,  827 => 269,  815 => 263,  811 => 262,  795 => 261,  791 => 258,  788 => 255,  787 => 254,  786 => 253,  784 => 252,  781 => 251,  778 => 250,  775 => 249,  772 => 248,  769 => 247,  767 => 246,  764 => 245,  761 => 244,  758 => 243,  756 => 242,  747 => 241,  737 => 238,  728 => 237,  718 => 234,  709 => 233,  699 => 230,  696 => 229,  694 => 228,  691 => 227,  689 => 226,  687 => 225,  678 => 224,  668 => 221,  665 => 220,  663 => 219,  661 => 218,  652 => 217,  638 => 210,  635 => 209,  632 => 208,  630 => 207,  627 => 206,  621 => 204,  618 => 203,  616 => 202,  607 => 201,  593 => 196,  590 => 195,  584 => 193,  581 => 192,  578 => 191,  576 => 190,  573 => 189,  571 => 188,  562 => 187,  550 => 183,  543 => 181,  541 => 180,  539 => 179,  535 => 178,  530 => 177,  526 => 175,  519 => 173,  517 => 172,  515 => 171,  511 => 170,  508 => 169,  506 => 168,  497 => 167,  487 => 164,  485 => 163,  483 => 162,  477 => 159,  475 => 158,  473 => 157,  471 => 156,  469 => 155,  460 => 153,  458 => 152,  450 => 151,  447 => 149,  445 => 148,  442 => 147,  439 => 146,  437 => 145,  435 => 144,  432 => 143,  429 => 142,  427 => 141,  425 => 140,  416 => 139,  405 => 136,  401 => 134,  398 => 133,  390 => 128,  379 => 121,  371 => 116,  358 => 106,  347 => 99,  344 => 98,  338 => 96,  335 => 95,  332 => 94,  329 => 92,  327 => 91,  318 => 90,  307 => 87,  303 => 85,  301 => 84,  299 => 82,  298 => 81,  297 => 80,  296 => 79,  290 => 77,  287 => 76,  284 => 75,  281 => 73,  279 => 72,  270 => 71,  256 => 66,  252 => 65,  248 => 64,  243 => 62,  239 => 61,  236 => 60,  233 => 59,  230 => 57,  228 => 56,  219 => 55,  205 => 48,  203 => 47,  200 => 45,  191 => 44,  181 => 41,  175 => 38,  172 => 37,  170 => 36,  167 => 35,  165 => 34,  163 => 33,  157 => 30,  154 => 29,  151 => 28,  149 => 27,  146 => 26,  137 => 25,  127 => 22,  125 => 21,  116 => 20,  106 => 17,  103 => 16,  101 => 15,  99 => 14,  90 => 13,  80 => 10,  77 => 9,  75 => 8,  73 => 7,  64 => 6,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"form_div_layout.html.twig\" %}

{# Based on Foundation 5 Doc #}
{# Widgets #}

{% block form_widget_simple -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' button')|trim}) %}
    {{- parent() -}}
{%- endblock button_widget %}

{% block money_widget -%}
    <div class=\"row collapse\">
        {% set prepend = '{{' == money_pattern[0:2] %}
        {% if not prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"prefix\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
            </div>
        {% endif %}
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        {% if prepend %}
            <div class=\"small-3 large-2 columns\">
                <span class=\"postfix\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
            </div>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"row collapse\">
        <div class=\"small-9 large-10 columns\">
            {{- block('form_widget_simple') -}}
        </div>
        <div class=\"small-3 large-2 columns\">
            <span class=\"postfix\">%</span>
        </div>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        <div class=\"row\">
            <div class=\"large-7 columns\">{{ form_errors(form.date) }}</div>
            <div class=\"large-5 columns\">{{ form_errors(form.time) }}</div>
        </div>
        <div {{ block('widget_container_attributes') }}>
            <div class=\"large-7 columns\">{{ form_widget(form.date, { datetime: true } ) }}</div>
            <div class=\"large-5 columns\">{{ form_widget(form.time, { datetime: true } ) }}</div>
        </div>
    {% endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or not datetime %}
            <div {{ block('widget_container_attributes') }}>
        {% endif %}
        {{- date_pattern|replace({
            '{{ year }}': '<div class=\"large-4 columns\">' ~ form_widget(form.year) ~ '</div>',
            '{{ month }}': '<div class=\"large-4 columns\">' ~ form_widget(form.month) ~ '</div>',
            '{{ day }}': '<div class=\"large-4 columns\">' ~ form_widget(form.day) ~ '</div>',
        })|raw -}}
        {% if datetime is not defined or not datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' row')|trim}) %}
        {% if datetime is not defined or false == datetime %}
            <div {{ block('widget_container_attributes') -}}>
        {% endif %}
        {% if with_seconds %}
            <div class=\"large-4 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
            <div class=\"large-4 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.second) }}
                    </div>
                </div>
            </div>
        {% else %}
            <div class=\"large-6 columns\">{{ form_widget(form.hour) }}</div>
            <div class=\"large-6 columns\">
                <div class=\"row collapse\">
                    <div class=\"small-3 large-2 columns\">
                        <span class=\"prefix\">:</span>
                    </div>
                    <div class=\"small-9 large-10 columns\">
                        {{ form_widget(form.minute) }}
                    </div>
                </div>
            </div>
        {% endif %}
        {% if datetime is not defined or false == datetime %}
            </div>
        {% endif %}
    {% endif %}
{%- endblock time_widget %}

{% block choice_widget_collapsed -%}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}

    {% if multiple -%}
        {% set attr = attr|merge({style: (attr.style|default('') ~ ' height: auto; background-image: none;')|trim}) %}
    {% endif %}

    {% if required and placeholder is none and not placeholder_in_choices and not multiple -%}
        {% set required = false %}
    {%- endif -%}
    <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\" data-customforms=\"disabled\"{% endif %}>
        {% if placeholder is not none -%}
            <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>{{ translation_domain is same as(false) ? placeholder : placeholder|trans({}, translation_domain) }}</option>
        {%- endif %}
        {%- if preferred_choices|length > 0 -%}
            {% set options = preferred_choices %}
            {{- block('choice_widget_options') -}}
            {% if choices|length > 0 and separator is not none -%}
                <option disabled=\"disabled\">{{ separator }}</option>
            {%- endif %}
        {%- endif -%}
        {% set options = choices -%}
        {{- block('choice_widget_options') -}}
    </select>
{%- endblock choice_widget_collapsed %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') %}
        <ul class=\"inline-list\">
            {% for child in form %}
                <li>{{ form_widget(child, {
                        parent_label_class: label_attr.class|default(''),
                    }) }}</li>
            {% endfor %}
        </ul>
    {% else %}
        <div {{ block('widget_container_attributes') }}>
            {% for child in form %}
                {{ form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                }) }}
            {% endfor %}
        </div>
    {% endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if errors|length > 0 -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if 'checkbox-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        <div class=\"checkbox\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {% set parent_label_class = parent_label_class|default('') %}
    {% if 'radio-inline' in parent_label_class %}
        {{ form_label(form, null, { widget: parent() }) }}
    {% else %}
        {% if errors|length > 0 -%}
            {% set attr = attr|merge({class: (attr.class|default('') ~ ' error')|trim}) %}
        {% endif %}
        <div class=\"radio\">
            {{ form_label(form, null, { widget: parent() }) }}
        </div>
    {% endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {% set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) %}
    {{- block('form_label') -}}
{%- endblock choice_label %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label -%}
    {% if required %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
    {% endif %}
    {% if errors|length > 0 -%}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' error')|trim}) %}
    {% endif %}
    {% if parent_label_class is defined %}
        {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ parent_label_class)|trim}) %}
    {% endif %}
    {% if label is empty %}
        {%- if label_format is not empty -%}
            {% set label = label_format|replace({
                '%name%': name,
                '%id%': id,
            }) %}
        {%- else -%}
            {% set label = name|humanize %}
        {%- endif -%}
    {% endif %}
    <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
        {{ widget|raw }}
        {{ translation_domain is same as(false) ? label : label|trans({}, translation_domain) }}
    </label>
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if (not compound or force_error|default(false)) and not valid %} error{% endif %}\">
            {{ form_label(form) }}
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock form_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{ block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"row\">
        <div class=\"large-12 columns{% if not valid %} error{% endif %}\">
            {{ form_widget(form) }}
            {{ form_errors(form) }}
        </div>
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<small class=\"error\">{% else %}<div data-alert class=\"alert-box alert\">{% endif %}
        {%- for error in errors -%}
            {{ error.message }}
            {% if not loop.last %}, {% endif %}
        {%- endfor -%}
        {% if form.parent %}</small>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "foundation_5_layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\foundation_5_layout.html.twig");
    }
}
