<?php

/* @WebProfiler/Profiler/info.html.twig */
class __TwigTemplate_89c2c37e811f517fe402f6e7ce33e4f2f12b3d88378b40055913e8aa1ebba8d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Profiler/info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f9df56eb6103c9dbd37e1fc12591e2a095dc4fa564dcac087b00bacce8aeb15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f9df56eb6103c9dbd37e1fc12591e2a095dc4fa564dcac087b00bacce8aeb15->enter($__internal_5f9df56eb6103c9dbd37e1fc12591e2a095dc4fa564dcac087b00bacce8aeb15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        $__internal_39c5a708cf8ff61c9b9d3e0e4ce5218df041360f74f41faa98876d00d62ae88e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39c5a708cf8ff61c9b9d3e0e4ce5218df041360f74f41faa98876d00d62ae88e->enter($__internal_39c5a708cf8ff61c9b9d3e0e4ce5218df041360f74f41faa98876d00d62ae88e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5f9df56eb6103c9dbd37e1fc12591e2a095dc4fa564dcac087b00bacce8aeb15->leave($__internal_5f9df56eb6103c9dbd37e1fc12591e2a095dc4fa564dcac087b00bacce8aeb15_prof);

        
        $__internal_39c5a708cf8ff61c9b9d3e0e4ce5218df041360f74f41faa98876d00d62ae88e->leave($__internal_39c5a708cf8ff61c9b9d3e0e4ce5218df041360f74f41faa98876d00d62ae88e_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_e283f06dada79bf4d87f73def384b82e09da248c6c69a344b69d3cbe5cf7fa94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e283f06dada79bf4d87f73def384b82e09da248c6c69a344b69d3cbe5cf7fa94->enter($__internal_e283f06dada79bf4d87f73def384b82e09da248c6c69a344b69d3cbe5cf7fa94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_fa57e031f68c66f69781cb9fd03e31549add363adf6bc10cfb2e4bf68a0bf755 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa57e031f68c66f69781cb9fd03e31549add363adf6bc10cfb2e4bf68a0bf755->enter($__internal_fa57e031f68c66f69781cb9fd03e31549add363adf6bc10cfb2e4bf68a0bf755_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_fa57e031f68c66f69781cb9fd03e31549add363adf6bc10cfb2e4bf68a0bf755->leave($__internal_fa57e031f68c66f69781cb9fd03e31549add363adf6bc10cfb2e4bf68a0bf755_prof);

        
        $__internal_e283f06dada79bf4d87f73def384b82e09da248c6c69a344b69d3cbe5cf7fa94->leave($__internal_e283f06dada79bf4d87f73def384b82e09da248c6c69a344b69d3cbe5cf7fa94_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d89264f01127cdde65412376cddb0957c82f51db6501d63f601316b00900124a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d89264f01127cdde65412376cddb0957c82f51db6501d63f601316b00900124a->enter($__internal_d89264f01127cdde65412376cddb0957c82f51db6501d63f601316b00900124a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_9c348a44ce903a690786cde89ed2270ab6bea7acda1987512ac67cf8757a520e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c348a44ce903a690786cde89ed2270ab6bea7acda1987512ac67cf8757a520e->enter($__internal_9c348a44ce903a690786cde89ed2270ab6bea7acda1987512ac67cf8757a520e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_9c348a44ce903a690786cde89ed2270ab6bea7acda1987512ac67cf8757a520e->leave($__internal_9c348a44ce903a690786cde89ed2270ab6bea7acda1987512ac67cf8757a520e_prof);

        
        $__internal_d89264f01127cdde65412376cddb0957c82f51db6501d63f601316b00900124a->leave($__internal_d89264f01127cdde65412376cddb0957c82f51db6501d63f601316b00900124a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "@WebProfiler/Profiler/info.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\info.html.twig");
    }
}
