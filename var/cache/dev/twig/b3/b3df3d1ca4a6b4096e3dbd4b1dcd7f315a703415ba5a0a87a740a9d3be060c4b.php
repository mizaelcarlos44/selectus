<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_1a986333b094b87a5e8b136b3a1590ecbde6e661346357fede5bf1e61240b198 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_20ca2e978da0dea71328b70c9e81c2ac1b5e987f5f7e4be4bc7998fa18412907 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20ca2e978da0dea71328b70c9e81c2ac1b5e987f5f7e4be4bc7998fa18412907->enter($__internal_20ca2e978da0dea71328b70c9e81c2ac1b5e987f5f7e4be4bc7998fa18412907_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_d78629a052280619ea0cea14a1d0b0b8a8dd31c073802f0decd6b6fc58e02318 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d78629a052280619ea0cea14a1d0b0b8a8dd31c073802f0decd6b6fc58e02318->enter($__internal_d78629a052280619ea0cea14a1d0b0b8a8dd31c073802f0decd6b6fc58e02318_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_20ca2e978da0dea71328b70c9e81c2ac1b5e987f5f7e4be4bc7998fa18412907->leave($__internal_20ca2e978da0dea71328b70c9e81c2ac1b5e987f5f7e4be4bc7998fa18412907_prof);

        
        $__internal_d78629a052280619ea0cea14a1d0b0b8a8dd31c073802f0decd6b6fc58e02318->leave($__internal_d78629a052280619ea0cea14a1d0b0b8a8dd31c073802f0decd6b6fc58e02318_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\number_widget.html.php");
    }
}
