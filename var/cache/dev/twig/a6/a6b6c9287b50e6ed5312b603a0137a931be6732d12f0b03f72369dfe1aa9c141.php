<?php

/* inscricao/new.html.twig */
class __TwigTemplate_706f3a1453f7797785fb36fbf0528d8a543a6ed863a681fe4f16b52e04ef0766 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "inscricao/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f1d0d1ccfb9cd1d2fc4c4db5917f68410e64c5a85159864b99c39dfaaf60a14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f1d0d1ccfb9cd1d2fc4c4db5917f68410e64c5a85159864b99c39dfaaf60a14->enter($__internal_5f1d0d1ccfb9cd1d2fc4c4db5917f68410e64c5a85159864b99c39dfaaf60a14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/new.html.twig"));

        $__internal_6d519b1180615809c24ae98a2151dfb8ca2f1242b5ba086ecc5349a3bd1da78a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d519b1180615809c24ae98a2151dfb8ca2f1242b5ba086ecc5349a3bd1da78a->enter($__internal_6d519b1180615809c24ae98a2151dfb8ca2f1242b5ba086ecc5349a3bd1da78a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5f1d0d1ccfb9cd1d2fc4c4db5917f68410e64c5a85159864b99c39dfaaf60a14->leave($__internal_5f1d0d1ccfb9cd1d2fc4c4db5917f68410e64c5a85159864b99c39dfaaf60a14_prof);

        
        $__internal_6d519b1180615809c24ae98a2151dfb8ca2f1242b5ba086ecc5349a3bd1da78a->leave($__internal_6d519b1180615809c24ae98a2151dfb8ca2f1242b5ba086ecc5349a3bd1da78a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c245a231d8fa35b55c25ef2568c00a94459d644e923b713f9df7a079b4dbec98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c245a231d8fa35b55c25ef2568c00a94459d644e923b713f9df7a079b4dbec98->enter($__internal_c245a231d8fa35b55c25ef2568c00a94459d644e923b713f9df7a079b4dbec98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d0b58f2a060dd4f43db0687605b9d50b5f72d2e8790cbfd8734aafc2c71f878c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0b58f2a060dd4f43db0687605b9d50b5f72d2e8790cbfd8734aafc2c71f878c->enter($__internal_d0b58f2a060dd4f43db0687605b9d50b5f72d2e8790cbfd8734aafc2c71f878c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Cadastrar inscrição</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        echo "
        <input class=\"btn btn-success\"  type=\"submit\" value=\"Salvar\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

";
        
        $__internal_d0b58f2a060dd4f43db0687605b9d50b5f72d2e8790cbfd8734aafc2c71f878c->leave($__internal_d0b58f2a060dd4f43db0687605b9d50b5f72d2e8790cbfd8734aafc2c71f878c_prof);

        
        $__internal_c245a231d8fa35b55c25ef2568c00a94459d644e923b713f9df7a079b4dbec98->leave($__internal_c245a231d8fa35b55c25ef2568c00a94459d644e923b713f9df7a079b4dbec98_prof);

    }

    public function getTemplateName()
    {
        return "inscricao/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Cadastrar inscrição</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input class=\"btn btn-success\"  type=\"submit\" value=\"Salvar\" />
    {{ form_end(form) }}

{% endblock %}
", "inscricao/new.html.twig", "C:\\wamp64\\www\\selectus\\app\\Resources\\views\\inscricao\\new.html.twig");
    }
}
