<?php

/* @WebProfiler/Collector/exception.css.twig */
class __TwigTemplate_44f3b3633ca2ed283bb608ad0c7448d62d8a870d640bbc742586e5411268b51b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_336da3bb4ecadd3079e044d88c9f3f44f0837e9db87a65f0dd13c02eb4a81c53 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_336da3bb4ecadd3079e044d88c9f3f44f0837e9db87a65f0dd13c02eb4a81c53->enter($__internal_336da3bb4ecadd3079e044d88c9f3f44f0837e9db87a65f0dd13c02eb4a81c53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.css.twig"));

        $__internal_feaa1a7ec0a244381657518bb8e590bfc5c429918b6c258e606e49be80b9a0a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_feaa1a7ec0a244381657518bb8e590bfc5c429918b6c258e606e49be80b9a0a1->enter($__internal_feaa1a7ec0a244381657518bb8e590bfc5c429918b6c258e606e49be80b9a0a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.css.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
";
        
        $__internal_336da3bb4ecadd3079e044d88c9f3f44f0837e9db87a65f0dd13c02eb4a81c53->leave($__internal_336da3bb4ecadd3079e044d88c9f3f44f0837e9db87a65f0dd13c02eb4a81c53_prof);

        
        $__internal_feaa1a7ec0a244381657518bb8e590bfc5c429918b6c258e606e49be80b9a0a1->leave($__internal_feaa1a7ec0a244381657518bb8e590bfc5c429918b6c258e606e49be80b9a0a1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/exception.css.twig') }}

.container {
    max-width: auto;
    margin: 0;
    padding: 0;
}
.container .container {
    padding: 0;
}

.exception-summary {
    background: #FFF;
    border: 1px solid #E0E0E0;
    box-shadow: 0 0 1px rgba(128, 128, 128, .2);
    margin: 1em 0;
    padding: 10px;
}
.exception-summary.exception-without-message {
    display: none;
}

.exception-message {
    color: #B0413E;
}

.exception-metadata,
.exception-illustration {
    display: none;
}

.exception-message-wrapper .container {
    min-height: auto;
}
", "@WebProfiler/Collector/exception.css.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.css.twig");
    }
}
