<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_3c0ae02049e09568dd4caa191fe4cca5c4bb1803dedb579cb689d27449e2dba2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c441d652e6113cbf4c3f82a9bfe11d0e9367e906e7e1c13a49925b93788b8ec3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c441d652e6113cbf4c3f82a9bfe11d0e9367e906e7e1c13a49925b93788b8ec3->enter($__internal_c441d652e6113cbf4c3f82a9bfe11d0e9367e906e7e1c13a49925b93788b8ec3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_99e61466fd20886a23326b2ef241646a619db4f6e95fcaec21e8e7d1ad233fa0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99e61466fd20886a23326b2ef241646a619db4f6e95fcaec21e8e7d1ad233fa0->enter($__internal_99e61466fd20886a23326b2ef241646a619db4f6e95fcaec21e8e7d1ad233fa0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_c441d652e6113cbf4c3f82a9bfe11d0e9367e906e7e1c13a49925b93788b8ec3->leave($__internal_c441d652e6113cbf4c3f82a9bfe11d0e9367e906e7e1c13a49925b93788b8ec3_prof);

        
        $__internal_99e61466fd20886a23326b2ef241646a619db4f6e95fcaec21e8e7d1ad233fa0->leave($__internal_99e61466fd20886a23326b2ef241646a619db4f6e95fcaec21e8e7d1ad233fa0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\hidden_row.html.php");
    }
}
