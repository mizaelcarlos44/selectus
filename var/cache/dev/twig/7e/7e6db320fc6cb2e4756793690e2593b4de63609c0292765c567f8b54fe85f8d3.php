<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_cd01f0532a053df441c0291a483537c5b39dae9131dea8f326950f7bc65f147f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d7bd1078abead80faf928b8d21174c6dd63a287a8fb818358a7823c69b62689 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d7bd1078abead80faf928b8d21174c6dd63a287a8fb818358a7823c69b62689->enter($__internal_1d7bd1078abead80faf928b8d21174c6dd63a287a8fb818358a7823c69b62689_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        $__internal_35fdb65bc1d5e3187ccb82829621c783dccd51ea67e5715e150bbe975ecbb686 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35fdb65bc1d5e3187ccb82829621c783dccd51ea67e5715e150bbe975ecbb686->enter($__internal_35fdb65bc1d5e3187ccb82829621c783dccd51ea67e5715e150bbe975ecbb686_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_1d7bd1078abead80faf928b8d21174c6dd63a287a8fb818358a7823c69b62689->leave($__internal_1d7bd1078abead80faf928b8d21174c6dd63a287a8fb818358a7823c69b62689_prof);

        
        $__internal_35fdb65bc1d5e3187ccb82829621c783dccd51ea67e5715e150bbe975ecbb686->leave($__internal_35fdb65bc1d5e3187ccb82829621c783dccd51ea67e5715e150bbe975ecbb686_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_bb2f7e1a5365fb9063b97111829ac878b416897c8c4aaf158a0a398acb8ce027 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb2f7e1a5365fb9063b97111829ac878b416897c8c4aaf158a0a398acb8ce027->enter($__internal_bb2f7e1a5365fb9063b97111829ac878b416897c8c4aaf158a0a398acb8ce027_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_15f57df05e24e8c77eac87c530d7529b98aeca995ac9e1979fae11f7b66bc178 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15f57df05e24e8c77eac87c530d7529b98aeca995ac9e1979fae11f7b66bc178->enter($__internal_15f57df05e24e8c77eac87c530d7529b98aeca995ac9e1979fae11f7b66bc178_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_15f57df05e24e8c77eac87c530d7529b98aeca995ac9e1979fae11f7b66bc178->leave($__internal_15f57df05e24e8c77eac87c530d7529b98aeca995ac9e1979fae11f7b66bc178_prof);

        
        $__internal_bb2f7e1a5365fb9063b97111829ac878b416897c8c4aaf158a0a398acb8ce027->leave($__internal_bb2f7e1a5365fb9063b97111829ac878b416897c8c4aaf158a0a398acb8ce027_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}
