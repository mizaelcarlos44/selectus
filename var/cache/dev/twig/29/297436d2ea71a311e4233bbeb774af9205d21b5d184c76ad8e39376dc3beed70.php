<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_d2fb6d43772f1ccfaa1d2a53f822e25a293fb346b9844a31875a1dc9c10173f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e77e45c21f9b02dc988e280851464f67e22ff6c2e1485fa075ce9b1beb8a34db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e77e45c21f9b02dc988e280851464f67e22ff6c2e1485fa075ce9b1beb8a34db->enter($__internal_e77e45c21f9b02dc988e280851464f67e22ff6c2e1485fa075ce9b1beb8a34db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_024f9cb710c89d1c124ffc4b871958c0ef9031d50147915797f390d9969434db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_024f9cb710c89d1c124ffc4b871958c0ef9031d50147915797f390d9969434db->enter($__internal_024f9cb710c89d1c124ffc4b871958c0ef9031d50147915797f390d9969434db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_e77e45c21f9b02dc988e280851464f67e22ff6c2e1485fa075ce9b1beb8a34db->leave($__internal_e77e45c21f9b02dc988e280851464f67e22ff6c2e1485fa075ce9b1beb8a34db_prof);

        
        $__internal_024f9cb710c89d1c124ffc4b871958c0ef9031d50147915797f390d9969434db->leave($__internal_024f9cb710c89d1c124ffc4b871958c0ef9031d50147915797f390d9969434db_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}
