<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_becee07805f88215adbcce699f266c3ca2d45355762a1dc7101f30680e4dba9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22a442def1b38287324380bef0e4d8dddfc1553562d4292bc177176c9b289f29 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22a442def1b38287324380bef0e4d8dddfc1553562d4292bc177176c9b289f29->enter($__internal_22a442def1b38287324380bef0e4d8dddfc1553562d4292bc177176c9b289f29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_1a8f734ece192acbc8574c8d72cf00c14c0dd75a86780345d0496bbd5e862680 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a8f734ece192acbc8574c8d72cf00c14c0dd75a86780345d0496bbd5e862680->enter($__internal_1a8f734ece192acbc8574c8d72cf00c14c0dd75a86780345d0496bbd5e862680_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_22a442def1b38287324380bef0e4d8dddfc1553562d4292bc177176c9b289f29->leave($__internal_22a442def1b38287324380bef0e4d8dddfc1553562d4292bc177176c9b289f29_prof);

        
        $__internal_1a8f734ece192acbc8574c8d72cf00c14c0dd75a86780345d0496bbd5e862680->leave($__internal_1a8f734ece192acbc8574c8d72cf00c14c0dd75a86780345d0496bbd5e862680_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\reset_widget.html.php");
    }
}
