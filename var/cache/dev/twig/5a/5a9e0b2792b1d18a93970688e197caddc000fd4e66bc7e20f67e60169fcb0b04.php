<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_72906fcea3debb325d332f3cc1831730afe2e8c6e0b34177386e0180f03850f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3690ff81ce089b9dba889fd41635a73a48c548a03eb97aa4841c9c6cc4082d6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3690ff81ce089b9dba889fd41635a73a48c548a03eb97aa4841c9c6cc4082d6->enter($__internal_c3690ff81ce089b9dba889fd41635a73a48c548a03eb97aa4841c9c6cc4082d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_4fa8f1a8e912993dd828c74a31c96c95c65f52b28300f6ae729d0def273f80df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4fa8f1a8e912993dd828c74a31c96c95c65f52b28300f6ae729d0def273f80df->enter($__internal_4fa8f1a8e912993dd828c74a31c96c95c65f52b28300f6ae729d0def273f80df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_c3690ff81ce089b9dba889fd41635a73a48c548a03eb97aa4841c9c6cc4082d6->leave($__internal_c3690ff81ce089b9dba889fd41635a73a48c548a03eb97aa4841c9c6cc4082d6_prof);

        
        $__internal_4fa8f1a8e912993dd828c74a31c96c95c65f52b28300f6ae729d0def273f80df->leave($__internal_4fa8f1a8e912993dd828c74a31c96c95c65f52b28300f6ae729d0def273f80df_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}
