<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_2d896137cb2172f6bbf2d7c8aa0f18ca5835c9a54ad72c7165ac4ba7bd3bd871 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee5622b7f7cb17300a473850e8e9e5784c9b5c87047a4e715c45fb42be11aa20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee5622b7f7cb17300a473850e8e9e5784c9b5c87047a4e715c45fb42be11aa20->enter($__internal_ee5622b7f7cb17300a473850e8e9e5784c9b5c87047a4e715c45fb42be11aa20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_956e78e1c44da80d5f9f9fd301601313f4f6c1578378d59c7958c5037d6ceb06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_956e78e1c44da80d5f9f9fd301601313f4f6c1578378d59c7958c5037d6ceb06->enter($__internal_956e78e1c44da80d5f9f9fd301601313f4f6c1578378d59c7958c5037d6ceb06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_ee5622b7f7cb17300a473850e8e9e5784c9b5c87047a4e715c45fb42be11aa20->leave($__internal_ee5622b7f7cb17300a473850e8e9e5784c9b5c87047a4e715c45fb42be11aa20_prof);

        
        $__internal_956e78e1c44da80d5f9f9fd301601313f4f6c1578378d59c7958c5037d6ceb06->leave($__internal_956e78e1c44da80d5f9f9fd301601313f4f6c1578378d59c7958c5037d6ceb06_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}
