<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_4e01af8ce8baf523efd5a8898ea2d69e264b4291a85d43acd3eb2ccb02c28d2e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a6ad1f737962b922a8e4dba3edf473fdc3ebbcdd44a4413644223881911b314 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a6ad1f737962b922a8e4dba3edf473fdc3ebbcdd44a4413644223881911b314->enter($__internal_1a6ad1f737962b922a8e4dba3edf473fdc3ebbcdd44a4413644223881911b314_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_926cc457a5afee7e4ea90b5ecf518b544487d15c45e013d458ded6f871690f32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_926cc457a5afee7e4ea90b5ecf518b544487d15c45e013d458ded6f871690f32->enter($__internal_926cc457a5afee7e4ea90b5ecf518b544487d15c45e013d458ded6f871690f32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_1a6ad1f737962b922a8e4dba3edf473fdc3ebbcdd44a4413644223881911b314->leave($__internal_1a6ad1f737962b922a8e4dba3edf473fdc3ebbcdd44a4413644223881911b314_prof);

        
        $__internal_926cc457a5afee7e4ea90b5ecf518b544487d15c45e013d458ded6f871690f32->leave($__internal_926cc457a5afee7e4ea90b5ecf518b544487d15c45e013d458ded6f871690f32_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\submit_widget.html.php");
    }
}
