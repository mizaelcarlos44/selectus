<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_d11cbdf64e7fb3d374c87fcdd598a98dae51609b9a05afc1633ef255f0cb3bf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21bc9b8969d26d8d6a5d26672a69ae72aae02904d7c8c456dac515f5d8917f88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21bc9b8969d26d8d6a5d26672a69ae72aae02904d7c8c456dac515f5d8917f88->enter($__internal_21bc9b8969d26d8d6a5d26672a69ae72aae02904d7c8c456dac515f5d8917f88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_62a73b4253150805d06bc83888576e8ea7bdc795fa98bdc1238df4fe880922bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62a73b4253150805d06bc83888576e8ea7bdc795fa98bdc1238df4fe880922bd->enter($__internal_62a73b4253150805d06bc83888576e8ea7bdc795fa98bdc1238df4fe880922bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_21bc9b8969d26d8d6a5d26672a69ae72aae02904d7c8c456dac515f5d8917f88->leave($__internal_21bc9b8969d26d8d6a5d26672a69ae72aae02904d7c8c456dac515f5d8917f88_prof);

        
        $__internal_62a73b4253150805d06bc83888576e8ea7bdc795fa98bdc1238df4fe880922bd->leave($__internal_62a73b4253150805d06bc83888576e8ea7bdc795fa98bdc1238df4fe880922bd_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\email_widget.html.php");
    }
}
