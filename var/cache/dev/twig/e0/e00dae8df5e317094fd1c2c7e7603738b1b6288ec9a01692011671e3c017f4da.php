<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_8472a2c61093b31931e5effd2c08f217660f70ca7b1d73a848589cde042d1731 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b00f10b2206eae23b348e1869ad2d404a908f152bdd3e77398f5d30e4a4575b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b00f10b2206eae23b348e1869ad2d404a908f152bdd3e77398f5d30e4a4575b9->enter($__internal_b00f10b2206eae23b348e1869ad2d404a908f152bdd3e77398f5d30e4a4575b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_b5b2eecaa1a60f6abfbabc29d0783a7563c7304b7bc54232b6bc7b6d5a58f14c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b5b2eecaa1a60f6abfbabc29d0783a7563c7304b7bc54232b6bc7b6d5a58f14c->enter($__internal_b5b2eecaa1a60f6abfbabc29d0783a7563c7304b7bc54232b6bc7b6d5a58f14c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
";
        
        $__internal_b00f10b2206eae23b348e1869ad2d404a908f152bdd3e77398f5d30e4a4575b9->leave($__internal_b00f10b2206eae23b348e1869ad2d404a908f152bdd3e77398f5d30e4a4575b9_prof);

        
        $__internal_b5b2eecaa1a60f6abfbabc29d0783a7563c7304b7bc54232b6bc7b6d5a58f14c->leave($__internal_b5b2eecaa1a60f6abfbabc29d0783a7563c7304b7bc54232b6bc7b6d5a58f14c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\form_widget_compound.html.php");
    }
}
