<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_3b720ad5a78ee950d77f8afe647fbefbbcac94e4dc42bdea0ada3fe970d57725 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fefe58735fc0ba69f9a4068d198434aa15184267faf79e8ac6ff7c3a9945144a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fefe58735fc0ba69f9a4068d198434aa15184267faf79e8ac6ff7c3a9945144a->enter($__internal_fefe58735fc0ba69f9a4068d198434aa15184267faf79e8ac6ff7c3a9945144a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        $__internal_ce77d21ff1a0ce12d00d0cdeeabc0985d9b163946800806090f76a5a379c3375 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce77d21ff1a0ce12d00d0cdeeabc0985d9b163946800806090f76a5a379c3375->enter($__internal_ce77d21ff1a0ce12d00d0cdeeabc0985d9b163946800806090f76a5a379c3375_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => ($context["exception"] ?? $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_fefe58735fc0ba69f9a4068d198434aa15184267faf79e8ac6ff7c3a9945144a->leave($__internal_fefe58735fc0ba69f9a4068d198434aa15184267faf79e8ac6ff7c3a9945144a_prof);

        
        $__internal_ce77d21ff1a0ce12d00d0cdeeabc0985d9b163946800806090f76a5a379c3375->leave($__internal_ce77d21ff1a0ce12d00d0cdeeabc0985d9b163946800806090f76a5a379c3375_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "@Twig/Exception/exception.css.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.css.twig");
    }
}
