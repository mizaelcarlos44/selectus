<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_edf58e9eebc3ee350bde014d0a827730b4c2501c79baa078d306516d85b70674 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e9fb3393d9bfdc1cfbe07a4ffea5a5f9df8736eb5bc80d7ac6b13dd1fab8141 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0e9fb3393d9bfdc1cfbe07a4ffea5a5f9df8736eb5bc80d7ac6b13dd1fab8141->enter($__internal_0e9fb3393d9bfdc1cfbe07a4ffea5a5f9df8736eb5bc80d7ac6b13dd1fab8141_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_1863356f559820ae03bad5ccd08ef26ce72464a2b37ad05a8c4cb5207fc0a59f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1863356f559820ae03bad5ccd08ef26ce72464a2b37ad05a8c4cb5207fc0a59f->enter($__internal_1863356f559820ae03bad5ccd08ef26ce72464a2b37ad05a8c4cb5207fc0a59f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_0e9fb3393d9bfdc1cfbe07a4ffea5a5f9df8736eb5bc80d7ac6b13dd1fab8141->leave($__internal_0e9fb3393d9bfdc1cfbe07a4ffea5a5f9df8736eb5bc80d7ac6b13dd1fab8141_prof);

        
        $__internal_1863356f559820ae03bad5ccd08ef26ce72464a2b37ad05a8c4cb5207fc0a59f->leave($__internal_1863356f559820ae03bad5ccd08ef26ce72464a2b37ad05a8c4cb5207fc0a59f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\radio_widget.html.php");
    }
}
