<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_216180dd67c50f963ee3aaeb8f32af49e9daf4dbec518aae9b66fafa8fb9b3fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53e2de259e21f4cf99f482c4e21518ece19be6d6d704e4b0d5131617e3f10f2e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53e2de259e21f4cf99f482c4e21518ece19be6d6d704e4b0d5131617e3f10f2e->enter($__internal_53e2de259e21f4cf99f482c4e21518ece19be6d6d704e4b0d5131617e3f10f2e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        $__internal_588f66e94a0d2ae716c7c128299bcc850f8a9582bb7d2c0f4bcc386fac19ea5a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_588f66e94a0d2ae716c7c128299bcc850f8a9582bb7d2c0f4bcc386fac19ea5a->enter($__internal_588f66e94a0d2ae716c7c128299bcc850f8a9582bb7d2c0f4bcc386fac19ea5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_53e2de259e21f4cf99f482c4e21518ece19be6d6d704e4b0d5131617e3f10f2e->leave($__internal_53e2de259e21f4cf99f482c4e21518ece19be6d6d704e4b0d5131617e3f10f2e_prof);

        
        $__internal_588f66e94a0d2ae716c7c128299bcc850f8a9582bb7d2c0f4bcc386fac19ea5a->leave($__internal_588f66e94a0d2ae716c7c128299bcc850f8a9582bb7d2c0f4bcc386fac19ea5a_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "@Twig/Exception/error.atom.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.atom.twig");
    }
}
