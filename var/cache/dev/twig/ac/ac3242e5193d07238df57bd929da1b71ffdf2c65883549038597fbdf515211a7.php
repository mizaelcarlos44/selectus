<?php

/* inscricao/edit.html.twig */
class __TwigTemplate_7318b123d764734d61929b304bbbd097630c0ba535f66d09203e932ecb11bcf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "inscricao/edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0a77ca0928cf17c7504a3d50e5ed136bb833e0f40a0e05be21520b1d68778dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0a77ca0928cf17c7504a3d50e5ed136bb833e0f40a0e05be21520b1d68778dc->enter($__internal_f0a77ca0928cf17c7504a3d50e5ed136bb833e0f40a0e05be21520b1d68778dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/edit.html.twig"));

        $__internal_a70da89859f2b6a6517f953e96fc2fdc1308d035f38e46a11fbd5209401fe49d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a70da89859f2b6a6517f953e96fc2fdc1308d035f38e46a11fbd5209401fe49d->enter($__internal_a70da89859f2b6a6517f953e96fc2fdc1308d035f38e46a11fbd5209401fe49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "inscricao/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f0a77ca0928cf17c7504a3d50e5ed136bb833e0f40a0e05be21520b1d68778dc->leave($__internal_f0a77ca0928cf17c7504a3d50e5ed136bb833e0f40a0e05be21520b1d68778dc_prof);

        
        $__internal_a70da89859f2b6a6517f953e96fc2fdc1308d035f38e46a11fbd5209401fe49d->leave($__internal_a70da89859f2b6a6517f953e96fc2fdc1308d035f38e46a11fbd5209401fe49d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d47e92742080cda4cfeb60928cd8fd716990ab72110097c5fdd4710b5eea3430 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d47e92742080cda4cfeb60928cd8fd716990ab72110097c5fdd4710b5eea3430->enter($__internal_d47e92742080cda4cfeb60928cd8fd716990ab72110097c5fdd4710b5eea3430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_55735118aecffca65f88b072edd85efad39f6d2a1122c000d774c71f84dad188 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55735118aecffca65f88b072edd85efad39f6d2a1122c000d774c71f84dad188->enter($__internal_55735118aecffca65f88b072edd85efad39f6d2a1122c000d774c71f84dad188_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Alterar inscrição</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'widget');
        echo "
        <input class=\"btn btn-success\" type=\"submit\" value=\"Salvar\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["edit_form"] ?? $this->getContext($context, "edit_form")), 'form_end');
        echo "

           
\t";
        // line 12
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_start');
        echo "
\t\t<input  class=\"btn btn-danger\" type=\"submit\" value=\"Excluir\">
\t";
        // line 14
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form"] ?? $this->getContext($context, "delete_form")), 'form_end');
        echo "
    
";
        
        $__internal_55735118aecffca65f88b072edd85efad39f6d2a1122c000d774c71f84dad188->leave($__internal_55735118aecffca65f88b072edd85efad39f6d2a1122c000d774c71f84dad188_prof);

        
        $__internal_d47e92742080cda4cfeb60928cd8fd716990ab72110097c5fdd4710b5eea3430->leave($__internal_d47e92742080cda4cfeb60928cd8fd716990ab72110097c5fdd4710b5eea3430_prof);

    }

    public function getTemplateName()
    {
        return "inscricao/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 14,  68 => 12,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Alterar inscrição</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input class=\"btn btn-success\" type=\"submit\" value=\"Salvar\" />
    {{ form_end(edit_form) }}

           
\t{{ form_start(delete_form) }}
\t\t<input  class=\"btn btn-danger\" type=\"submit\" value=\"Excluir\">
\t{{ form_end(delete_form) }}
    
{% endblock %}
", "inscricao/edit.html.twig", "C:\\wamp64\\www\\selectus\\app\\Resources\\views\\inscricao\\edit.html.twig");
    }
}
