<?php

/* bootstrap_3_horizontal_layout.html.twig */
class __TwigTemplate_78c03d7aa14b8e33d83484233321ef3d7a8622dca05166fff8249a99f88ede44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_3_layout.html.twig", "bootstrap_3_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_3_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_start' => array($this, 'block_form_start'),
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b7604371829b6292d6f567679385c1ef219143ccb2117e2524bf9ddf80e8849 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b7604371829b6292d6f567679385c1ef219143ccb2117e2524bf9ddf80e8849->enter($__internal_4b7604371829b6292d6f567679385c1ef219143ccb2117e2524bf9ddf80e8849_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        $__internal_8b4584714982c3c126d7376e0947b98654d1fb8d3c6b88ae2f04a618444831d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b4584714982c3c126d7376e0947b98654d1fb8d3c6b88ae2f04a618444831d4->enter($__internal_8b4584714982c3c126d7376e0947b98654d1fb8d3c6b88ae2f04a618444831d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 3
        $this->displayBlock('form_start', $context, $blocks);
        // line 7
        echo "
";
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('form_label', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 22
        echo "
";
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('form_row', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('submit_row', $context, $blocks);
        // line 43
        echo "
";
        // line 44
        $this->displayBlock('reset_row', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('form_group_class', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('checkbox_row', $context, $blocks);
        
        $__internal_4b7604371829b6292d6f567679385c1ef219143ccb2117e2524bf9ddf80e8849->leave($__internal_4b7604371829b6292d6f567679385c1ef219143ccb2117e2524bf9ddf80e8849_prof);

        
        $__internal_8b4584714982c3c126d7376e0947b98654d1fb8d3c6b88ae2f04a618444831d4->leave($__internal_8b4584714982c3c126d7376e0947b98654d1fb8d3c6b88ae2f04a618444831d4_prof);

    }

    // line 3
    public function block_form_start($context, array $blocks = array())
    {
        $__internal_64a22f71040364ae8868741a0177f5a9f71b7f5154b0adfa7e31df3d2133c9a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64a22f71040364ae8868741a0177f5a9f71b7f5154b0adfa7e31df3d2133c9a7->enter($__internal_64a22f71040364ae8868741a0177f5a9f71b7f5154b0adfa7e31df3d2133c9a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        $__internal_d7f65bd1f1b4cd7f40d7906d90d06c885f3d030039d7039a89d04b95de89fd98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7f65bd1f1b4cd7f40d7906d90d06c885f3d030039d7039a89d04b95de89fd98->enter($__internal_d7f65bd1f1b4cd7f40d7906d90d06c885f3d030039d7039a89d04b95de89fd98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_start"));

        // line 4
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-horizontal"))));
        // line 5
        $this->displayParentBlock("form_start", $context, $blocks);
        
        $__internal_d7f65bd1f1b4cd7f40d7906d90d06c885f3d030039d7039a89d04b95de89fd98->leave($__internal_d7f65bd1f1b4cd7f40d7906d90d06c885f3d030039d7039a89d04b95de89fd98_prof);

        
        $__internal_64a22f71040364ae8868741a0177f5a9f71b7f5154b0adfa7e31df3d2133c9a7->leave($__internal_64a22f71040364ae8868741a0177f5a9f71b7f5154b0adfa7e31df3d2133c9a7_prof);

    }

    // line 10
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_a536d061d51bcf54acbd7d64581da53ae05878544fe69b3ec9b83b92907fc95b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a536d061d51bcf54acbd7d64581da53ae05878544fe69b3ec9b83b92907fc95b->enter($__internal_a536d061d51bcf54acbd7d64581da53ae05878544fe69b3ec9b83b92907fc95b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_97d9fbb318eeee933481a792f522197e692faf1dbd3e16c054ce8fc7ab01000a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97d9fbb318eeee933481a792f522197e692faf1dbd3e16c054ce8fc7ab01000a->enter($__internal_97d9fbb318eeee933481a792f522197e692faf1dbd3e16c054ce8fc7ab01000a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 11
        if ((($context["label"] ?? $this->getContext($context, "label")) === false)) {
            // line 12
            echo "<div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>";
        } else {
            // line 14
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 15
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        
        $__internal_97d9fbb318eeee933481a792f522197e692faf1dbd3e16c054ce8fc7ab01000a->leave($__internal_97d9fbb318eeee933481a792f522197e692faf1dbd3e16c054ce8fc7ab01000a_prof);

        
        $__internal_a536d061d51bcf54acbd7d64581da53ae05878544fe69b3ec9b83b92907fc95b->leave($__internal_a536d061d51bcf54acbd7d64581da53ae05878544fe69b3ec9b83b92907fc95b_prof);

    }

    // line 19
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_ef7b500bf7916b334c4d2f92d6e78a5532db857f67062d1786fcac1de202314d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef7b500bf7916b334c4d2f92d6e78a5532db857f67062d1786fcac1de202314d->enter($__internal_ef7b500bf7916b334c4d2f92d6e78a5532db857f67062d1786fcac1de202314d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_511809db9cc8ee7d78fbae5b5411ae26127017e264ab9c49b8fb27841eda3175 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_511809db9cc8ee7d78fbae5b5411ae26127017e264ab9c49b8fb27841eda3175->enter($__internal_511809db9cc8ee7d78fbae5b5411ae26127017e264ab9c49b8fb27841eda3175_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 20
        echo "col-sm-2";
        
        $__internal_511809db9cc8ee7d78fbae5b5411ae26127017e264ab9c49b8fb27841eda3175->leave($__internal_511809db9cc8ee7d78fbae5b5411ae26127017e264ab9c49b8fb27841eda3175_prof);

        
        $__internal_ef7b500bf7916b334c4d2f92d6e78a5532db857f67062d1786fcac1de202314d->leave($__internal_ef7b500bf7916b334c4d2f92d6e78a5532db857f67062d1786fcac1de202314d_prof);

    }

    // line 25
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_80c9b4af8b6cc2534ab10f84e9fe77fbe7bdfcab86101756115a22f9275ec613 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80c9b4af8b6cc2534ab10f84e9fe77fbe7bdfcab86101756115a22f9275ec613->enter($__internal_80c9b4af8b6cc2534ab10f84e9fe77fbe7bdfcab86101756115a22f9275ec613_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_3b55939066f757e60470ae1e9a33f5a9622e0d53fbe39b976c3e29bc4b1bf0b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b55939066f757e60470ae1e9a33f5a9622e0d53fbe39b976c3e29bc4b1bf0b5->enter($__internal_3b55939066f757e60470ae1e9a33f5a9622e0d53fbe39b976c3e29bc4b1bf0b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 26
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 28
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 31
        echo "</div>
";
        // line 32
        echo "</div>";
        
        $__internal_3b55939066f757e60470ae1e9a33f5a9622e0d53fbe39b976c3e29bc4b1bf0b5->leave($__internal_3b55939066f757e60470ae1e9a33f5a9622e0d53fbe39b976c3e29bc4b1bf0b5_prof);

        
        $__internal_80c9b4af8b6cc2534ab10f84e9fe77fbe7bdfcab86101756115a22f9275ec613->leave($__internal_80c9b4af8b6cc2534ab10f84e9fe77fbe7bdfcab86101756115a22f9275ec613_prof);

    }

    // line 35
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_2b6faf7012d7d92cb41264bf97f770f8d40b9ce6e906ea131fa37d8b35768ffa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b6faf7012d7d92cb41264bf97f770f8d40b9ce6e906ea131fa37d8b35768ffa->enter($__internal_2b6faf7012d7d92cb41264bf97f770f8d40b9ce6e906ea131fa37d8b35768ffa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_916f80039f8015866e0eea6ac21f54c92159ff02c064cea737e62c8d67dc29c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_916f80039f8015866e0eea6ac21f54c92159ff02c064cea737e62c8d67dc29c9->enter($__internal_916f80039f8015866e0eea6ac21f54c92159ff02c064cea737e62c8d67dc29c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 36
        echo "<div class=\"form-group\">";
        // line 37
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 38
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 40
        echo "</div>";
        // line 41
        echo "</div>";
        
        $__internal_916f80039f8015866e0eea6ac21f54c92159ff02c064cea737e62c8d67dc29c9->leave($__internal_916f80039f8015866e0eea6ac21f54c92159ff02c064cea737e62c8d67dc29c9_prof);

        
        $__internal_2b6faf7012d7d92cb41264bf97f770f8d40b9ce6e906ea131fa37d8b35768ffa->leave($__internal_2b6faf7012d7d92cb41264bf97f770f8d40b9ce6e906ea131fa37d8b35768ffa_prof);

    }

    // line 44
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_fa5c398ead3e27fbbf3696871b320cca6cf71b0bd7a896e0255a5018e6248c11 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa5c398ead3e27fbbf3696871b320cca6cf71b0bd7a896e0255a5018e6248c11->enter($__internal_fa5c398ead3e27fbbf3696871b320cca6cf71b0bd7a896e0255a5018e6248c11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_8bb9be33c18f8832e3c67eb3b4356098329396bed664ef00cbe7440e3295e382 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bb9be33c18f8832e3c67eb3b4356098329396bed664ef00cbe7440e3295e382->enter($__internal_8bb9be33c18f8832e3c67eb3b4356098329396bed664ef00cbe7440e3295e382_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 45
        echo "<div class=\"form-group\">";
        // line 46
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 47
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 49
        echo "</div>";
        // line 50
        echo "</div>";
        
        $__internal_8bb9be33c18f8832e3c67eb3b4356098329396bed664ef00cbe7440e3295e382->leave($__internal_8bb9be33c18f8832e3c67eb3b4356098329396bed664ef00cbe7440e3295e382_prof);

        
        $__internal_fa5c398ead3e27fbbf3696871b320cca6cf71b0bd7a896e0255a5018e6248c11->leave($__internal_fa5c398ead3e27fbbf3696871b320cca6cf71b0bd7a896e0255a5018e6248c11_prof);

    }

    // line 53
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_c5d7460fbab5bd2558fad4d706367521879f49ab0f06bab3b8632fc2e81ca927 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c5d7460fbab5bd2558fad4d706367521879f49ab0f06bab3b8632fc2e81ca927->enter($__internal_c5d7460fbab5bd2558fad4d706367521879f49ab0f06bab3b8632fc2e81ca927_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_0ae6889f15def465936d04fb2bc8873bcdcea26bbfe584790ed6ec0230acd840 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0ae6889f15def465936d04fb2bc8873bcdcea26bbfe584790ed6ec0230acd840->enter($__internal_0ae6889f15def465936d04fb2bc8873bcdcea26bbfe584790ed6ec0230acd840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 54
        echo "col-sm-10";
        
        $__internal_0ae6889f15def465936d04fb2bc8873bcdcea26bbfe584790ed6ec0230acd840->leave($__internal_0ae6889f15def465936d04fb2bc8873bcdcea26bbfe584790ed6ec0230acd840_prof);

        
        $__internal_c5d7460fbab5bd2558fad4d706367521879f49ab0f06bab3b8632fc2e81ca927->leave($__internal_c5d7460fbab5bd2558fad4d706367521879f49ab0f06bab3b8632fc2e81ca927_prof);

    }

    // line 57
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_e73bd4e7c74c08d564b06e819e483efbd0da7275d514acbee540337d1cd91051 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e73bd4e7c74c08d564b06e819e483efbd0da7275d514acbee540337d1cd91051->enter($__internal_e73bd4e7c74c08d564b06e819e483efbd0da7275d514acbee540337d1cd91051_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_72ae6159b6dbea891d782d4b947b867b8c6bbe53ba08d6dda732193cf174f943 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72ae6159b6dbea891d782d4b947b867b8c6bbe53ba08d6dda732193cf174f943->enter($__internal_72ae6159b6dbea891d782d4b947b867b8c6bbe53ba08d6dda732193cf174f943_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 58
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 59
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 60
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 63
        echo "</div>";
        // line 64
        echo "</div>";
        
        $__internal_72ae6159b6dbea891d782d4b947b867b8c6bbe53ba08d6dda732193cf174f943->leave($__internal_72ae6159b6dbea891d782d4b947b867b8c6bbe53ba08d6dda732193cf174f943_prof);

        
        $__internal_e73bd4e7c74c08d564b06e819e483efbd0da7275d514acbee540337d1cd91051->leave($__internal_e73bd4e7c74c08d564b06e819e483efbd0da7275d514acbee540337d1cd91051_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  320 => 64,  318 => 63,  316 => 62,  314 => 61,  310 => 60,  306 => 59,  300 => 58,  291 => 57,  281 => 54,  272 => 53,  262 => 50,  260 => 49,  258 => 48,  254 => 47,  250 => 46,  248 => 45,  239 => 44,  229 => 41,  227 => 40,  225 => 39,  221 => 38,  217 => 37,  215 => 36,  206 => 35,  196 => 32,  193 => 31,  191 => 30,  189 => 29,  185 => 28,  183 => 27,  177 => 26,  168 => 25,  158 => 20,  149 => 19,  138 => 15,  136 => 14,  131 => 12,  129 => 11,  120 => 10,  110 => 5,  108 => 4,  99 => 3,  89 => 57,  86 => 56,  84 => 53,  81 => 52,  79 => 44,  76 => 43,  74 => 35,  71 => 34,  69 => 25,  66 => 24,  63 => 22,  61 => 19,  58 => 18,  56 => 10,  53 => 9,  50 => 7,  48 => 3,  45 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_3_layout.html.twig\" %}

{% block form_start -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-horizontal')|trim}) %}
    {{- parent() -}}
{%- endblock form_start %}

{# Labels #}

{% block form_label -%}
    {%- if label is same as(false) -%}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {%- else -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) -%}
        {{- parent() -}}
    {%- endif -%}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>
{##}</div>
{%- endblock form_row %}

{% block submit_row -%}
    <div class=\"form-group\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock submit_row %}

{% block reset_row -%}
    <div class=\"form-group\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>{#--#}
    </div>
{%- endblock checkbox_row %}", "bootstrap_3_horizontal_layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_3_horizontal_layout.html.twig");
    }
}
