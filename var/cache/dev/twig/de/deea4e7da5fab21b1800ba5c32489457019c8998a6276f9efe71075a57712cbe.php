<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_2469ecb5cf3e075367343b6f22045d4dcf2577b9a0f902fada5445d8fb8ea0f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7e58588b2bd6d19fbbefe247b815c3b6a77076adf4e068d6b2f16ff205655d10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e58588b2bd6d19fbbefe247b815c3b6a77076adf4e068d6b2f16ff205655d10->enter($__internal_7e58588b2bd6d19fbbefe247b815c3b6a77076adf4e068d6b2f16ff205655d10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_60021246db37b2631c0e6422cdae34ef95291d0a93bad92a436f5f6b75f0f420 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60021246db37b2631c0e6422cdae34ef95291d0a93bad92a436f5f6b75f0f420->enter($__internal_60021246db37b2631c0e6422cdae34ef95291d0a93bad92a436f5f6b75f0f420_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_7e58588b2bd6d19fbbefe247b815c3b6a77076adf4e068d6b2f16ff205655d10->leave($__internal_7e58588b2bd6d19fbbefe247b815c3b6a77076adf4e068d6b2f16ff205655d10_prof);

        
        $__internal_60021246db37b2631c0e6422cdae34ef95291d0a93bad92a436f5f6b75f0f420->leave($__internal_60021246db37b2631c0e6422cdae34ef95291d0a93bad92a436f5f6b75f0f420_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget_compound.html.php");
    }
}
