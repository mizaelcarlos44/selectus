<?php

/* bootstrap_4_layout.html.twig */
class __TwigTemplate_2a82d64eab75704a3827f78b7499c8f7f22bbdb4b225e24e54727c440f306d4c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_base_layout.html.twig", "bootstrap_4_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_base_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'money_widget' => array($this, 'block_money_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'dateinterval_widget' => array($this, 'block_dateinterval_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'widget_attributes' => array($this, 'block_widget_attributes'),
                'button_widget' => array($this, 'block_button_widget'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'form_label' => array($this, 'block_form_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a28fa0a838086670c8c697c521cf52bd10d8eb925c2de2d5e53dff3f375bcda2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a28fa0a838086670c8c697c521cf52bd10d8eb925c2de2d5e53dff3f375bcda2->enter($__internal_a28fa0a838086670c8c697c521cf52bd10d8eb925c2de2d5e53dff3f375bcda2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_layout.html.twig"));

        $__internal_c198f7df56a3b11d367fe63c22fa5773cd4ce0fdddd7f01e274c44b0f0f6c5e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c198f7df56a3b11d367fe63c22fa5773cd4ce0fdddd7f01e274c44b0f0f6c5e0->enter($__internal_c198f7df56a3b11d367fe63c22fa5773cd4ce0fdddd7f01e274c44b0f0f6c5e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('money_widget', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('date_widget', $context, $blocks);
        // line 28
        echo "
";
        // line 29
        $this->displayBlock('time_widget', $context, $blocks);
        // line 36
        echo "
";
        // line 37
        $this->displayBlock('dateinterval_widget', $context, $blocks);
        // line 44
        echo "
";
        // line 45
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 60
        $this->displayBlock('widget_attributes', $context, $blocks);
        // line 67
        $this->displayBlock('button_widget', $context, $blocks);
        // line 71
        echo "
";
        // line 72
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 83
        echo "
";
        // line 84
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 95
        echo "
";
        // line 96
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 120
        echo "
";
        // line 122
        echo "
";
        // line 123
        $this->displayBlock('form_label', $context, $blocks);
        // line 132
        echo "
";
        // line 133
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 158
        echo "
";
        // line 160
        echo "
";
        // line 161
        $this->displayBlock('form_row', $context, $blocks);
        // line 171
        echo "
";
        // line 173
        echo "
";
        // line 174
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_a28fa0a838086670c8c697c521cf52bd10d8eb925c2de2d5e53dff3f375bcda2->leave($__internal_a28fa0a838086670c8c697c521cf52bd10d8eb925c2de2d5e53dff3f375bcda2_prof);

        
        $__internal_c198f7df56a3b11d367fe63c22fa5773cd4ce0fdddd7f01e274c44b0f0f6c5e0->leave($__internal_c198f7df56a3b11d367fe63c22fa5773cd4ce0fdddd7f01e274c44b0f0f6c5e0_prof);

    }

    // line 5
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_2a77e8a67cfe0adb1d59abf500b7a35e4119e5b6abc69e908761ca9e3a4366d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a77e8a67cfe0adb1d59abf500b7a35e4119e5b6abc69e908761ca9e3a4366d4->enter($__internal_2a77e8a67cfe0adb1d59abf500b7a35e4119e5b6abc69e908761ca9e3a4366d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_c06447a3ce8749e469524001e0d18f06df5bd134a95032a733efb7fc74f75c98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c06447a3ce8749e469524001e0d18f06df5bd134a95032a733efb7fc74f75c98->enter($__internal_c06447a3ce8749e469524001e0d18f06df5bd134a95032a733efb7fc74f75c98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 6
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            // line 7
            echo "        ";
            $context["group_class"] = " form-control is-invalid";
            // line 8
            echo "        ";
            $context["valid"] = true;
            // line 9
            echo "    ";
        }
        // line 10
        $this->displayParentBlock("money_widget", $context, $blocks);
        
        $__internal_c06447a3ce8749e469524001e0d18f06df5bd134a95032a733efb7fc74f75c98->leave($__internal_c06447a3ce8749e469524001e0d18f06df5bd134a95032a733efb7fc74f75c98_prof);

        
        $__internal_2a77e8a67cfe0adb1d59abf500b7a35e4119e5b6abc69e908761ca9e3a4366d4->leave($__internal_2a77e8a67cfe0adb1d59abf500b7a35e4119e5b6abc69e908761ca9e3a4366d4_prof);

    }

    // line 13
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_6db4f18961f5b8e8b7ce39a66ab7fb40d0fa500fcb960c1ccc0b2e00cbdac2ff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6db4f18961f5b8e8b7ce39a66ab7fb40d0fa500fcb960c1ccc0b2e00cbdac2ff->enter($__internal_6db4f18961f5b8e8b7ce39a66ab7fb40d0fa500fcb960c1ccc0b2e00cbdac2ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_7ea05aad4432aec2a910eca363eb562ad2c93afdbb8f9b03a6a082590a24e1b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ea05aad4432aec2a910eca363eb562ad2c93afdbb8f9b03a6a082590a24e1b5->enter($__internal_7ea05aad4432aec2a910eca363eb562ad2c93afdbb8f9b03a6a082590a24e1b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 14
        if (((($context["widget"] ?? $this->getContext($context, "widget")) != "single_text") &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            // line 15
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 16
            $context["valid"] = true;
        }
        // line 18
        $this->displayParentBlock("datetime_widget", $context, $blocks);
        
        $__internal_7ea05aad4432aec2a910eca363eb562ad2c93afdbb8f9b03a6a082590a24e1b5->leave($__internal_7ea05aad4432aec2a910eca363eb562ad2c93afdbb8f9b03a6a082590a24e1b5_prof);

        
        $__internal_6db4f18961f5b8e8b7ce39a66ab7fb40d0fa500fcb960c1ccc0b2e00cbdac2ff->leave($__internal_6db4f18961f5b8e8b7ce39a66ab7fb40d0fa500fcb960c1ccc0b2e00cbdac2ff_prof);

    }

    // line 21
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_d85897913c14f3770e6aa8acbf0b4f88c3f52f68d8c9b9e67cb015053e15dcfb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d85897913c14f3770e6aa8acbf0b4f88c3f52f68d8c9b9e67cb015053e15dcfb->enter($__internal_d85897913c14f3770e6aa8acbf0b4f88c3f52f68d8c9b9e67cb015053e15dcfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_a1f0ba9bc711fa49e51b8246b13fcb01bce85ecd7aa9d75573a7d6a6999cec72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1f0ba9bc711fa49e51b8246b13fcb01bce85ecd7aa9d75573a7d6a6999cec72->enter($__internal_a1f0ba9bc711fa49e51b8246b13fcb01bce85ecd7aa9d75573a7d6a6999cec72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 22
        if (((($context["widget"] ?? $this->getContext($context, "widget")) != "single_text") &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            // line 23
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 24
            $context["valid"] = true;
        }
        // line 26
        $this->displayParentBlock("date_widget", $context, $blocks);
        
        $__internal_a1f0ba9bc711fa49e51b8246b13fcb01bce85ecd7aa9d75573a7d6a6999cec72->leave($__internal_a1f0ba9bc711fa49e51b8246b13fcb01bce85ecd7aa9d75573a7d6a6999cec72_prof);

        
        $__internal_d85897913c14f3770e6aa8acbf0b4f88c3f52f68d8c9b9e67cb015053e15dcfb->leave($__internal_d85897913c14f3770e6aa8acbf0b4f88c3f52f68d8c9b9e67cb015053e15dcfb_prof);

    }

    // line 29
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_57cad15412d9dcd00a9c84183ae53656959818bb15d0af9c872d31c9987da856 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_57cad15412d9dcd00a9c84183ae53656959818bb15d0af9c872d31c9987da856->enter($__internal_57cad15412d9dcd00a9c84183ae53656959818bb15d0af9c872d31c9987da856_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_70a12db4e84a6f3fc302a2111d7845aa8c3e915998fb6b1cdea6ffaac5487bb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70a12db4e84a6f3fc302a2111d7845aa8c3e915998fb6b1cdea6ffaac5487bb9->enter($__internal_70a12db4e84a6f3fc302a2111d7845aa8c3e915998fb6b1cdea6ffaac5487bb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 30
        if (((($context["widget"] ?? $this->getContext($context, "widget")) != "single_text") &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            // line 31
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 32
            $context["valid"] = true;
        }
        // line 34
        $this->displayParentBlock("time_widget", $context, $blocks);
        
        $__internal_70a12db4e84a6f3fc302a2111d7845aa8c3e915998fb6b1cdea6ffaac5487bb9->leave($__internal_70a12db4e84a6f3fc302a2111d7845aa8c3e915998fb6b1cdea6ffaac5487bb9_prof);

        
        $__internal_57cad15412d9dcd00a9c84183ae53656959818bb15d0af9c872d31c9987da856->leave($__internal_57cad15412d9dcd00a9c84183ae53656959818bb15d0af9c872d31c9987da856_prof);

    }

    // line 37
    public function block_dateinterval_widget($context, array $blocks = array())
    {
        $__internal_4135faacf44f0041a5acf2b4f272504dd4015f318fa4fd89957e6f497c3517c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4135faacf44f0041a5acf2b4f272504dd4015f318fa4fd89957e6f497c3517c1->enter($__internal_4135faacf44f0041a5acf2b4f272504dd4015f318fa4fd89957e6f497c3517c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        $__internal_450e0f17f8facf8334026d315861793dccf20d88ad3997bdffb1a00cdc3441a6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_450e0f17f8facf8334026d315861793dccf20d88ad3997bdffb1a00cdc3441a6->enter($__internal_450e0f17f8facf8334026d315861793dccf20d88ad3997bdffb1a00cdc3441a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        // line 38
        if (((($context["widget"] ?? $this->getContext($context, "widget")) != "single_text") &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            // line 39
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 40
            $context["valid"] = true;
        }
        // line 42
        $this->displayParentBlock("dateinterval_widget", $context, $blocks);
        
        $__internal_450e0f17f8facf8334026d315861793dccf20d88ad3997bdffb1a00cdc3441a6->leave($__internal_450e0f17f8facf8334026d315861793dccf20d88ad3997bdffb1a00cdc3441a6_prof);

        
        $__internal_4135faacf44f0041a5acf2b4f272504dd4015f318fa4fd89957e6f497c3517c1->leave($__internal_4135faacf44f0041a5acf2b4f272504dd4015f318fa4fd89957e6f497c3517c1_prof);

    }

    // line 45
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_869fea0cc13a38351c17f450003448c9361c035e4cd9ec06680571fb13970462 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_869fea0cc13a38351c17f450003448c9361c035e4cd9ec06680571fb13970462->enter($__internal_869fea0cc13a38351c17f450003448c9361c035e4cd9ec06680571fb13970462_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_057e10dfa0ca3208398e8436bdc5b10017712aa54d9e3eac0f733291c9018a4d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_057e10dfa0ca3208398e8436bdc5b10017712aa54d9e3eac0f733291c9018a4d->enter($__internal_057e10dfa0ca3208398e8436bdc5b10017712aa54d9e3eac0f733291c9018a4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 46
        echo "<div class=\"input-group";
        echo (( !($context["valid"] ?? $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
        echo "\">
        ";
        // line 47
        $context["valid"] = true;
        // line 48
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 49
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_057e10dfa0ca3208398e8436bdc5b10017712aa54d9e3eac0f733291c9018a4d->leave($__internal_057e10dfa0ca3208398e8436bdc5b10017712aa54d9e3eac0f733291c9018a4d_prof);

        
        $__internal_869fea0cc13a38351c17f450003448c9361c035e4cd9ec06680571fb13970462->leave($__internal_869fea0cc13a38351c17f450003448c9361c035e4cd9ec06680571fb13970462_prof);

    }

    // line 53
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_fe687eed29044535b600f1690a64bc76b2ccaa38bebbaa557f7a40b3a5fe4d16 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe687eed29044535b600f1690a64bc76b2ccaa38bebbaa557f7a40b3a5fe4d16->enter($__internal_fe687eed29044535b600f1690a64bc76b2ccaa38bebbaa557f7a40b3a5fe4d16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_c30af634d942d171bbee2647a592faa2bd2b61e8a09c4174349dbecf00dd7152 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c30af634d942d171bbee2647a592faa2bd2b61e8a09c4174349dbecf00dd7152->enter($__internal_c30af634d942d171bbee2647a592faa2bd2b61e8a09c4174349dbecf00dd7152_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 54
        if (( !array_key_exists("type", $context) || (($context["type"] ?? $this->getContext($context, "type")) != "hidden"))) {
            // line 55
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control") . (((((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? $this->getContext($context, "type")), "")) : ("")) == "file")) ? ("-file") : (""))))));
        }
        // line 57
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_c30af634d942d171bbee2647a592faa2bd2b61e8a09c4174349dbecf00dd7152->leave($__internal_c30af634d942d171bbee2647a592faa2bd2b61e8a09c4174349dbecf00dd7152_prof);

        
        $__internal_fe687eed29044535b600f1690a64bc76b2ccaa38bebbaa557f7a40b3a5fe4d16->leave($__internal_fe687eed29044535b600f1690a64bc76b2ccaa38bebbaa557f7a40b3a5fe4d16_prof);

    }

    // line 60
    public function block_widget_attributes($context, array $blocks = array())
    {
        $__internal_6b6cd6f4450f57bf627c873b9cb9a7c3e14ff064cf7cdd66b450c59ae3e7eb38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b6cd6f4450f57bf627c873b9cb9a7c3e14ff064cf7cdd66b450c59ae3e7eb38->enter($__internal_6b6cd6f4450f57bf627c873b9cb9a7c3e14ff064cf7cdd66b450c59ae3e7eb38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        $__internal_823bce16a7a96c85ef5269045f95c64b0a5882e5ed630678c3e7e0a56e67d867 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_823bce16a7a96c85ef5269045f95c64b0a5882e5ed630678c3e7e0a56e67d867->enter($__internal_823bce16a7a96c85ef5269045f95c64b0a5882e5ed630678c3e7e0a56e67d867_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "widget_attributes"));

        // line 61
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            // line 62
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            // line 63
            echo "    ";
        }
        // line 64
        $this->displayParentBlock("widget_attributes", $context, $blocks);
        
        $__internal_823bce16a7a96c85ef5269045f95c64b0a5882e5ed630678c3e7e0a56e67d867->leave($__internal_823bce16a7a96c85ef5269045f95c64b0a5882e5ed630678c3e7e0a56e67d867_prof);

        
        $__internal_6b6cd6f4450f57bf627c873b9cb9a7c3e14ff064cf7cdd66b450c59ae3e7eb38->leave($__internal_6b6cd6f4450f57bf627c873b9cb9a7c3e14ff064cf7cdd66b450c59ae3e7eb38_prof);

    }

    // line 67
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_f15051bc19e3edf744bda37b27377427c73599ba9474023f4a067ce7453e9799 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f15051bc19e3edf744bda37b27377427c73599ba9474023f4a067ce7453e9799->enter($__internal_f15051bc19e3edf744bda37b27377427c73599ba9474023f4a067ce7453e9799_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_e288d33644f497d453323c8baa772273a2d53363ad789f7f3f91f77d2c7b0ab1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e288d33644f497d453323c8baa772273a2d53363ad789f7f3f91f77d2c7b0ab1->enter($__internal_e288d33644f497d453323c8baa772273a2d53363ad789f7f3f91f77d2c7b0ab1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 68
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "btn-secondary")) : ("btn-secondary")) . " btn"))));
        // line 69
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_e288d33644f497d453323c8baa772273a2d53363ad789f7f3f91f77d2c7b0ab1->leave($__internal_e288d33644f497d453323c8baa772273a2d53363ad789f7f3f91f77d2c7b0ab1_prof);

        
        $__internal_f15051bc19e3edf744bda37b27377427c73599ba9474023f4a067ce7453e9799->leave($__internal_f15051bc19e3edf744bda37b27377427c73599ba9474023f4a067ce7453e9799_prof);

    }

    // line 72
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_dcbd453125e301a0120e59bf063e7f658ddebc56f96465fe934f838de23edef4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcbd453125e301a0120e59bf063e7f658ddebc56f96465fe934f838de23edef4->enter($__internal_dcbd453125e301a0120e59bf063e7f658ddebc56f96465fe934f838de23edef4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_822e9effd27d5d682ab711129d4f989295bdf1bc6b8cb0169c2e9f7bacb8b35b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_822e9effd27d5d682ab711129d4f989295bdf1bc6b8cb0169c2e9f7bacb8b35b->enter($__internal_822e9effd27d5d682ab711129d4f989295bdf1bc6b8cb0169c2e9f7bacb8b35b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 73
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 74
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-check-input"))));
        // line 75
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 76
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 78
            echo "<div class=\"form-check";
            echo (( !($context["valid"] ?? $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
            echo "\">";
            // line 79
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 80
            echo "</div>";
        }
        
        $__internal_822e9effd27d5d682ab711129d4f989295bdf1bc6b8cb0169c2e9f7bacb8b35b->leave($__internal_822e9effd27d5d682ab711129d4f989295bdf1bc6b8cb0169c2e9f7bacb8b35b_prof);

        
        $__internal_dcbd453125e301a0120e59bf063e7f658ddebc56f96465fe934f838de23edef4->leave($__internal_dcbd453125e301a0120e59bf063e7f658ddebc56f96465fe934f838de23edef4_prof);

    }

    // line 84
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_572d38479846c2ae79bd04f95861e582b6ed4d09dfcd55d19877401995a1d9b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_572d38479846c2ae79bd04f95861e582b6ed4d09dfcd55d19877401995a1d9b7->enter($__internal_572d38479846c2ae79bd04f95861e582b6ed4d09dfcd55d19877401995a1d9b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_c0b659cbf8fa8a688b52ad0cdb2d8cc66e09362e7134af58b3dcc1a2e3767987 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0b659cbf8fa8a688b52ad0cdb2d8cc66e09362e7134af58b3dcc1a2e3767987->enter($__internal_c0b659cbf8fa8a688b52ad0cdb2d8cc66e09362e7134af58b3dcc1a2e3767987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 85
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 86
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-check-input"))));
        // line 87
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 88
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 90
            echo "<div class=\"form-check";
            echo (( !($context["valid"] ?? $this->getContext($context, "valid"))) ? (" form-control is-invalid") : (""));
            echo "\">";
            // line 91
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 92
            echo "</div>";
        }
        
        $__internal_c0b659cbf8fa8a688b52ad0cdb2d8cc66e09362e7134af58b3dcc1a2e3767987->leave($__internal_c0b659cbf8fa8a688b52ad0cdb2d8cc66e09362e7134af58b3dcc1a2e3767987_prof);

        
        $__internal_572d38479846c2ae79bd04f95861e582b6ed4d09dfcd55d19877401995a1d9b7->leave($__internal_572d38479846c2ae79bd04f95861e582b6ed4d09dfcd55d19877401995a1d9b7_prof);

    }

    // line 96
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_87d0a44e07707d7882cda0809ad0d19a5f503fe01abdabbccafb72a120c9c76f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87d0a44e07707d7882cda0809ad0d19a5f503fe01abdabbccafb72a120c9c76f->enter($__internal_87d0a44e07707d7882cda0809ad0d19a5f503fe01abdabbccafb72a120c9c76f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_30c7423b77a0f889c09e6d5e2936d91cb55edb9ffee5f678f2d432bf775a4754 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30c7423b77a0f889c09e6d5e2936d91cb55edb9ffee5f678f2d432bf775a4754->enter($__internal_30c7423b77a0f889c09e6d5e2936d91cb55edb9ffee5f678f2d432bf775a4754_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 97
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 98
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 99
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 100
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 101
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain")), "valid" =>                 // line 102
($context["valid"] ?? $this->getContext($context, "valid"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 106
            if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
                // line 107
                $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control is-invalid"))));
            }
            // line 109
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 110
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 111
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 112
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 113
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain")), "valid" => true));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 117
            echo "</div>";
        }
        
        $__internal_30c7423b77a0f889c09e6d5e2936d91cb55edb9ffee5f678f2d432bf775a4754->leave($__internal_30c7423b77a0f889c09e6d5e2936d91cb55edb9ffee5f678f2d432bf775a4754_prof);

        
        $__internal_87d0a44e07707d7882cda0809ad0d19a5f503fe01abdabbccafb72a120c9c76f->leave($__internal_87d0a44e07707d7882cda0809ad0d19a5f503fe01abdabbccafb72a120c9c76f_prof);

    }

    // line 123
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_c4a441d6b33a5467f393e3fa73f6b999acd084b049890c917c5bf6a5cdd17de2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4a441d6b33a5467f393e3fa73f6b999acd084b049890c917c5bf6a5cdd17de2->enter($__internal_c4a441d6b33a5467f393e3fa73f6b999acd084b049890c917c5bf6a5cdd17de2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_c425f181c5870c94cf847f319e9e11ce451eb1c8f5e3b8c4f48267f7f6264219 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c425f181c5870c94cf847f319e9e11ce451eb1c8f5e3b8c4f48267f7f6264219->enter($__internal_c425f181c5870c94cf847f319e9e11ce451eb1c8f5e3b8c4f48267f7f6264219_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 124
        if ((array_key_exists("compound", $context) && ($context["compound"] ?? $this->getContext($context, "compound")))) {
            // line 125
            $context["element"] = "legend";
            // line 126
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " col-form-legend"))));
        } else {
            // line 128
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " form-control-label"))));
        }
        // line 130
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_c425f181c5870c94cf847f319e9e11ce451eb1c8f5e3b8c4f48267f7f6264219->leave($__internal_c425f181c5870c94cf847f319e9e11ce451eb1c8f5e3b8c4f48267f7f6264219_prof);

        
        $__internal_c4a441d6b33a5467f393e3fa73f6b999acd084b049890c917c5bf6a5cdd17de2->leave($__internal_c4a441d6b33a5467f393e3fa73f6b999acd084b049890c917c5bf6a5cdd17de2_prof);

    }

    // line 133
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_7fb9d54a392b27e7ad56ceba1fa81bd08d0daed458a09ff90486ff76954593a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fb9d54a392b27e7ad56ceba1fa81bd08d0daed458a09ff90486ff76954593a3->enter($__internal_7fb9d54a392b27e7ad56ceba1fa81bd08d0daed458a09ff90486ff76954593a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_3ef073a7a9931155f73f0409b5d428f8caafc97cdefaa9d6b2ff00aeb1373a44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ef073a7a9931155f73f0409b5d428f8caafc97cdefaa9d6b2ff00aeb1373a44->enter($__internal_3ef073a7a9931155f73f0409b5d428f8caafc97cdefaa9d6b2ff00aeb1373a44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 135
        if (array_key_exists("widget", $context)) {
            // line 136
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " form-check-label"))));
            // line 137
            if (($context["required"] ?? $this->getContext($context, "required"))) {
                // line 138
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
            }
            // line 140
            if (array_key_exists("parent_label_class", $context)) {
                // line 141
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
            }
            // line 143
            if (( !(($context["label"] ?? $this->getContext($context, "label")) === false) && twig_test_empty(($context["label"] ?? $this->getContext($context, "label"))))) {
                // line 144
                if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                    // line 145
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                     // line 146
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                     // line 147
($context["id"] ?? $this->getContext($context, "id"))));
                } else {
                    // line 150
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
                }
            }
            // line 153
            echo "<label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 154
            echo ($context["widget"] ?? $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !(($context["label"] ?? $this->getContext($context, "label")) === false)) ? ((((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 155
            echo "</label>";
        }
        
        $__internal_3ef073a7a9931155f73f0409b5d428f8caafc97cdefaa9d6b2ff00aeb1373a44->leave($__internal_3ef073a7a9931155f73f0409b5d428f8caafc97cdefaa9d6b2ff00aeb1373a44_prof);

        
        $__internal_7fb9d54a392b27e7ad56ceba1fa81bd08d0daed458a09ff90486ff76954593a3->leave($__internal_7fb9d54a392b27e7ad56ceba1fa81bd08d0daed458a09ff90486ff76954593a3_prof);

    }

    // line 161
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_3b85841ec28f34b3368d4bdb8a20fee5abcf9cfb7eeb43581c43bfe5877fc0d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b85841ec28f34b3368d4bdb8a20fee5abcf9cfb7eeb43581c43bfe5877fc0d4->enter($__internal_3b85841ec28f34b3368d4bdb8a20fee5abcf9cfb7eeb43581c43bfe5877fc0d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_71480990ddb08adea60341bffc1e414740d16a89ff2075cd5e6dd04da09bbd84 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71480990ddb08adea60341bffc1e414740d16a89ff2075cd5e6dd04da09bbd84->enter($__internal_71480990ddb08adea60341bffc1e414740d16a89ff2075cd5e6dd04da09bbd84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 162
        if ((array_key_exists("compound", $context) && ($context["compound"] ?? $this->getContext($context, "compound")))) {
            // line 163
            $context["element"] = "fieldset";
        }
        // line 165
        echo "<";
        echo twig_escape_filter($this->env, ((array_key_exists("element", $context)) ? (_twig_default_filter(($context["element"] ?? $this->getContext($context, "element")), "div")) : ("div")), "html", null, true);
        echo " class=\"form-group\">";
        // line 166
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 167
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 168
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 169
        echo "</";
        echo twig_escape_filter($this->env, ((array_key_exists("element", $context)) ? (_twig_default_filter(($context["element"] ?? $this->getContext($context, "element")), "div")) : ("div")), "html", null, true);
        echo ">";
        
        $__internal_71480990ddb08adea60341bffc1e414740d16a89ff2075cd5e6dd04da09bbd84->leave($__internal_71480990ddb08adea60341bffc1e414740d16a89ff2075cd5e6dd04da09bbd84_prof);

        
        $__internal_3b85841ec28f34b3368d4bdb8a20fee5abcf9cfb7eeb43581c43bfe5877fc0d4->leave($__internal_3b85841ec28f34b3368d4bdb8a20fee5abcf9cfb7eeb43581c43bfe5877fc0d4_prof);

    }

    // line 174
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_2e8044e50017db8caba2c7fe4092e4635e9fa9b9399d3272d2c66e841f0d191e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e8044e50017db8caba2c7fe4092e4635e9fa9b9399d3272d2c66e841f0d191e->enter($__internal_2e8044e50017db8caba2c7fe4092e4635e9fa9b9399d3272d2c66e841f0d191e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_f0a70a776f5994d46f81969866baf9620bab68da18efd0a572c85c2546c3c5a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0a70a776f5994d46f81969866baf9620bab68da18efd0a572c85c2546c3c5a4->enter($__internal_f0a70a776f5994d46f81969866baf9620bab68da18efd0a572c85c2546c3c5a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 175
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 176
            echo "<div class=\"";
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "invalid-feedback";
            } else {
                echo "alert alert-danger";
            }
            echo "\">
        <ul class=\"list-unstyled mb-0\">";
            // line 178
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 179
                echo "<li>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 181
            echo "</ul>
    </div>";
        }
        
        $__internal_f0a70a776f5994d46f81969866baf9620bab68da18efd0a572c85c2546c3c5a4->leave($__internal_f0a70a776f5994d46f81969866baf9620bab68da18efd0a572c85c2546c3c5a4_prof);

        
        $__internal_2e8044e50017db8caba2c7fe4092e4635e9fa9b9399d3272d2c66e841f0d191e->leave($__internal_2e8044e50017db8caba2c7fe4092e4635e9fa9b9399d3272d2c66e841f0d191e_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_4_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  672 => 181,  664 => 179,  660 => 178,  651 => 176,  649 => 175,  640 => 174,  628 => 169,  626 => 168,  624 => 167,  622 => 166,  618 => 165,  615 => 163,  613 => 162,  604 => 161,  593 => 155,  589 => 154,  574 => 153,  570 => 150,  567 => 147,  566 => 146,  565 => 145,  563 => 144,  561 => 143,  558 => 141,  556 => 140,  553 => 138,  551 => 137,  549 => 136,  547 => 135,  538 => 133,  528 => 130,  525 => 128,  522 => 126,  520 => 125,  518 => 124,  509 => 123,  498 => 117,  492 => 113,  491 => 112,  490 => 111,  486 => 110,  482 => 109,  479 => 107,  477 => 106,  470 => 102,  469 => 101,  468 => 100,  467 => 99,  463 => 98,  461 => 97,  452 => 96,  441 => 92,  439 => 91,  435 => 90,  432 => 88,  430 => 87,  428 => 86,  426 => 85,  417 => 84,  406 => 80,  404 => 79,  400 => 78,  397 => 76,  395 => 75,  393 => 74,  391 => 73,  382 => 72,  372 => 69,  370 => 68,  361 => 67,  351 => 64,  348 => 63,  345 => 62,  343 => 61,  334 => 60,  324 => 57,  321 => 55,  319 => 54,  310 => 53,  299 => 49,  297 => 48,  295 => 47,  290 => 46,  281 => 45,  271 => 42,  268 => 40,  266 => 39,  264 => 38,  255 => 37,  245 => 34,  242 => 32,  240 => 31,  238 => 30,  229 => 29,  219 => 26,  216 => 24,  214 => 23,  212 => 22,  203 => 21,  193 => 18,  190 => 16,  188 => 15,  186 => 14,  177 => 13,  167 => 10,  164 => 9,  161 => 8,  158 => 7,  156 => 6,  147 => 5,  137 => 174,  134 => 173,  131 => 171,  129 => 161,  126 => 160,  123 => 158,  121 => 133,  118 => 132,  116 => 123,  113 => 122,  110 => 120,  108 => 96,  105 => 95,  103 => 84,  100 => 83,  98 => 72,  95 => 71,  93 => 67,  91 => 60,  89 => 53,  86 => 52,  84 => 45,  81 => 44,  79 => 37,  76 => 36,  74 => 29,  71 => 28,  69 => 21,  66 => 20,  64 => 13,  61 => 12,  59 => 5,  56 => 4,  53 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_base_layout.html.twig\" %}

{# Widgets #}

{% block money_widget -%}
    {% if not valid %}
        {% set group_class = ' form-control is-invalid' %}
        {% set valid = true %}
    {% endif %}
    {{- parent() -}}
{%- endblock money_widget %}

{% block datetime_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock date_widget %}

{% block time_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock time_widget %}

{% block dateinterval_widget -%}
    {%- if widget != 'single_text' and not valid -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) -%}
        {% set valid = true %}
    {%- endif -%}
    {{- parent() -}}
{%- endblock dateinterval_widget %}

{% block percent_widget -%}
    <div class=\"input-group{{ not valid ? ' form-control is-invalid' }}\">
        {% set valid = true %}
        {{- block('form_widget_simple') -}}
        <span class=\"input-group-addon\">%</span>
    </div>
{%- endblock percent_widget %}

{% block form_widget_simple -%}
    {% if type is not defined or type != 'hidden' %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control' ~ (type|default('') == 'file' ? '-file' : ''))|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{%- block widget_attributes -%}
    {%- if not valid %}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) %}
    {% endif -%}
    {{ parent() }}
{%- endblock widget_attributes -%}

{% block button_widget -%}
    {%- set attr = attr|merge({class: (attr.class|default('btn-secondary') ~ ' btn')|trim}) -%}
    {{- parent() -}}
{%- endblock button_widget %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-check-input')|trim}) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"form-check{{ not valid ? ' form-control is-invalid' }}\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-check-input')|trim}) -%}
    {%- if 'radio-inline' in parent_label_class -%}
        {{- form_label(form, null, { widget: parent() }) -}}
    {%- else -%}
        <div class=\"form-check{{ not valid ? ' form-control is-invalid' }}\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif -%}
{%- endblock radio_widget %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') -%}
        {%- for child in form %}
            {{- form_widget(child, {
                parent_label_class: label_attr.class|default(''),
                translation_domain: choice_translation_domain,
                valid: valid,
            }) -}}
        {% endfor -%}
    {%- else -%}
        {%- if not valid -%}
            {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control is-invalid')|trim}) %}
        {%- endif -%}
        <div {{ block('widget_container_attributes') }}>
            {%- for child in form %}
                {{- form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                    translation_domain: choice_translation_domain,
                    valid: true,
                }) -}}
            {% endfor -%}
        </div>
    {%- endif %}
{%- endblock choice_widget_expanded %}

{# Labels #}

{% block form_label -%}
    {%- if compound is defined and compound -%}
        {%- set element = 'legend' -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' col-form-legend')|trim}) -%}
    {%- else -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' form-control-label')|trim}) -%}
    {%- endif -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block checkbox_radio_label -%}
    {#- Do not display the label if widget is not defined in order to prevent double label rendering -#}
    {%- if widget is defined -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' form-check-label')|trim}) -%}
        {%- if required -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) -%}
        {%- endif -%}
        {%- if parent_label_class is defined -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) -%}
        {%- endif -%}
        {%- if label is not same as(false) and label is empty -%}
            {%- if label_format is not empty -%}
                {%- set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) -%}
            {%- else -%}
                {%- set label = name|humanize -%}
            {%- endif -%}
        {%- endif -%}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {%- endif -%}
{%- endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    {%- if compound is defined and compound -%}
        {%- set element = 'fieldset' -%}
    {%- endif -%}
    <{{ element|default('div') }} class=\"form-group\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </{{ element|default('div') }}>
{%- endblock form_row %}

{# Errors #}

{% block form_errors -%}
    {%- if errors|length > 0 -%}
    <div class=\"{% if form.parent %}invalid-feedback{% else %}alert alert-danger{% endif %}\">
        <ul class=\"list-unstyled mb-0\">
            {%- for error in errors -%}
                <li>{{ error.message }}</li>
            {%- endfor -%}
        </ul>
    </div>
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_4_layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_4_layout.html.twig");
    }
}
