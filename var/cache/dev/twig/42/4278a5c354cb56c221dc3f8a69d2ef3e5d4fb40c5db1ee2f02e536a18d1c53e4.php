<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_c9f78952cac0f06f836bef76a076f48afb0514ecc489dab37a42fd5e69371b77 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_250ebc953eb52cf826a0fd4a016a2653e6c14dda99f24cca75c9c014b7e1b9a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_250ebc953eb52cf826a0fd4a016a2653e6c14dda99f24cca75c9c014b7e1b9a8->enter($__internal_250ebc953eb52cf826a0fd4a016a2653e6c14dda99f24cca75c9c014b7e1b9a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_d3d3e6abe450e398dd367744e12185f1bf832edb8b8ec588c3e3bbd31993bab7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3d3e6abe450e398dd367744e12185f1bf832edb8b8ec588c3e3bbd31993bab7->enter($__internal_d3d3e6abe450e398dd367744e12185f1bf832edb8b8ec588c3e3bbd31993bab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_250ebc953eb52cf826a0fd4a016a2653e6c14dda99f24cca75c9c014b7e1b9a8->leave($__internal_250ebc953eb52cf826a0fd4a016a2653e6c14dda99f24cca75c9c014b7e1b9a8_prof);

        
        $__internal_d3d3e6abe450e398dd367744e12185f1bf832edb8b8ec588c3e3bbd31993bab7->leave($__internal_d3d3e6abe450e398dd367744e12185f1bf832edb8b8ec588c3e3bbd31993bab7_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_f32a7b8593f94d5b080c963f0c5e8a9de0d5d537f61234edc250b3f9115c3fe7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f32a7b8593f94d5b080c963f0c5e8a9de0d5d537f61234edc250b3f9115c3fe7->enter($__internal_f32a7b8593f94d5b080c963f0c5e8a9de0d5d537f61234edc250b3f9115c3fe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_855a03e10f63f8443e0cfb6a2139e498a4cb03809f261725f186046b537f3227 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_855a03e10f63f8443e0cfb6a2139e498a4cb03809f261725f186046b537f3227->enter($__internal_855a03e10f63f8443e0cfb6a2139e498a4cb03809f261725f186046b537f3227_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_855a03e10f63f8443e0cfb6a2139e498a4cb03809f261725f186046b537f3227->leave($__internal_855a03e10f63f8443e0cfb6a2139e498a4cb03809f261725f186046b537f3227_prof);

        
        $__internal_f32a7b8593f94d5b080c963f0c5e8a9de0d5d537f61234edc250b3f9115c3fe7->leave($__internal_f32a7b8593f94d5b080c963f0c5e8a9de0d5d537f61234edc250b3f9115c3fe7_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_34dea5c6e5aadb9897e774b2489ccdcbd18af7ec0bf803e9612b01a380d5bd92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34dea5c6e5aadb9897e774b2489ccdcbd18af7ec0bf803e9612b01a380d5bd92->enter($__internal_34dea5c6e5aadb9897e774b2489ccdcbd18af7ec0bf803e9612b01a380d5bd92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_1fff9329e19d91a6e211df9b024ddf7483ac602b557e5755012b556accd7c120 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fff9329e19d91a6e211df9b024ddf7483ac602b557e5755012b556accd7c120->enter($__internal_1fff9329e19d91a6e211df9b024ddf7483ac602b557e5755012b556accd7c120_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_1fff9329e19d91a6e211df9b024ddf7483ac602b557e5755012b556accd7c120->leave($__internal_1fff9329e19d91a6e211df9b024ddf7483ac602b557e5755012b556accd7c120_prof);

        
        $__internal_34dea5c6e5aadb9897e774b2489ccdcbd18af7ec0bf803e9612b01a380d5bd92->leave($__internal_34dea5c6e5aadb9897e774b2489ccdcbd18af7ec0bf803e9612b01a380d5bd92_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_15fa0979635547f8bb1855b4a4d0770b11c429c86b15b302dd6050a2f204affc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_15fa0979635547f8bb1855b4a4d0770b11c429c86b15b302dd6050a2f204affc->enter($__internal_15fa0979635547f8bb1855b4a4d0770b11c429c86b15b302dd6050a2f204affc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e8d78dd9f7f8b9f6043fe3f6414db15ad4bd5ad7b8aead1b374b26f63c58632e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8d78dd9f7f8b9f6043fe3f6414db15ad4bd5ad7b8aead1b374b26f63c58632e->enter($__internal_e8d78dd9f7f8b9f6043fe3f6414db15ad4bd5ad7b8aead1b374b26f63c58632e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_e8d78dd9f7f8b9f6043fe3f6414db15ad4bd5ad7b8aead1b374b26f63c58632e->leave($__internal_e8d78dd9f7f8b9f6043fe3f6414db15ad4bd5ad7b8aead1b374b26f63c58632e_prof);

        
        $__internal_15fa0979635547f8bb1855b4a4d0770b11c429c86b15b302dd6050a2f204affc->leave($__internal_15fa0979635547f8bb1855b4a4d0770b11c429c86b15b302dd6050a2f204affc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
