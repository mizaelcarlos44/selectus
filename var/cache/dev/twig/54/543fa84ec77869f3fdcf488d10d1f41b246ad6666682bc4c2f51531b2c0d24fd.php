<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_5617e637abbc5907a44f7f726adec1dbeceeb0872bc5d3f5d4b0ac330afbcc53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a29fd4ea92b52a469a47df87365ce6c84eeb08bdfa0f87fa1f885b6fad9ee1fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a29fd4ea92b52a469a47df87365ce6c84eeb08bdfa0f87fa1f885b6fad9ee1fa->enter($__internal_a29fd4ea92b52a469a47df87365ce6c84eeb08bdfa0f87fa1f885b6fad9ee1fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_0b71af0012903b33487bc1239b28a5f4967c1df383e293b23aeb3c7cea60591c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b71af0012903b33487bc1239b28a5f4967c1df383e293b23aeb3c7cea60591c->enter($__internal_0b71af0012903b33487bc1239b28a5f4967c1df383e293b23aeb3c7cea60591c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_a29fd4ea92b52a469a47df87365ce6c84eeb08bdfa0f87fa1f885b6fad9ee1fa->leave($__internal_a29fd4ea92b52a469a47df87365ce6c84eeb08bdfa0f87fa1f885b6fad9ee1fa_prof);

        
        $__internal_0b71af0012903b33487bc1239b28a5f4967c1df383e293b23aeb3c7cea60591c->leave($__internal_0b71af0012903b33487bc1239b28a5f4967c1df383e293b23aeb3c7cea60591c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_errors.html.php");
    }
}
