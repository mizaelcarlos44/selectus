<?php

/* bootstrap_4_horizontal_layout.html.twig */
class __TwigTemplate_667226cd829e40b6ee91ed40423295dcde227271e6470ec03d289f9668fae665 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("bootstrap_4_layout.html.twig", "bootstrap_4_horizontal_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."bootstrap_4_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_label' => array($this, 'block_form_label'),
                'form_label_class' => array($this, 'block_form_label_class'),
                'form_row' => array($this, 'block_form_row'),
                'fieldset_form_row' => array($this, 'block_fieldset_form_row'),
                'submit_row' => array($this, 'block_submit_row'),
                'reset_row' => array($this, 'block_reset_row'),
                'form_group_class' => array($this, 'block_form_group_class'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_18878c384a276d440d80d9a5cd5fa4401ed104bf19cbec562cd02629ad6111cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_18878c384a276d440d80d9a5cd5fa4401ed104bf19cbec562cd02629ad6111cf->enter($__internal_18878c384a276d440d80d9a5cd5fa4401ed104bf19cbec562cd02629ad6111cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_horizontal_layout.html.twig"));

        $__internal_3ddaeb151a9743bb1d5cb77871d5ed6d256f9cb723ea798da8e881815b7c2543 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ddaeb151a9743bb1d5cb77871d5ed6d256f9cb723ea798da8e881815b7c2543->enter($__internal_3ddaeb151a9743bb1d5cb77871d5ed6d256f9cb723ea798da8e881815b7c2543_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_4_horizontal_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_label', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('form_label_class', $context, $blocks);
        // line 20
        echo "
";
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('form_row', $context, $blocks);
        // line 36
        echo "
";
        // line 37
        $this->displayBlock('fieldset_form_row', $context, $blocks);
        // line 48
        echo "
";
        // line 49
        $this->displayBlock('submit_row', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('reset_row', $context, $blocks);
        // line 66
        echo "
";
        // line 67
        $this->displayBlock('form_group_class', $context, $blocks);
        // line 70
        echo "
";
        // line 71
        $this->displayBlock('checkbox_row', $context, $blocks);
        
        $__internal_18878c384a276d440d80d9a5cd5fa4401ed104bf19cbec562cd02629ad6111cf->leave($__internal_18878c384a276d440d80d9a5cd5fa4401ed104bf19cbec562cd02629ad6111cf_prof);

        
        $__internal_3ddaeb151a9743bb1d5cb77871d5ed6d256f9cb723ea798da8e881815b7c2543->leave($__internal_3ddaeb151a9743bb1d5cb77871d5ed6d256f9cb723ea798da8e881815b7c2543_prof);

    }

    // line 5
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_8d272a779531b89b2e5d9affd2869575a4f23ebee2c91a58cf7ae901b69736c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d272a779531b89b2e5d9affd2869575a4f23ebee2c91a58cf7ae901b69736c0->enter($__internal_8d272a779531b89b2e5d9affd2869575a4f23ebee2c91a58cf7ae901b69736c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_b91073cb67861aafe7ded16c9f61d420834711ff7de6845c15a9bb61bd3ff6ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b91073cb67861aafe7ded16c9f61d420834711ff7de6845c15a9bb61bd3ff6ab->enter($__internal_b91073cb67861aafe7ded16c9f61d420834711ff7de6845c15a9bb61bd3ff6ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 6
        if ((($context["label"] ?? $this->getContext($context, "label")) === false)) {
            // line 7
            echo "<div class=\"";
            $this->displayBlock("form_label_class", $context, $blocks);
            echo "\"></div>";
        } else {
            // line 9
            if (( !array_key_exists("expanded", $context) ||  !($context["expanded"] ?? $this->getContext($context, "expanded")))) {
                // line 10
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " col-form-label"))));
            }
            // line 12
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") .             $this->renderBlock("form_label_class", $context, $blocks)))));
            // line 13
            $this->displayParentBlock("form_label", $context, $blocks);
        }
        
        $__internal_b91073cb67861aafe7ded16c9f61d420834711ff7de6845c15a9bb61bd3ff6ab->leave($__internal_b91073cb67861aafe7ded16c9f61d420834711ff7de6845c15a9bb61bd3ff6ab_prof);

        
        $__internal_8d272a779531b89b2e5d9affd2869575a4f23ebee2c91a58cf7ae901b69736c0->leave($__internal_8d272a779531b89b2e5d9affd2869575a4f23ebee2c91a58cf7ae901b69736c0_prof);

    }

    // line 17
    public function block_form_label_class($context, array $blocks = array())
    {
        $__internal_c65d0050d1e85a2e8d9609271a2456a3907c51cfdb9d54e81455416f6bacdcaf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c65d0050d1e85a2e8d9609271a2456a3907c51cfdb9d54e81455416f6bacdcaf->enter($__internal_c65d0050d1e85a2e8d9609271a2456a3907c51cfdb9d54e81455416f6bacdcaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        $__internal_7fa3bd5b59fd1b744d2fb1299fb1cf2a695d6039c723a8515e4b76dfd6372c61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7fa3bd5b59fd1b744d2fb1299fb1cf2a695d6039c723a8515e4b76dfd6372c61->enter($__internal_7fa3bd5b59fd1b744d2fb1299fb1cf2a695d6039c723a8515e4b76dfd6372c61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label_class"));

        // line 18
        echo "col-sm-2";
        
        $__internal_7fa3bd5b59fd1b744d2fb1299fb1cf2a695d6039c723a8515e4b76dfd6372c61->leave($__internal_7fa3bd5b59fd1b744d2fb1299fb1cf2a695d6039c723a8515e4b76dfd6372c61_prof);

        
        $__internal_c65d0050d1e85a2e8d9609271a2456a3907c51cfdb9d54e81455416f6bacdcaf->leave($__internal_c65d0050d1e85a2e8d9609271a2456a3907c51cfdb9d54e81455416f6bacdcaf_prof);

    }

    // line 23
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_1cd338cbde4f16eda99843bd998b54abc2623912ba4a758c0a0147fc02398420 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1cd338cbde4f16eda99843bd998b54abc2623912ba4a758c0a0147fc02398420->enter($__internal_1cd338cbde4f16eda99843bd998b54abc2623912ba4a758c0a0147fc02398420_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_fc6b14588d24c33e3bd1c1ebd9cc8aa33906b229995c2dc6431a9d5e23b7d893 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc6b14588d24c33e3bd1c1ebd9cc8aa33906b229995c2dc6431a9d5e23b7d893->enter($__internal_fc6b14588d24c33e3bd1c1ebd9cc8aa33906b229995c2dc6431a9d5e23b7d893_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 24
        if ((array_key_exists("expanded", $context) && ($context["expanded"] ?? $this->getContext($context, "expanded")))) {
            // line 25
            $this->displayBlock("fieldset_form_row", $context, $blocks);
        } else {
            // line 27
            echo "<div class=\"form-group row";
            if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
                echo " is-invalid";
            }
            echo "\">";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
            // line 29
            echo "<div class=\"";
            $this->displayBlock("form_group_class", $context, $blocks);
            echo "\">";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 32
            echo "</div>
    ";
            // line 33
            echo "</div>";
        }
        
        $__internal_fc6b14588d24c33e3bd1c1ebd9cc8aa33906b229995c2dc6431a9d5e23b7d893->leave($__internal_fc6b14588d24c33e3bd1c1ebd9cc8aa33906b229995c2dc6431a9d5e23b7d893_prof);

        
        $__internal_1cd338cbde4f16eda99843bd998b54abc2623912ba4a758c0a0147fc02398420->leave($__internal_1cd338cbde4f16eda99843bd998b54abc2623912ba4a758c0a0147fc02398420_prof);

    }

    // line 37
    public function block_fieldset_form_row($context, array $blocks = array())
    {
        $__internal_266a18954cdb550e31faa1b81aa688fcff3d07384658571fb2e2bc5c3869e7ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_266a18954cdb550e31faa1b81aa688fcff3d07384658571fb2e2bc5c3869e7ca->enter($__internal_266a18954cdb550e31faa1b81aa688fcff3d07384658571fb2e2bc5c3869e7ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fieldset_form_row"));

        $__internal_2d6d029a52793f317b59d643f46cbe1592450ecf15c9afca1991a0df8cba6254 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d6d029a52793f317b59d643f46cbe1592450ecf15c9afca1991a0df8cba6254->enter($__internal_2d6d029a52793f317b59d643f46cbe1592450ecf15c9afca1991a0df8cba6254_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fieldset_form_row"));

        // line 38
        echo "<fieldset class=\"form-group\">
        <div class=\"row";
        // line 39
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " is-invalid";
        }
        echo "\">";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 41
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 44
        echo "</div>
        </div>
";
        // line 46
        echo "</fieldset>";
        
        $__internal_2d6d029a52793f317b59d643f46cbe1592450ecf15c9afca1991a0df8cba6254->leave($__internal_2d6d029a52793f317b59d643f46cbe1592450ecf15c9afca1991a0df8cba6254_prof);

        
        $__internal_266a18954cdb550e31faa1b81aa688fcff3d07384658571fb2e2bc5c3869e7ca->leave($__internal_266a18954cdb550e31faa1b81aa688fcff3d07384658571fb2e2bc5c3869e7ca_prof);

    }

    // line 49
    public function block_submit_row($context, array $blocks = array())
    {
        $__internal_bf6a6cb3f74ffb7a4edbbd47105f734d405834603c6eaf6428554bf7e96b2911 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf6a6cb3f74ffb7a4edbbd47105f734d405834603c6eaf6428554bf7e96b2911->enter($__internal_bf6a6cb3f74ffb7a4edbbd47105f734d405834603c6eaf6428554bf7e96b2911_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        $__internal_929f758157673b8bf74f7ef55702e217e1b882b22c32c62dd45324f20b4be87c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_929f758157673b8bf74f7ef55702e217e1b882b22c32c62dd45324f20b4be87c->enter($__internal_929f758157673b8bf74f7ef55702e217e1b882b22c32c62dd45324f20b4be87c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "submit_row"));

        // line 50
        echo "<div class=\"form-group row\">";
        // line 51
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 52
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 54
        echo "</div>";
        // line 55
        echo "</div>";
        
        $__internal_929f758157673b8bf74f7ef55702e217e1b882b22c32c62dd45324f20b4be87c->leave($__internal_929f758157673b8bf74f7ef55702e217e1b882b22c32c62dd45324f20b4be87c_prof);

        
        $__internal_bf6a6cb3f74ffb7a4edbbd47105f734d405834603c6eaf6428554bf7e96b2911->leave($__internal_bf6a6cb3f74ffb7a4edbbd47105f734d405834603c6eaf6428554bf7e96b2911_prof);

    }

    // line 58
    public function block_reset_row($context, array $blocks = array())
    {
        $__internal_64100c7b112c443406bb859c15dc51721653f9b2502570a10d76fbc822202024 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64100c7b112c443406bb859c15dc51721653f9b2502570a10d76fbc822202024->enter($__internal_64100c7b112c443406bb859c15dc51721653f9b2502570a10d76fbc822202024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        $__internal_593137a02b27b3c659bfb6fa86360192b84c47528b0a9b7e09ad6c32e86b8d83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_593137a02b27b3c659bfb6fa86360192b84c47528b0a9b7e09ad6c32e86b8d83->enter($__internal_593137a02b27b3c659bfb6fa86360192b84c47528b0a9b7e09ad6c32e86b8d83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "reset_row"));

        // line 59
        echo "<div class=\"form-group row\">";
        // line 60
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 61
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 63
        echo "</div>";
        // line 64
        echo "</div>";
        
        $__internal_593137a02b27b3c659bfb6fa86360192b84c47528b0a9b7e09ad6c32e86b8d83->leave($__internal_593137a02b27b3c659bfb6fa86360192b84c47528b0a9b7e09ad6c32e86b8d83_prof);

        
        $__internal_64100c7b112c443406bb859c15dc51721653f9b2502570a10d76fbc822202024->leave($__internal_64100c7b112c443406bb859c15dc51721653f9b2502570a10d76fbc822202024_prof);

    }

    // line 67
    public function block_form_group_class($context, array $blocks = array())
    {
        $__internal_85267d1357fea028abb86b6d03acb5843fb44eae351cd35e1996643bddb7eb33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85267d1357fea028abb86b6d03acb5843fb44eae351cd35e1996643bddb7eb33->enter($__internal_85267d1357fea028abb86b6d03acb5843fb44eae351cd35e1996643bddb7eb33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        $__internal_9e5bf5442d89c83f9039f81f2ffff9875633494592d79dbd94ff1e954e6f0152 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e5bf5442d89c83f9039f81f2ffff9875633494592d79dbd94ff1e954e6f0152->enter($__internal_9e5bf5442d89c83f9039f81f2ffff9875633494592d79dbd94ff1e954e6f0152_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_group_class"));

        // line 68
        echo "col-sm-10";
        
        $__internal_9e5bf5442d89c83f9039f81f2ffff9875633494592d79dbd94ff1e954e6f0152->leave($__internal_9e5bf5442d89c83f9039f81f2ffff9875633494592d79dbd94ff1e954e6f0152_prof);

        
        $__internal_85267d1357fea028abb86b6d03acb5843fb44eae351cd35e1996643bddb7eb33->leave($__internal_85267d1357fea028abb86b6d03acb5843fb44eae351cd35e1996643bddb7eb33_prof);

    }

    // line 71
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_69d3742cb188b3eb8155df40fbcbd10b504a2c987a60ffd62dfdc8e5094d14ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69d3742cb188b3eb8155df40fbcbd10b504a2c987a60ffd62dfdc8e5094d14ea->enter($__internal_69d3742cb188b3eb8155df40fbcbd10b504a2c987a60ffd62dfdc8e5094d14ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_cc389dc0ca04a3b0b06a7524b56eefdcb182be21d7756772e0925da869a45b1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc389dc0ca04a3b0b06a7524b56eefdcb182be21d7756772e0925da869a45b1c->enter($__internal_cc389dc0ca04a3b0b06a7524b56eefdcb182be21d7756772e0925da869a45b1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 72
        echo "<div class=\"form-group row\">";
        // line 73
        echo "<div class=\"";
        $this->displayBlock("form_label_class", $context, $blocks);
        echo "\"></div>";
        // line 74
        echo "<div class=\"";
        $this->displayBlock("form_group_class", $context, $blocks);
        echo "\">";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 76
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 77
        echo "</div>";
        // line 78
        echo "</div>";
        
        $__internal_cc389dc0ca04a3b0b06a7524b56eefdcb182be21d7756772e0925da869a45b1c->leave($__internal_cc389dc0ca04a3b0b06a7524b56eefdcb182be21d7756772e0925da869a45b1c_prof);

        
        $__internal_69d3742cb188b3eb8155df40fbcbd10b504a2c987a60ffd62dfdc8e5094d14ea->leave($__internal_69d3742cb188b3eb8155df40fbcbd10b504a2c987a60ffd62dfdc8e5094d14ea_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_4_horizontal_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  347 => 78,  345 => 77,  343 => 76,  341 => 75,  337 => 74,  333 => 73,  331 => 72,  322 => 71,  312 => 68,  303 => 67,  293 => 64,  291 => 63,  289 => 62,  285 => 61,  281 => 60,  279 => 59,  270 => 58,  260 => 55,  258 => 54,  256 => 53,  252 => 52,  248 => 51,  246 => 50,  237 => 49,  227 => 46,  223 => 44,  221 => 43,  219 => 42,  215 => 41,  213 => 40,  208 => 39,  205 => 38,  196 => 37,  185 => 33,  182 => 32,  180 => 31,  178 => 30,  174 => 29,  172 => 28,  166 => 27,  163 => 25,  161 => 24,  152 => 23,  142 => 18,  133 => 17,  122 => 13,  120 => 12,  117 => 10,  115 => 9,  110 => 7,  108 => 6,  99 => 5,  89 => 71,  86 => 70,  84 => 67,  81 => 66,  79 => 58,  76 => 57,  74 => 49,  71 => 48,  69 => 37,  66 => 36,  64 => 23,  61 => 22,  58 => 20,  56 => 17,  53 => 16,  51 => 5,  48 => 4,  45 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"bootstrap_4_layout.html.twig\" %}

{# Labels #}

{% block form_label -%}
    {%- if label is same as(false) -%}
        <div class=\"{{ block('form_label_class') }}\"></div>
    {%- else -%}
        {%- if expanded is not defined or not expanded -%}
            {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' col-form-label')|trim}) -%}
        {%- endif -%}
        {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ block('form_label_class'))|trim}) -%}
        {{- parent() -}}
    {%- endif -%}
{%- endblock form_label %}

{% block form_label_class -%}
col-sm-2
{%- endblock form_label_class %}

{# Rows #}

{% block form_row -%}
    {%- if expanded is defined and expanded -%}
        {{ block('fieldset_form_row') }}
    {%- else -%}
        <div class=\"form-group row{% if (not compound or force_error|default(false)) and not valid %} is-invalid{% endif %}\">
            {{- form_label(form) -}}
            <div class=\"{{ block('form_group_class') }}\">
                {{- form_widget(form) -}}
                {{- form_errors(form) -}}
            </div>
    {##}</div>
    {%- endif -%}
{%- endblock form_row %}

{% block fieldset_form_row -%}
    <fieldset class=\"form-group\">
        <div class=\"row{% if (not compound or force_error|default(false)) and not valid %} is-invalid{% endif %}\">
            {{- form_label(form) -}}
            <div class=\"{{ block('form_group_class') }}\">
                {{- form_widget(form) -}}
                {{- form_errors(form) -}}
            </div>
        </div>
{##}</fieldset>
{%- endblock fieldset_form_row %}

{% block submit_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock submit_row %}

{% block reset_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
        </div>{#--#}
    </div>
{%- endblock reset_row %}

{% block form_group_class -%}
col-sm-10
{%- endblock form_group_class %}

{% block checkbox_row -%}
    <div class=\"form-group row\">{#--#}
        <div class=\"{{ block('form_label_class') }}\"></div>{#--#}
        <div class=\"{{ block('form_group_class') }}\">
            {{- form_widget(form) -}}
            {{- form_errors(form) -}}
        </div>{#--#}
    </div>
{%- endblock checkbox_row %}
", "bootstrap_4_horizontal_layout.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_4_horizontal_layout.html.twig");
    }
}
