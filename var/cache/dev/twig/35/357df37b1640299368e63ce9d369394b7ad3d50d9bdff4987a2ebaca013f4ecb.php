<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_8e3ec9b4855ffd5c7a52c5a7c578d283896ab36df9764c55c6501d798818eb89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_037e3af831371f2c1dcf3a34fbad0618b8f00085273375a1d308121803b3aab2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_037e3af831371f2c1dcf3a34fbad0618b8f00085273375a1d308121803b3aab2->enter($__internal_037e3af831371f2c1dcf3a34fbad0618b8f00085273375a1d308121803b3aab2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_66383926349e525dd0b40fc7a480d5b337a8e6e2a4ecbe0b843d55fb71a28d11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_66383926349e525dd0b40fc7a480d5b337a8e6e2a4ecbe0b843d55fb71a28d11->enter($__internal_66383926349e525dd0b40fc7a480d5b337a8e6e2a4ecbe0b843d55fb71a28d11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_037e3af831371f2c1dcf3a34fbad0618b8f00085273375a1d308121803b3aab2->leave($__internal_037e3af831371f2c1dcf3a34fbad0618b8f00085273375a1d308121803b3aab2_prof);

        
        $__internal_66383926349e525dd0b40fc7a480d5b337a8e6e2a4ecbe0b843d55fb71a28d11->leave($__internal_66383926349e525dd0b40fc7a480d5b337a8e6e2a4ecbe0b843d55fb71a28d11_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_attributes.html.php");
    }
}
