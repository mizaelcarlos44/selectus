<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_79fd4dc20dac5c3d1fbecfb0258b595f3b2311e486bf28752724406086231de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55665cc5864ece539666cf85083dc8c4dcee00e187ef33a5dd2b3d4743b1951d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55665cc5864ece539666cf85083dc8c4dcee00e187ef33a5dd2b3d4743b1951d->enter($__internal_55665cc5864ece539666cf85083dc8c4dcee00e187ef33a5dd2b3d4743b1951d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_4e8ec7b379462d95e15b41ba183671dcc9da8dc9e1573d8aef036c2fd33ea4f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e8ec7b379462d95e15b41ba183671dcc9da8dc9e1573d8aef036c2fd33ea4f0->enter($__internal_4e8ec7b379462d95e15b41ba183671dcc9da8dc9e1573d8aef036c2fd33ea4f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_55665cc5864ece539666cf85083dc8c4dcee00e187ef33a5dd2b3d4743b1951d->leave($__internal_55665cc5864ece539666cf85083dc8c4dcee00e187ef33a5dd2b3d4743b1951d_prof);

        
        $__internal_4e8ec7b379462d95e15b41ba183671dcc9da8dc9e1573d8aef036c2fd33ea4f0->leave($__internal_4e8ec7b379462d95e15b41ba183671dcc9da8dc9e1573d8aef036c2fd33ea4f0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\collection_widget.html.php");
    }
}
