<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_77c469f91eb7870c6523f95c5075bbae8c2dbe0ea18c6f1fa53f85e31cefd268 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_db06b6c05813238173e7e95fccfa2d0d4a0f10d76ef4e8283277ebc406a2830b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db06b6c05813238173e7e95fccfa2d0d4a0f10d76ef4e8283277ebc406a2830b->enter($__internal_db06b6c05813238173e7e95fccfa2d0d4a0f10d76ef4e8283277ebc406a2830b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_5aec406b72f24ac613a620cc9e456f83c971508713fd542bb5799fd3efdb0208 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5aec406b72f24ac613a620cc9e456f83c971508713fd542bb5799fd3efdb0208->enter($__internal_5aec406b72f24ac613a620cc9e456f83c971508713fd542bb5799fd3efdb0208_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
";
        
        $__internal_db06b6c05813238173e7e95fccfa2d0d4a0f10d76ef4e8283277ebc406a2830b->leave($__internal_db06b6c05813238173e7e95fccfa2d0d4a0f10d76ef4e8283277ebc406a2830b_prof);

        
        $__internal_5aec406b72f24ac613a620cc9e456f83c971508713fd542bb5799fd3efdb0208->leave($__internal_5aec406b72f24ac613a620cc9e456f83c971508713fd542bb5799fd3efdb0208_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form); ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\button_row.html.php");
    }
}
