<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_3cb80f5652630e0858780fc311ca8f915e5d8f4d92c0802441d14215ecb380e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2f1850124b1f1fc79ae4f581011a11384b107c853c75d94d8c84b0008932796 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2f1850124b1f1fc79ae4f581011a11384b107c853c75d94d8c84b0008932796->enter($__internal_a2f1850124b1f1fc79ae4f581011a11384b107c853c75d94d8c84b0008932796_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        $__internal_045d9c1c875f4b655b3111aa61e9e00a5b5afc38e7504ae3c2629e33041235ba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_045d9c1c875f4b655b3111aa61e9e00a5b5afc38e7504ae3c2629e33041235ba->enter($__internal_045d9c1c875f4b655b3111aa61e9e00a5b5afc38e7504ae3c2629e33041235ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo ($context["status_code"] ?? $this->getContext($context, "status_code"));
        echo " ";
        echo ($context["status_text"] ?? $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_a2f1850124b1f1fc79ae4f581011a11384b107c853c75d94d8c84b0008932796->leave($__internal_a2f1850124b1f1fc79ae4f581011a11384b107c853c75d94d8c84b0008932796_prof);

        
        $__internal_045d9c1c875f4b655b3111aa61e9e00a5b5afc38e7504ae3c2629e33041235ba->leave($__internal_045d9c1c875f4b655b3111aa61e9e00a5b5afc38e7504ae3c2629e33041235ba_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "@Twig/Exception/error.txt.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.txt.twig");
    }
}
