<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_01f9eaf209b5cb5b73b192a4d79b1bc9209abd245030dd6e15c78cd2b273fc47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f342f47e6c3ca651b74eb2d619d588bb07b7538be1a9c50be3e4934085fe3a8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f342f47e6c3ca651b74eb2d619d588bb07b7538be1a9c50be3e4934085fe3a8c->enter($__internal_f342f47e6c3ca651b74eb2d619d588bb07b7538be1a9c50be3e4934085fe3a8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $__internal_de65708f28a61c6cdb2c45c7dac9f5dcc4b585105b96fd9d0ef0865474f64191 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_de65708f28a61c6cdb2c45c7dac9f5dcc4b585105b96fd9d0ef0865474f64191->enter($__internal_de65708f28a61c6cdb2c45c7dac9f5dcc4b585105b96fd9d0ef0865474f64191_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_f342f47e6c3ca651b74eb2d619d588bb07b7538be1a9c50be3e4934085fe3a8c->leave($__internal_f342f47e6c3ca651b74eb2d619d588bb07b7538be1a9c50be3e4934085fe3a8c_prof);

        
        $__internal_de65708f28a61c6cdb2c45c7dac9f5dcc4b585105b96fd9d0ef0865474f64191->leave($__internal_de65708f28a61c6cdb2c45c7dac9f5dcc4b585105b96fd9d0ef0865474f64191_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\forward.svg");
    }
}
