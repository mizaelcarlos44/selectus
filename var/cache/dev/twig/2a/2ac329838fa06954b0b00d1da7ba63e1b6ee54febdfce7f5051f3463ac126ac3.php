<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_914874d3da876f894a48e992d9bf7c9e4751bc264ff32959e2659779e5ffc6da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3e477e8a40a6213bdf04c09239f50b0b2ac2a31c95fc6e2c673222635b200284 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e477e8a40a6213bdf04c09239f50b0b2ac2a31c95fc6e2c673222635b200284->enter($__internal_3e477e8a40a6213bdf04c09239f50b0b2ac2a31c95fc6e2c673222635b200284_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_9f65c1bac9fd56e32f69530c372b86db5eeea30fccb11d07af27b46981d60ec3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f65c1bac9fd56e32f69530c372b86db5eeea30fccb11d07af27b46981d60ec3->enter($__internal_9f65c1bac9fd56e32f69530c372b86db5eeea30fccb11d07af27b46981d60ec3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_3e477e8a40a6213bdf04c09239f50b0b2ac2a31c95fc6e2c673222635b200284->leave($__internal_3e477e8a40a6213bdf04c09239f50b0b2ac2a31c95fc6e2c673222635b200284_prof);

        
        $__internal_9f65c1bac9fd56e32f69530c372b86db5eeea30fccb11d07af27b46981d60ec3->leave($__internal_9f65c1bac9fd56e32f69530c372b86db5eeea30fccb11d07af27b46981d60ec3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}
