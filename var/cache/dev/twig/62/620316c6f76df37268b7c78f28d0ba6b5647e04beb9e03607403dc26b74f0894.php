<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_af6092474556d59b2056973638b1922c770e3833f54c7a92242747fb2a0a8402 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4ba9d7edc3931c05e4f4a1df49e96d193fcaaffc24b78f18a374f59acb5ed55a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ba9d7edc3931c05e4f4a1df49e96d193fcaaffc24b78f18a374f59acb5ed55a->enter($__internal_4ba9d7edc3931c05e4f4a1df49e96d193fcaaffc24b78f18a374f59acb5ed55a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_cb5d232a09adb7731c07df32be48011e0f627bfb1b2bbbe434da2018196d1c9d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb5d232a09adb7731c07df32be48011e0f627bfb1b2bbbe434da2018196d1c9d->enter($__internal_cb5d232a09adb7731c07df32be48011e0f627bfb1b2bbbe434da2018196d1c9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_4ba9d7edc3931c05e4f4a1df49e96d193fcaaffc24b78f18a374f59acb5ed55a->leave($__internal_4ba9d7edc3931c05e4f4a1df49e96d193fcaaffc24b78f18a374f59acb5ed55a_prof);

        
        $__internal_cb5d232a09adb7731c07df32be48011e0f627bfb1b2bbbe434da2018196d1c9d->leave($__internal_cb5d232a09adb7731c07df32be48011e0f627bfb1b2bbbe434da2018196d1c9d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\textarea_widget.html.php");
    }
}
