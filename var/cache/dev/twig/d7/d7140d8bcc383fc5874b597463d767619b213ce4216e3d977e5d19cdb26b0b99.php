<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_eefe215a66c8104e90f879b6ab4cf695b9d9195b49254f2a3e3bc7437f58f457 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f8982cdae301bfe0d49e693fb245137d892d986b3c70a6d5efafa343766d9dad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8982cdae301bfe0d49e693fb245137d892d986b3c70a6d5efafa343766d9dad->enter($__internal_f8982cdae301bfe0d49e693fb245137d892d986b3c70a6d5efafa343766d9dad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        $__internal_f9bd557d01dc58373e20f445a0eac89871dcc26ab63f996190e40bbaebebc80f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9bd557d01dc58373e20f445a0eac89871dcc26ab63f996190e40bbaebebc80f->enter($__internal_f9bd557d01dc58373e20f445a0eac89871dcc26ab63f996190e40bbaebebc80f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_f8982cdae301bfe0d49e693fb245137d892d986b3c70a6d5efafa343766d9dad->leave($__internal_f8982cdae301bfe0d49e693fb245137d892d986b3c70a6d5efafa343766d9dad_prof);

        
        $__internal_f9bd557d01dc58373e20f445a0eac89871dcc26ab63f996190e40bbaebebc80f->leave($__internal_f9bd557d01dc58373e20f445a0eac89871dcc26ab63f996190e40bbaebebc80f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.css.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.css.twig");
    }
}
