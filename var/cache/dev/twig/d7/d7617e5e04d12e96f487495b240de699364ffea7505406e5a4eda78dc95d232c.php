<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_549ef9b04c3b04b5cfd07295a3312501d50ed7c92300be1de5ba1803b31e3d95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c4fc8208812d992f8ccd6a3bab76352ab371a416702089009f523ba479afc20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c4fc8208812d992f8ccd6a3bab76352ab371a416702089009f523ba479afc20->enter($__internal_7c4fc8208812d992f8ccd6a3bab76352ab371a416702089009f523ba479afc20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_1d5cc8aff52bb75c73e803d98d80e8fc1941704744c40ba82858f061a761cf29 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d5cc8aff52bb75c73e803d98d80e8fc1941704744c40ba82858f061a761cf29->enter($__internal_1d5cc8aff52bb75c73e803d98d80e8fc1941704744c40ba82858f061a761cf29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_7c4fc8208812d992f8ccd6a3bab76352ab371a416702089009f523ba479afc20->leave($__internal_7c4fc8208812d992f8ccd6a3bab76352ab371a416702089009f523ba479afc20_prof);

        
        $__internal_1d5cc8aff52bb75c73e803d98d80e8fc1941704744c40ba82858f061a761cf29->leave($__internal_1d5cc8aff52bb75c73e803d98d80e8fc1941704744c40ba82858f061a761cf29_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_attributes.html.php");
    }
}
