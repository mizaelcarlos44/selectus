<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_1253556a646148b46732b4956f595bc68f998958627453acf22d0f1af8679245 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60853c95a33a4705b295c7c8e30cf193cc0d5ffd3052d6cf79e212fffd843fde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60853c95a33a4705b295c7c8e30cf193cc0d5ffd3052d6cf79e212fffd843fde->enter($__internal_60853c95a33a4705b295c7c8e30cf193cc0d5ffd3052d6cf79e212fffd843fde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $__internal_8b619089bcbcf0b2cccf3630fc8a259d31c563cdd36a5a18661ee1ca817ce95d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b619089bcbcf0b2cccf3630fc8a259d31c563cdd36a5a18661ee1ca817ce95d->enter($__internal_8b619089bcbcf0b2cccf3630fc8a259d31c563cdd36a5a18661ee1ca817ce95d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_60853c95a33a4705b295c7c8e30cf193cc0d5ffd3052d6cf79e212fffd843fde->leave($__internal_60853c95a33a4705b295c7c8e30cf193cc0d5ffd3052d6cf79e212fffd843fde_prof);

        
        $__internal_8b619089bcbcf0b2cccf3630fc8a259d31c563cdd36a5a18661ee1ca817ce95d->leave($__internal_8b619089bcbcf0b2cccf3630fc8a259d31c563cdd36a5a18661ee1ca817ce95d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b6ca0630031591b3c0813488be226ccc1686d8c73f6885e0bb7d5054492cfc73 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b6ca0630031591b3c0813488be226ccc1686d8c73f6885e0bb7d5054492cfc73->enter($__internal_b6ca0630031591b3c0813488be226ccc1686d8c73f6885e0bb7d5054492cfc73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_098dc3ac24a813e8e79deaaa1034d49c77bb4e44c03c64ffb6a1de30e0b06fad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_098dc3ac24a813e8e79deaaa1034d49c77bb4e44c03c64ffb6a1de30e0b06fad->enter($__internal_098dc3ac24a813e8e79deaaa1034d49c77bb4e44c03c64ffb6a1de30e0b06fad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_098dc3ac24a813e8e79deaaa1034d49c77bb4e44c03c64ffb6a1de30e0b06fad->leave($__internal_098dc3ac24a813e8e79deaaa1034d49c77bb4e44c03c64ffb6a1de30e0b06fad_prof);

        
        $__internal_b6ca0630031591b3c0813488be226ccc1686d8c73f6885e0bb7d5054492cfc73->leave($__internal_b6ca0630031591b3c0813488be226ccc1686d8c73f6885e0bb7d5054492cfc73_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_35d1d64f4a1495fb3dce64e69773d9e35141fd9b2e99d81dda54bf1ecb7ef1d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35d1d64f4a1495fb3dce64e69773d9e35141fd9b2e99d81dda54bf1ecb7ef1d8->enter($__internal_35d1d64f4a1495fb3dce64e69773d9e35141fd9b2e99d81dda54bf1ecb7ef1d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3e8762510a1987a2e49e6fe0035fc0fa698b3fcf2ab95521dcb58c692c44cb67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e8762510a1987a2e49e6fe0035fc0fa698b3fcf2ab95521dcb58c692c44cb67->enter($__internal_3e8762510a1987a2e49e6fe0035fc0fa698b3fcf2ab95521dcb58c692c44cb67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_3e8762510a1987a2e49e6fe0035fc0fa698b3fcf2ab95521dcb58c692c44cb67->leave($__internal_3e8762510a1987a2e49e6fe0035fc0fa698b3fcf2ab95521dcb58c692c44cb67_prof);

        
        $__internal_35d1d64f4a1495fb3dce64e69773d9e35141fd9b2e99d81dda54bf1ecb7ef1d8->leave($__internal_35d1d64f4a1495fb3dce64e69773d9e35141fd9b2e99d81dda54bf1ecb7ef1d8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}
