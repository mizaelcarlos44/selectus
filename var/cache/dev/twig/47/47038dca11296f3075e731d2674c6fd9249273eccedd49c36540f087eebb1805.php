<?php

/* @Framework/Form/color_widget.html.php */
class __TwigTemplate_b60209ee9a0bc5cf5840332f5378f7b19e8570b7bcef78b3d96a457f7c58d697 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d384ae4f350ba5b0b1d25d888dcbcf12c17458f2a2d192e40de5e338aae895e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0d384ae4f350ba5b0b1d25d888dcbcf12c17458f2a2d192e40de5e338aae895e->enter($__internal_0d384ae4f350ba5b0b1d25d888dcbcf12c17458f2a2d192e40de5e338aae895e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/color_widget.html.php"));

        $__internal_970efc9769294a07253c3922ed3189ecf804e5753bd2c462cb801f9343258508 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_970efc9769294a07253c3922ed3189ecf804e5753bd2c462cb801f9343258508->enter($__internal_970efc9769294a07253c3922ed3189ecf804e5753bd2c462cb801f9343258508_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/color_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'color'));
";
        
        $__internal_0d384ae4f350ba5b0b1d25d888dcbcf12c17458f2a2d192e40de5e338aae895e->leave($__internal_0d384ae4f350ba5b0b1d25d888dcbcf12c17458f2a2d192e40de5e338aae895e_prof);

        
        $__internal_970efc9769294a07253c3922ed3189ecf804e5753bd2c462cb801f9343258508->leave($__internal_970efc9769294a07253c3922ed3189ecf804e5753bd2c462cb801f9343258508_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/color_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'color'));
", "@Framework/Form/color_widget.html.php", "C:\\wamp64\\www\\selectus\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\color_widget.html.php");
    }
}
